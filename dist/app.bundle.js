webpackJsonp([0],{

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	__webpack_require__(1);
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _angularUiRouter = __webpack_require__(295);
	
	var _angularUiRouter2 = _interopRequireDefault(_angularUiRouter);
	
	var _angularMaterial = __webpack_require__(296);
	
	var _angularMaterial2 = _interopRequireDefault(_angularMaterial);
	
	__webpack_require__(302);
	
	__webpack_require__(306);
	
	__webpack_require__(308);
	
	var _coreCoreJs = __webpack_require__(310);
	
	var _coreCoreJs2 = _interopRequireDefault(_coreCoreJs);
	
	var _commonCommonJs = __webpack_require__(324);
	
	var _commonCommonJs2 = _interopRequireDefault(_commonCommonJs);
	
	var _componentsComponentsJs = __webpack_require__(492);
	
	var _componentsComponentsJs2 = _interopRequireDefault(_componentsComponentsJs);
	
	var _appComponentJs = __webpack_require__(660);
	
	var _appComponentJs2 = _interopRequireDefault(_appComponentJs);
	
	_angular2['default'].module('app', [_angularUiRouter2['default'], _angularMaterial2['default'], _coreCoreJs2['default'].name, _commonCommonJs2['default'].name, _componentsComponentsJs2['default'].name])
	// TODO: this works for now, but probably should get moved to its own file
	.run(["$rootScope", function ($rootScope) {
	    'ngInject';
	    $rootScope.$onMany = function (events, fn) {
	        for (var i = 0, len = events.length; i < len; i++) {
	            this.$on(events[i], fn);
	        }
	    };
	}]).config(["$locationProvider", function ($locationProvider) {
	    'ngInject';
	    // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
	    // #how-to-configure-your-server-to-work-with-html5mode
	    $locationProvider.html5Mode(true).hashPrefix('#');
	}]).component('app', _appComponentJs2['default']);
	
	// do not allow user to accidentally close (not set on localhost)
	if (window.location.href.indexOf('localhost') === -1) {
	    window.onbeforeunload = function () {
	        return '';
	    };
	}

/***/ },

/***/ 310:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _angularUiRouter = __webpack_require__(295);
	
	var _angularUiRouter2 = _interopRequireDefault(_angularUiRouter);
	
	__webpack_require__(296);
	
	var _angularMaterialIcons = __webpack_require__(311);
	
	var _angularMaterialIcons2 = _interopRequireDefault(_angularMaterialIcons);
	
	var _constantsJs = __webpack_require__(313);
	
	var _constantsJs2 = _interopRequireDefault(_constantsJs);
	
	var _configsConfigsJs = __webpack_require__(315);
	
	var _configsConfigsJs2 = _interopRequireDefault(_configsConfigsJs);
	
	var _ripitServiceJs = __webpack_require__(319);
	
	var _ripitServiceJs2 = _interopRequireDefault(_ripitServiceJs);
	
	var coreModule = _angular2['default'].module('ripit.core', [_angularUiRouter2['default'], _angularMaterialIcons2['default'], _configsConfigsJs2['default'].name, _constantsJs2['default'].name]).service('RipItService', _ripitServiceJs2['default']);
	
	exports['default'] = coreModule;
	module.exports = exports['default'];

/***/ },

/***/ 313:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _configSettingsJs = __webpack_require__(314);
	
	var _configSettingsJs2 = _interopRequireDefault(_configSettingsJs);
	
	var constantsModule = _angular2['default'].module('ripit.constants', []).constant('APP_SETTINGS', _configSettingsJs2['default']);
	
	exports['default'] = constantsModule;
	module.exports = exports['default'];

/***/ },

/***/ 314:
/***/ function(module, exports) {

	/* eslint-disable no-var, spaced-comment */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	exports['default'] = {
	    feedbackEmail: 'test2@test.test',
	    regexHelpEmail: '',
	    catapultBaseUrl: '',
	    dirtyWords: [],
	    normalizer: {
	        'User Names': {
	            Facebook: ['fb', 'facebook'],
	            WhatsApp: ['whatsapp'],
	            Viber: ['viber'],
	            Skype: ['skype'],
	            Twitter: ['twitter'],
	            BlackBerry: ['bblackberry', 'bbbm'],
	            Youtube: ['youtube'],
	            PalTalk: ['paltalk'],
	            Apple: ['apple'],
	            Google: ['google'],
	            Smukh: ['shmukh'],
	            Tango: ['tango'],
	            'Al-Fida': ['alfida', 'al fida', 'al-fida'],
	            Sina: ['sina'],
	            Orkut: ['youtube'],
	            AlPlatformMedia: ['alplatformmedia', 'al platform media'],
	            'Linked-In': ['linkedin', 'linked in', 'linked-in'],
	            Tencent: ['tencent', 'qq'],
	            Pinger: ['pinger'],
	            Afraid: ['afraid'],
	            ChangeIP: ['changeip', 'change ip', 'change-ip'],
	            NoIP: ['noip', 'no ip', 'no-ip'],
	            Dyn: ['dyn'],
	            SiteLutions: ['sitelution', 'sitelutions'],
	            Tagged: ['tagged'],
	            Bayt: ['bayt'],
	            'Cash U': ['cashu', 'cash u', 'cash-u'],
	            Gurmad: ['gurmad'],
	            Yahoo: ['yahoo'],
	            HaninNET: ['haninnet', 'hanin net', 'hanin-net'],
	            Instagram: ['instagram']
	        }
	    },
	    regexPatterns: [{
	        label: 'DNI',
	        id: '',
	        selected: true,
	        children: [{
	            label: 'Email',
	            id: '',
	            selected: true,
	            patterns: [{
	                name: 'Email',
	                regex: /(((?:[A-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"))(?:@|\(AT\))(?:(?:[A-z0-9](?:[A-z0-9-]*[A-z0-9])?\.)+[A-z0-9](?:[A-z0-9-]*[A-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[A-z0-9-]*[A-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))/ig,
	                groups: ['Email', 'Username'],
	                mainGroup: 'Email'
	            }],
	            options: [{
	                name: 'Email',
	                label: 'Exclude .gov, .mil',
	                regex: /(?!.*\.(?:mil|gov)\b)(((?:[A-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"))(?:@|\(AT\))(?:(?:[A-z0-9](?:[A-z0-9-]*[A-z0-9])?\.)+[A-z0-9](?:[A-z0-9-]*[A-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[A-z0-9-]*[A-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))/ig,
	                groups: ['Email', 'Username'],
	                mainGroup: 'Email',
	                defaults: true
	            }]
	        }, {
	            label: 'IP Address',
	            id: '',
	            selected: true,
	            patterns: [{
	                name: 'ip_v4',
	                regex: /(\b[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\b)/ig
	            }, {
	                name: 'ip_v6',
	                regex: /(?:[0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,7}:|(?:[0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,5}(?::[0-9a-fA-F]{1,4}){1,2}|(?:[0-9a-fA-F]{1,4}:){1,4}(?::[0-9a-fA-F]{1,4}){1,3}|(?:[0-9a-fA-F]{1,4}:){1,3}(?::[0-9a-fA-F]{1,4}){1,4}|(?:[0-9a-fA-F]{1,4}:){1,2}(?::[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:(?:(?::[0-9a-fA-F]{1,4}){1,6})|:(?:(?:\:[0-9a-fA-F]{1,4}){1,7}|\:)|fe80:(?:\:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(?:ffff(?:\:0{1,4}){0,1}:){0,1}(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])|(?:[0-9a-fA-F]{1,4}:){1,4}:(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])/ig
	            }]
	        }, {
	            label: 'MAC Address',
	            id: '',
	            selected: true,
	            patterns: [{
	                name: 'MAC',
	                regex: /\b(?=.*[a-f])([a-f\d]{2}(?:[:-])?[a-f\d]{2}(?:[:-])?[a-f\d]{2}(?:[:-])?[a-f\d]{2}(?:[:-])?[a-f\d]{2}(?:[:-])?[a-f\d]{2}(?:[:-])?)\b/ig
	            }]
	        }, {
	            label: 'URLs',
	            id: '',
	            selected: true,
	            patterns: [{
	                name: 'url',
	                regex: /(?:(?:http|https|ftp):\/\/|www\.)(?:\S+(?::\S*)?@)?(?:(?:(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[0-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))|localhost)(?::\d{2,5})?(?:(?:\/|\\?|#)[^\s\[\]"'><]*)?/ig
	            }]
	        }, {
	            label: 'TPNs',
	            id: '',
	            selected: true,
	            dominant: ['All Selectors', 'random'],
	            patterns: [{
	                name: 'tpn',
	                regex: /((?:TIDE Number(?:\:)?|TPN(?:\:)?)|\(TPN\))(?:\s*)\d+/ig
	            }]
	        }, {
	            label: 'Objective Names',
	            id: '',
	            selected: true,
	            patterns: [{
	                name: 'objnames',
	                regex: /\bOBJ(?:ECTIVE)?(?:\s+)?[A-Z]+?\b/g
	            }]
	        }, {
	            label: 'Reporting',
	            id: '',
	            selected: true,
	            patterns: [{
	                // serial: 1/AA/123456-12 or 1/AA/12-12
	                name: 'n',
	                regex: /\b(?:[a-z]-)?(\d\/[a-z]{2}(?:\/[a-z0-9]{3})?\/\d{2,6}\-\d{2})\b/ig,
	                groups: ['Report'],
	                mainGroup: 'Report'
	            }, {
	                // serial: tdx-123/123123-12 or td-123/123123-12
	                name: 't',
	                regex: /\bserial\:(?:\s+)?(td(?:x)?\-\d{3}\/\d{6}\-\d{2})\b/ig,
	                groups: ['Report'],
	                mainGroup: 'Report'
	            }, {
	                // SUBJECT: (U) IIR 1 222 3333 44 or  SUBJECT: (U) IIR 1 222 3333 44
	                name: 'i',
	                regex: /\bsubject\:(?:\s+)?\((?:U|U\/\/FOUO)\)(?:\s+)?(iir(?:\s+)?\d{1}(?:\s+)?\d{3}(?:\s+)?\d{4}(?:\s+)?\d{2})\b/ig,
	                groups: ['Report'],
	                mainGroup: 'Report'
	            }]
	        }, {
	            label: 'Hash',
	            id: '',
	            selected: true,
	            children: [{
	                label: 'MD5',
	                id: '',
	                selected: true,
	                patterns: [{
	                    name: 'md5',
	                    regex: /\b[a-f0-9]{32}\b/ig
	                }]
	            }, {
	                label: 'SHA-1',
	                id: '',
	                selected: true,
	                patterns: [{
	                    name: 'sha-1',
	                    regex: /\b[a-f0-9]{40}\b/ig
	                }]
	            }]
	        }]
	    }, {
	        label: 'Social Media',
	        id: '',
	        selected: true,
	        children: [{
	            label: 'User Names',
	            id: '',
	            selected: true,
	            patterns: [
	            // VIBER
	            /*
	             ?_Viber_account: /(?:viber account )([\w]+)/ig,
	             ?_Viber_VoiceID: /([\w]+)(?:<vibervoiceID>)/ig,
	              //VSAT DATA
	             ?_VSAT_MAC: /(?:Yahclick VSAT)(?:.)+(?:MAC address )([0-9a-fA-F]+)/ig,
	             ?_VSAT_MAC_2: /(?:VSAT)(?:\s)+([0-9a-fA-F]+)/ig,
	             ?_VSAT_IP:/(?:IP address)(?:[\sa-zA-Z])+([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})/ig,
	             //IP address 95.210.162.20
	              //YOUTUBE
	             //?_Youtube_Username: nayeem acm
	              // TWITTER
	             ?_Twitter_url:/(?:Twitter URL: )([\w:\/.]+)/ig,
	             ?_Twitter_User: /([\S]+)(?:<twitter>)/ig,
	             //TELEGRAM
	             ?_Telegram_account:/(?:Telegram account \(*)([\w\d]{5,30})\)ig,
	              //WHATSAPP
	             ?_WhatsApp_account:/(?:whatsapp account )([\d]{1,30})/ig,
	             ?_WhatsApp_number:/(?:whatsapp number: )([\d]{1,30})/ig,
	             ?_WhatsApp_id:/(?:whatsapp id )([\d]{1,30})/ig,
	             // SKYPE
	             ?_Skype_account: /(?:Skype account )+([^\n\r\s]{1,30})/ig,
	             ?_Skype_ID: /(?:Skype ID: )+([^\n\r\s]{1,30})/ig,
	             ?_Skype_User: /([\S]+)(?:<SkypeUser>)/ig,
	             ?_Skype_Hash: /([\w]+)(?:<SkypeHash>)/ig,
	              ?: /(\b(\w{5,16}\b))(?=((?!\w{5,16}).)*\s(skype|facebook|viber|twitter|youtube|paltalk|shmukh|tango|al-fida|sina|orkut|alplatformmedia|linkedin|pinger|afraid|changeip|noip|dyn|sitelutions|tagged|bayt|cash u|gurmad)\b)/ig
	             */
	            {
	                // ex: {username}, his|her|their {service} account|id|number
	                name: 'FBI_sentence_before',
	                regex: /\b(\w+)(?:[,; ()]+)?\b(?:his|her|their)\b(?:\s+)(\w+)(?:\s+)?\b(?:account|id|number)\b/ig,
	                groups: ['User Names', 'Service'],
	                mainGroup: 'User Names'
	            }, {
	                // ex: {service} account|account name|id|user|username|user name is|was {username}
	                name: 'FBI_sentence_after',
	                regex: /(\w+)(?:\s+)?(?:account(?:\sname)?|id|user(?:\s?name)?)(?:\s+)?(?:is|was)(?:\s+)?(\w+)/ig,
	                groups: ['Service', 'User Names'],
	                mainGroup: 'User Names'
	            }, {
	                // ex: {username}<{service}>
	                name: 'FBI_Carrot',
	                regex: /(\w+)(?:\s+)?<([\w.]+)>/ig,
	                groups: ['User Names', 'Service'],
	                mainGroup: 'User Names'
	            }, {
	                // ex: {service} account|id|number|link {username}
	                name: 'FBI_ID_colon',
	                regex: /([A-Za-z]+ ?)(?:\s+)?(?:account|id|number|link):(?:\s+)?([\w@.,:/]+)/ig,
	                groups: ['Service', 'User Names'],
	                mainGroup: 'User Names'
	            }, {
	                // ex: {service} account|id {username}
	                name: 'FBI_ID_nocolon',
	                regex: /\b([\w]+)(?:\s+)?(?:(?:id)(?:s?|entifications?)?(?:\s+)?(?:numbers?)?|account(?:\s+)?(?:name)?|user(?:\s+)?(?:name)?)(?:\s+)?((?:[0-9]{1,15}(?:[\s,]+)?)+)\b/ig,
	                groups: ['Service', 'User Names'],
	                mainGroup: 'User Names'
	            }, {
	                // ex: BlackBerry PIN|PIN number|PIN numbers: 12345678 98765432
	                name: 'FBI_ID_BBM',
	                regex: /(BlackBerry)\s+(?:PIN\s+(?:numbers?)?):?(?:\s)?(?:\(+)((?:\w{8}(?:\s)?)*)/ig,
	                groups: ['Service', 'User Names'],
	                mainGroup: 'User Names'
	            }, {
	                // ex: Viber account|accounts|id|ids abcd1234 9874fedc
	                name: 'viber',
	                regex: /(Viber)(?:\s+)?(?:accounts?|ids?)(?:\s+)?((?:[a-f0-9]{8}(?:\s)?)*)/ig,
	                groups: ['Service', 'User Names'],
	                mainGroup: 'User Names'
	            }, {
	                // ex: tango user id|ids 12345678 012345678912
	                name: 'tango',
	                regex: /\b(tango)(?:\s+)?(?:user\sid)(?:\s+)?(?=[\d]{8,12})([\d]{8,12})\b/ig,
	                groups: ['Service', 'User Names'],
	                mainGroup: 'User Names'
	            }, {
	                // ex: username|user name|userID|handle|screenname|screen name {username}
	                name: 'fbiOne',
	                regex: /\b(?:user(?:\s+)?name|userID|handle|screen(?:\s+)?name)(?:\s+)?[ï¿½'"]{0,2}(\w{5,15})\b/ig,
	                groups: ['User Names'],
	                mainGroup: 'User Names'
	            }, {
	                name: 'twitter',
	                regex: /(\b@(\w{1,15})(?!\.\w)\b)/ig
	            }, {
	                name: 'FBI_simple_ID_colon',
	                regex: /id\:(?:\s+)?(\d+)()/ig
	            }]
	        }, {
	            label: 'Hashtags',
	            id: '',
	            selected: true,
	            patterns: [{
	                name: 'hashtag',
	                regex: /#[^\s\t#^,."'()]{5,}/ig
	            }]
	        }]
	    }, {
	        label: 'Locations',
	        id: '',
	        selected: true,
	        patterns: [{
	            //14Q FJ 1234567890, 4Q FJ 1234567890, 42Q FJ 1234567890
	            name: 'mgrs',
	            regex: /\b(((?:[0]?[\d]|[\d]{2})[A-Z])(?:\s+)?([A-Z]{2})(?:\s+)?((?:\d\d\d\d\d(?:\s)?\d\d\d\d\d)|(?:\d\d\d\d(?:\s)?\d\d\d\d)|(?:\d\d\d(?:\s)?\d\d\d)|(?:\d\d(?:\s)?\d\d)|(?:\d(?:\s)?\d)))\b/ig,
	            groups: ['Locations'],
	            mainGroup: 'Locations'
	        }, {
	            name: 'dd',
	            regex: /\d{1,3}\.\d{2,}(?:\xB0)?[NS]?[ \,]{1,2}[-]?\d{1,3}\.\d{2,}(?:\xB0)?[EW]?/ig
	        }, {
	            name: 'dms',
	            regex: /\d{1,2}(?:\xB0)(?:\s+)?\d{1,2}\'(?:\s+)?\d{1,2}(?:\.\d{1,})?\"[NS](?:\s+)\d{1,3}(?:\xB0)(?:\s+)?\d{1,2}\'(?:\s+)?\d{1,2}(?:\.\d{1,})?\"[EW]/ig
	        }, {
	            name: 'degree-decMinutes',
	            regex: /\d{1,2}(?:\xB0)(?:\s+)?\d{1,2}\.\d{1,}\'[NS](?:\s+)\d{1,3}(?:\xB0)(?:\s+)?\d{1,2}\.\d{1,}\'[EW]/ig
	        }, {
	            name: 'dms-fbi',
	            regex: /\b\d{5,7}\.\d{2}[NS]?[/\s]\d{5,7}\.\d{2}[EW]\b/ig
	        }, {
	            name: 'dms-?',
	            regex: /\b\d{4,9}[NS][\s]{0,2}\d{5,10}[EW]\b/ig
	        }]
	
	    }, {
	        label: 'Towers',
	        id: '',
	        selected: true,
	        patterns: [{
	            name: 'tower',
	            regex: /\b\d{3}[.-]\d{3}[.-]\d{4,5}[.-]\d{5}\b/gi
	        }]
	    }, {
	        label: 'Selectors',
	        id: '',
	        selected: true,
	        children: [{
	            label: 'Normalized Selectors',
	            id: '',
	            selected: true,
	            patterns: [{
	                name: 'selector',
	                regex: /\b\d{9,16}\b/gi
	            }],
	            options: [{
	                name: 'Normalized Selectors',
	                label: 'Exclude US selectors',
	                regex: /\b(?!1)\d{9,16}\b/g,
	                defaults: true
	            }]
	        }, {
	            label: 'Proton',
	            id: '',
	            selected: false,
	            patterns: [{
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{1}\-\d{4,6}\-\d{4,6}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{2}\-\d{8,10}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{2}\-\d{1}\-\d{8}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{2}\-\d{2}\-\d{6,9}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{2}\-\d{3}\-\d{5,8}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{2}\-\d{4}\-\d{4,7}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{2}\-\d{5}\-\d{4,5}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{3}\-\d{6,9}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{3}\-\d{1}\-\d{5,10}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{3}\-\d{2}\-\d{5,10}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{3}\-\d{3}\-\d{5,6}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{3}\-\d{5}\-\d{5,6}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{3}\-\d{5}\-\d{4}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{3}\-\d{1}\-\d{5,10}\b/ig
	            }, {
	                name: 'selector',
	                regex: /\b(?:imsi|imei@)?\d{14}\-\d{1}\-\d{5,10}\b/ig
	            }]
	        }]
	    }, {
	        label: 'Custom',
	        id: '',
	        selected: false,
	        children: [{
	            label: 'CUSTOM_REGEX_INPUT_DIRECTIVE'
	        }]
	    }]
	
	};
	module.exports = exports['default'];

/***/ },

/***/ 315:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _iconConfigJs = __webpack_require__(316);
	
	var _iconConfigJs2 = _interopRequireDefault(_iconConfigJs);
	
	var _routerJs = __webpack_require__(317);
	
	var _routerJs2 = _interopRequireDefault(_routerJs);
	
	var _themeJs = __webpack_require__(318);
	
	var _themeJs2 = _interopRequireDefault(_themeJs);
	
	var configsModule = _angular2['default'].module('ripit.configs', []).config(_iconConfigJs2['default']).config(_routerJs2['default']).config(_themeJs2['default']);
	
	exports['default'] = configsModule;
	module.exports = exports['default'];

/***/ },

/***/ 316:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	__webpack_require__(311);
	
	function IconConfig(ngMdIconServiceProvider) {
	    'ngInject';
	
	    ngMdIconServiceProvider.addShapes({
	        'document': '<path d="M19.5 3h0.5l6 7v18.009c0 1.093-0.894 1.991-1.997 1.991h-15.005c-1.107 0-1.997-0.899-1.997-2.007v-22.985c0-1.109 0.897-2.007 2.003-2.007h10.497zM19 4h-10.004c-0.55 0-0.996 0.455-0.996 0.995v23.009c0 0.55 0.455 0.995 1 0.995h15c0.552 0 1-0.445 1-0.993v-17.007h-4.002c-1.103 0-1.998-0.887-1.998-2.006v-4.994zM20 4.5v4.491c0 0.557 0.451 1.009 0.997 1.009h3.703l-4.7-5.5z"></path>',
	        'document-bmp': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM12 19.75v-3.75h2.995c1.111 0 2.005 0.895 2.005 2 0 0.6-0.261 1.135-0.676 1.5 0.415 0.366 0.676 0.902 0.676 1.5 0 1.112-0.898 2-2.005 2h-2.995v-3.25zM13 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM13 20v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM20.5 19l-1.5-3h-1v7h1v-5l1 2h1l1-2v5h1v-7h-1l-1.5 3zM24 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM25 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001z"></path>',
	        'document-c': '<path d="M21 13h6.993c1.671 0 3.007 1.343 3.007 2.999v7.002c0 1.657-1.346 2.999-3.007 2.999h-6.993v2.009c0 1.093-0.894 1.991-1.997 1.991h-15.005c-1.107 0-1.997-0.899-1.997-2.007v-22.985c0-1.109 0.897-2.007 2.003-2.007h10.997l6 7v3zM20 26h-8.993c-1.671 0-3.007-1.343-3.007-2.999v-7.002c0-1.657 1.346-2.999 3.007-2.999h8.993v-2h-4.002c-1.103 0-1.998-0.887-1.998-2.006v-4.994h-10.004c-0.55 0-0.996 0.455-0.996 0.995v23.009c0 0.55 0.455 0.995 1 0.995h15c0.552 0 1-0.445 1-0.993v-2.007zM15 4.5v4.491c0 0.557 0.451 1.009 0.997 1.009h3.703l-4.7-5.5zM10.995 14c-1.102 0-1.995 0.9-1.995 1.992v7.016c0 1.1 0.902 1.992 1.995 1.992h17.011c1.102 0 1.995-0.9 1.995-1.992v-7.016c0-1.1-0.902-1.992-1.995-1.992h-17.011zM22 21c-0.003 1.117-0.9 2-2.005 2h-0.99c-1.111 0-2.005-0.898-2.005-2.006v-2.988c0-1.119 0.898-2.006 2.005-2.006h0.99c1.109 0 2.002 0.895 2.005 2h-1c0-0.552-0.443-1-0.999-1h-1.002c-0.552 0-0.999 0.444-0.999 1v3c0 0.552 0.443 1 0.999 1h1.002c0.552 0 0.999-0.444 0.999-1l1-0z"></path>',
	        'document-csv': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM17 21c-0.003 1.117-0.9 2-2.005 2h-0.99c-1.111 0-2.005-0.898-2.005-2.006v-2.988c0-1.119 0.898-2.006 2.005-2.006h0.99c1.109 0 2.002 0.895 2.005 2h-1c0-0.552-0.443-1-0.999-1h-1.002c-0.552 0-0.999 0.444-0.999 1v3c0 0.552 0.443 1 0.999 1h1.002c0.552 0 0.999-0.444 0.999-1l1-0zM20.005 16c-1.107 0-2.005 0.888-2.005 2 0 1.105 0.888 2 2 2h0.991c0.557 0 1.009 0.444 1.009 1 0 0.552-0.443 1-0.999 1h-1.002c-0.552 0-0.999-0.456-0.999-0.996v-0.011h-1v0.006c0 1.105 0.894 2.001 2.005 2.001h0.99c1.107 0 2.005-0.888 2.005-2 0-1.105-0.888-2-2-2h-0.991c-0.557 0-1.009-0.444-1.009-1 0-0.552 0.443-1 0.999-1h1.002c0.552 0 0.999 0.453 0.999 1h1c0-1.105-0.894-2-2.005-2h-0.99zM26.5 21.25l1.5-5.25h1l-2 7h-1l-2-7h1l1.5 5.25z"></path>',
	        'document-dat': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM22 20h-3v3h-1v-5c0-1.112 0.898-2 2.005-2h0.99c1.111 0 2.005 0.895 2.005 2v5h-1v-3zM19.999 17c-0.552 0-0.999 0.444-0.999 1v1h3v-1c0-0.552-0.443-1-0.999-1h-1.002zM12 16h2.995c1.111 0 2.005 0.898 2.005 2.006v2.988c0 1.119-0.898 2.006-2.005 2.006h-2.995v-7zM13 17v5h2.001c0.552 0 0.999-0.444 0.999-1v-3c0-0.552-0.443-1-0.999-1h-2.001zM26 17v6h1v-6h2v-1h-5v1h2z"></path>',
	        'document-doc': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM12 16h2.995c1.111 0 2.005 0.898 2.005 2.006v2.988c0 1.119-0.898 2.006-2.005 2.006h-2.995v-7zM13 17v5h2.001c0.552 0 0.999-0.444 0.999-1v-3c0-0.552-0.443-1-0.999-1h-2.001zM20.005 16h0.99c1.111 0 2.005 0.898 2.005 2.006v2.988c0 1.119-0.898 2.006-2.005 2.006h-0.99c-1.111 0-2.005-0.898-2.005-2.006v-2.988c0-1.119 0.898-2.006 2.005-2.006zM19.999 17c-0.552 0-0.999 0.444-0.999 1v3c0 0.552 0.443 1 0.999 1h1.002c0.552 0 0.999-0.444 0.999-1v-3c0-0.552-0.443-1-0.999-1h-1.002zM29 21c-0.003 1.117-0.9 2-2.005 2h-0.99c-1.111 0-2.005-0.898-2.005-2.006v-2.988c0-1.119 0.898-2.006 2.005-2.006h0.99c1.109 0 2.002 0.895 2.005 2h-1c0-0.552-0.443-1-0.999-1h-1.002c-0.552 0-0.999 0.444-0.999 1v3c0 0.552 0.443 1 0.999 1h1.002c0.552 0 0.999-0.444 0.999-1l1-0z"></path>',
	        'document-docx': '<path d="M19 13h10.006c1.654 0 2.994 1.343 2.994 2.999v7.002c0 1.657-1.341 2.999-2.994 2.999h-10.006v2.009c0 1.093-0.894 1.991-1.997 1.991h-15.005c-1.107 0-1.997-0.899-1.997-2.007v-22.985c0-1.109 0.897-2.007 2.003-2.007h10.997l6 7v3zM18 26h-12.006c-1.654 0-2.994-1.343-2.994-2.999v-7.002c0-1.657 1.341-2.999 2.994-2.999h12.006v-2h-4.002c-1.103 0-1.998-0.887-1.998-2.006v-4.994h-10.004c-0.55 0-0.996 0.455-0.996 0.995v23.009c0 0.55 0.455 0.995 1 0.995h15c0.552 0 1-0.445 1-0.993v-2.007zM13 4.5v4.491c0 0.557 0.451 1.009 0.997 1.009h3.703l-4.7-5.5zM6.007 14c-1.109 0-2.007 0.9-2.007 1.992v7.016c0 1.1 0.898 1.992 2.007 1.992h22.985c1.109 0 2.007-0.9 2.007-1.992v-7.016c0-1.1-0.898-1.992-2.007-1.992h-22.985zM6 16h2.995c1.111 0 2.005 0.898 2.005 2.006v2.988c0 1.119-0.898 2.006-2.005 2.006h-2.995v-7zM7 17v5h2.001c0.552 0 0.999-0.444 0.999-1v-3c0-0.552-0.443-1-0.999-1h-2.001zM14.005 16h0.99c1.111 0 2.005 0.898 2.005 2.006v2.988c0 1.119-0.898 2.006-2.005 2.006h-0.99c-1.111 0-2.005-0.898-2.005-2.006v-2.988c0-1.119 0.898-2.006 2.005-2.006zM13.999 17c-0.552 0-0.999 0.444-0.999 1v3c0 0.552 0.443 1 0.999 1h1.002c0.552 0 0.999-0.444 0.999-1v-3c0-0.552-0.443-1-0.999-1h-1.002zM23 21c-0.003 1.117-0.9 2-2.005 2h-0.99c-1.111 0-2.005-0.898-2.005-2.006v-2.988c0-1.119 0.898-2.006 2.005-2.006h0.99c1.109 0 2.002 0.895 2.005 2h-1c0-0.552-0.443-1-0.999-1h-1.002c-0.552 0-0.999 0.444-0.999 1v3c0 0.552 0.443 1 0.999 1h1.002c0.552 0 0.999-0.444 0.999-1l1-0zM26 19.5l-2-3.5h1l1.5 2.625 1.5-2.625h1l-2 3.5 2 3.5h-1l-1.5-2.625-1.5 2.625h-1l2-3.5z"></path>',
	        'document-dot': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM12 16h2.995c1.111 0 2.005 0.898 2.005 2.006v2.988c0 1.119-0.898 2.006-2.005 2.006h-2.995v-7zM13 17v5h2.001c0.552 0 0.999-0.444 0.999-1v-3c0-0.552-0.443-1-0.999-1h-2.001zM20.005 16h0.99c1.111 0 2.005 0.898 2.005 2.006v2.988c0 1.119-0.898 2.006-2.005 2.006h-0.99c-1.111 0-2.005-0.898-2.005-2.006v-2.988c0-1.119 0.898-2.006 2.005-2.006zM19.999 17c-0.552 0-0.999 0.444-0.999 1v3c0 0.552 0.443 1 0.999 1h1.002c0.552 0 0.999-0.444 0.999-1v-3c0-0.552-0.443-1-0.999-1h-1.002zM26 17v6h1v-6h2v-1h-5v1h2z"></path>',
	        'document-dotx': '<path d="M19 13h10.006c1.654 0 2.994 1.343 2.994 2.999v7.002c0 1.657-1.341 2.999-2.994 2.999h-10.006v2.009c0 1.093-0.894 1.991-1.997 1.991h-15.005c-1.107 0-1.997-0.899-1.997-2.007v-22.985c0-1.109 0.897-2.007 2.003-2.007h10.997l6 7v3zM18 26h-12.006c-1.654 0-2.994-1.343-2.994-2.999v-7.002c0-1.657 1.341-2.999 2.994-2.999h12.006v-2h-4.002c-1.103 0-1.998-0.887-1.998-2.006v-4.994h-10.004c-0.55 0-0.996 0.455-0.996 0.995v23.009c0 0.55 0.455 0.995 1 0.995h15c0.552 0 1-0.445 1-0.993v-2.007zM13 4.5v4.491c0 0.557 0.451 1.009 0.997 1.009h3.703l-4.7-5.5zM6.007 14c-1.109 0-2.007 0.9-2.007 1.992v7.016c0 1.1 0.898 1.992 2.007 1.992h22.985c1.109 0 2.007-0.9 2.007-1.992v-7.016c0-1.1-0.898-1.992-2.007-1.992h-22.985zM6 16h2.995c1.111 0 2.005 0.898 2.005 2.006v2.988c0 1.119-0.898 2.006-2.005 2.006h-2.995v-7zM7 17v5h2.001c0.552 0 0.999-0.444 0.999-1v-3c0-0.552-0.443-1-0.999-1h-2.001zM14.005 16h0.99c1.111 0 2.005 0.898 2.005 2.006v2.988c0 1.119-0.898 2.006-2.005 2.006h-0.99c-1.111 0-2.005-0.898-2.005-2.006v-2.988c0-1.119 0.898-2.006 2.005-2.006zM13.999 17c-0.552 0-0.999 0.444-0.999 1v3c0 0.552 0.443 1 0.999 1h1.002c0.552 0 0.999-0.444 0.999-1v-3c0-0.552-0.443-1-0.999-1h-1.002zM20 17v6h1v-6h2v-1h-5v1h2zM26 19.5l-2-3.5h1l1.5 2.625 1.5-2.625h1l-2 3.5 2 3.5h-1l-1.5-2.625-1.5 2.625h-1l2-3.5z"></path>',
	        'document-gif': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM17 20v2h-2c-0.556 0-1-0.448-1-1v-3c0-0.556 0.448-1 1-1h3v-1h-2.995c-1.107 0-2.005 0.887-2.005 2.006v2.988c0 1.108 0.894 2.006 2.005 2.006h2.995v-4h-3v1h2zM20 17v5h-1v1h3v-1h-1v-5h1v-1h-3v1h1zM24 19v-2h4v-1h-5v7h1v-3h3v-1h-3z"></path>',
	        'document-html': '<path d="M19 13h10.006c1.654 0 2.994 1.343 2.994 2.999v7.002c0 1.657-1.341 2.999-2.994 2.999h-10.006v2.009c0 1.093-0.894 1.991-1.997 1.991h-15.005c-1.107 0-1.997-0.899-1.997-2.007v-22.985c0-1.109 0.897-2.007 2.003-2.007h10.997l6 7v3zM18 26h-12.006c-1.654 0-2.994-1.343-2.994-2.999v-7.002c0-1.657 1.341-2.999 2.994-2.999h12.006v-2h-4.002c-1.103 0-1.998-0.887-1.998-2.006v-4.994h-10.004c-0.55 0-0.996 0.455-0.996 0.995v23.009c0 0.55 0.455 0.995 1 0.995h15c0.552 0 1-0.445 1-0.993v-2.007zM13 4.5v4.491c0 0.557 0.451 1.009 0.997 1.009h3.703l-4.7-5.5zM6.007 14c-1.109 0-2.007 0.9-2.007 1.992v7.016c0 1.1 0.898 1.992 2.007 1.992h22.985c1.109 0 2.007-0.9 2.007-1.992v-7.016c0-1.1-0.898-1.992-2.007-1.992h-22.985zM29 22v1h-5v-7h1v6h4zM14 17v6h1v-6h2v-1h-5v1h2zM10 19v-3h1v7h-1v-3h-3v3h-1v-7h1v3h3zM20.5 19l-1.5-3h-1v7h1v-5l1 2h1l1-2v5h1v-7h-1l-1.5 3z"></path>',
	        'document-error': '<path d="M24 25.923v-16.923l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h8.184l-1.182 2h13l-3-5.077zM15.773 28h-8.773c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v14.231l-2.5-4.231-4.727 8zM18 3.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM20.5 22l4.75 8h-9.5l4.75-8zM20 24v3h1v-3h-1zM20 28v1h1v-1h-1z"></path>',
	        'document-jpg': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM12 20v0.994c0 1.108 0.894 2.006 2.005 2.006h0.99c1.107 0 2.005-0.887 2.005-2.006v-4.994h-1v5.009c0 0.54-0.447 0.991-0.999 0.991h-1.002c-0.556 0-0.999-0.448-0.999-1v-1h-1zM18 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM19 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM28 20v2h-2c-0.556 0-1-0.448-1-1v-3c0-0.556 0.448-1 1-1h3v-1h-2.995c-1.107 0-2.005 0.887-2.005 2.006v2.988c0 1.108 0.894 2.006 2.005 2.006h2.995v-4h-3v1h2z"></path>',
	        'document-pdf': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM25 19v-2h4v-1h-5v7h1v-3h3v-1h-3zM12 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM13 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM18 16h2.995c1.111 0 2.005 0.898 2.005 2.006v2.988c0 1.119-0.898 2.006-2.005 2.006h-2.995v-7zM19 17v5h2.001c0.552 0 0.999-0.444 0.999-1v-3c0-0.552-0.443-1-0.999-1h-2.001z"></path>',
	        'document-php': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM12 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM13 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM22 19v-3h1v7h-1v-3h-3v3h-1v-7h1v3h3zM24 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM25 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001z"></path>',
	        'document-png': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM12 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM13 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM19 18v5h-1v-7h1l3 5v-5h1v7h-1l-3-5zM28 20v2h-2c-0.556 0-1-0.448-1-1v-3c0-0.556 0.448-1 1-1h3v-1h-2.995c-1.107 0-2.005 0.887-2.005 2.006v2.988c0 1.108 0.894 2.006 2.005 2.006h2.995v-4h-3v1h2z"></path>',
	        'document-ppt': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM18 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM19 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM12 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM13 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM26 17v6h1v-6h2v-1h-5v1h2z"></path>',
	        'document-pps': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM18 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM19 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM12 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM13 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM26.005 16c-1.107 0-2.005 0.888-2.005 2 0 1.105 0.888 2 2 2h0.991c0.557 0 1.009 0.444 1.009 1 0 0.552-0.443 1-0.999 1h-1.002c-0.552 0-0.999-0.456-0.999-0.996v-0.011h-1v0.006c0 1.105 0.894 2.001 2.005 2.001h0.99c1.107 0 2.005-0.888 2.005-2 0-1.105-0.888-2-2-2h-0.991c-0.557 0-1.009-0.444-1.009-1 0-0.552 0.443-1 0.999-1h1.002c0.552 0 0.999 0.453 0.999 1h1c0-1.105-0.894-2-2.005-2h-0.99z"></path>',
	        'document-pptx': '<path d="M19 13h10.006c1.654 0 2.994 1.343 2.994 2.999v7.002c0 1.657-1.341 2.999-2.994 2.999h-10.006v2.009c0 1.093-0.894 1.991-1.997 1.991h-15.005c-1.107 0-1.997-0.899-1.997-2.007v-22.985c0-1.109 0.897-2.007 2.003-2.007h10.997l6 7v3zM18 26h-12.006c-1.654 0-2.994-1.343-2.994-2.999v-7.002c0-1.657 1.341-2.999 2.994-2.999h12.006v-2h-4.002c-1.103 0-1.998-0.887-1.998-2.006v-4.994h-10.004c-0.55 0-0.996 0.455-0.996 0.995v23.009c0 0.55 0.455 0.995 1 0.995h15c0.552 0 1-0.445 1-0.993v-2.007zM13 4.5v4.491c0 0.557 0.451 1.009 0.997 1.009h3.703l-4.7-5.5zM6.007 14c-1.109 0-2.007 0.9-2.007 1.992v7.016c0 1.1 0.898 1.992 2.007 1.992h22.985c1.109 0 2.007-0.9 2.007-1.992v-7.016c0-1.1-0.898-1.992-2.007-1.992h-22.985zM6 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM7 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM12 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM13 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM20 17v6h1v-6h2v-1h-5v1h2zM26 19.5l-2-3.5h1l1.5 2.625 1.5-2.625h1l-2 3.5 2 3.5h-1l-1.5-2.625-1.5 2.625h-1l2-3.5z"></path>',
	        'document-py': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h4.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-4.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-10.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h10.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM8.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM12 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM13 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM21 20v3h-1v-3l-2-3v-1h1v1l1.5 2.25 1.5-2.25v-1h1v1l-2 3z"></path>',
	        'document-rar': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM13.8 20h-0.8v3h-1v-7h2.995c1.111 0 2.005 0.895 2.005 2 0 1.111-0.895 1.997-2 2l2 3h-1.2l-2-3zM13 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM22 20h-3v3h-1v-5c0-1.112 0.898-2 2.005-2h0.99c1.111 0 2.005 0.895 2.005 2v5h-1v-3zM19.999 17c-0.552 0-0.999 0.444-0.999 1v1h3v-1c0-0.552-0.443-1-0.999-1h-1.002zM25.8 20h-0.8v3h-1v-7h2.995c1.111 0 2.005 0.895 2.005 2 0 1.111-0.895 1.997-2 2l2 3h-1.2l-2-3zM25 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001z"></path>',
	        'document-rtf': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM13.8 20h-0.8v3h-1v-7h2.995c1.111 0 2.005 0.895 2.005 2 0 1.111-0.895 1.997-2 2l2 3h-1.2l-2-3zM13 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001zM20 17v6h1v-6h2v-1h-5v1h2zM25 19v-2h4v-1h-5v7h1v-3h3v-1h-3z"></path>',
	        'document-sql': '<path d="M20.676 22.883l0.885 0.885 0.707-0.707-0.753-0.753c0.303-0.35 0.485-0.808 0.485-1.313v-2.988c0-1.108-0.894-2.006-2.005-2.006h-0.99c-1.107 0-2.005 0.887-2.005 2.006v2.988c0 1.108 0.894 2.006 2.005 2.006h0.99c0.239 0 0.468-0.041 0.681-0.117zM19.793 22h-0.794c-0.556 0-0.999-0.448-0.999-1v-3c0-0.556 0.447-1 0.999-1h1.002c0.556 0 0.999 0.448 0.999 1v3c0 0.224-0.073 0.431-0.196 0.597l-1.365-1.365-0.707 0.707 1.061 1.061zM21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h6.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-6.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-8.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h8.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM10.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM13.005 16c-1.107 0-2.005 0.888-2.005 2 0 1.105 0.888 2 2 2h0.991c0.557 0 1.009 0.444 1.009 1 0 0.552-0.443 1-0.999 1h-1.002c-0.552 0-0.999-0.456-0.999-0.996v-0.011h-1v0.006c0 1.105 0.894 2.001 2.005 2.001h0.99c1.107 0 2.005-0.888 2.005-2 0-1.105-0.888-2-2-2h-0.991c-0.557 0-1.009-0.444-1.009-1 0-0.552 0.443-1 0.999-1h1.002c0.552 0 0.999 0.453 0.999 1h1c0-1.105-0.894-2-2.005-2h-0.99zM28 22v1h-5v-7h1v6h4z"></path>',
	        'document-tiff': '<path d="M19 13h10.007c1.654 0 2.993 1.343 2.993 2.999v7.002c0 1.657-1.34 2.999-2.993 2.999h-10.007v2.009c0 1.093-0.894 1.991-1.997 1.991h-15.005c-1.107 0-1.997-0.899-1.997-2.007v-22.985c0-1.109 0.897-2.007 2.003-2.007h10.997l6 7v3zM18 26h-10.007c-1.654 0-2.993-1.343-2.993-2.999v-7.002c0-1.657 1.34-2.999 2.993-2.999h10.007v-2h-4.002c-1.103 0-1.998-0.887-1.998-2.006v-4.994h-10.004c-0.55 0-0.996 0.455-0.996 0.995v23.009c0 0.55 0.455 0.995 1 0.995h15c0.552 0 1-0.445 1-0.993v-2.007zM13 4.5v4.491c0 0.557 0.451 1.009 0.997 1.009h3.703l-4.7-5.5zM8.004 14c-1.107 0-2.004 0.9-2.004 1.992v7.016c0 1.1 0.89 1.992 2.004 1.992h20.993c1.107 0 2.004-0.9 2.004-1.992v-7.016c0-1.1-0.89-1.992-2.004-1.992h-20.993zM10 17v6h1v-6h2v-1h-5v1h2zM15 17v5h-1v1h3v-1h-1v-5h1v-1h-3v1h1zM19 19v-2h4v-1h-5v7h1v-3h3v-1h-3zM25 19v-2h4v-1h-5v7h1v-3h3v-1h-3z"></path>',
	        'document-txt': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM14 17v6h1v-6h2v-1h-5v1h2zM20 19.5l-2-3.5h1l1.5 2.625 1.5-2.625h1l-2 3.5 2 3.5h-1l-1.5-2.625-1.5 2.625h-1l2-3.5zM26 17v6h1v-6h2v-1h-5v1h2z"></path>',
	        'document-xls': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM14 19.5l-2-3.5h1l1.5 2.625 1.5-2.625h1l-2 3.5 2 3.5h-1l-1.5-2.625-1.5 2.625h-1l2-3.5zM23 22v1h-5v-7h1v6h4zM26.005 16c-1.107 0-2.005 0.888-2.005 2 0 1.105 0.888 2 2 2h0.991c0.557 0 1.009 0.444 1.009 1 0 0.552-0.443 1-0.999 1h-1.002c-0.552 0-0.999-0.456-0.999-0.996v-0.011h-1v0.006c0 1.105 0.894 2.001 2.005 2.001h0.99c1.107 0 2.005-0.888 2.005-2 0-1.105-0.888-2-2-2h-0.991c-0.557 0-1.009-0.444-1.009-1 0-0.552 0.443-1 0.999-1h1.002c0.552 0 0.999 0.453 0.999 1h1c0-1.105-0.894-2-2.005-2h-0.99z"></path>',
	        'document-xlsx': '<path d="M19 13h10.006c1.654 0 2.994 1.343 2.994 2.999v7.002c0 1.657-1.341 2.999-2.994 2.999h-10.006v2.009c0 1.093-0.894 1.991-1.997 1.991h-15.005c-1.107 0-1.997-0.899-1.997-2.007v-22.985c0-1.109 0.897-2.007 2.003-2.007h10.997l6 7v3zM18 26h-12.006c-1.654 0-2.994-1.343-2.994-2.999v-7.002c0-1.657 1.341-2.999 2.994-2.999h12.006v-2h-4.002c-1.103 0-1.998-0.887-1.998-2.006v-4.994h-10.004c-0.55 0-0.996 0.455-0.996 0.995v23.009c0 0.55 0.455 0.995 1 0.995h15c0.552 0 1-0.445 1-0.993v-2.007zM13 4.5v4.491c0 0.557 0.451 1.009 0.997 1.009h3.703l-4.7-5.5zM6.007 14c-1.109 0-2.007 0.9-2.007 1.992v7.016c0 1.1 0.898 1.992 2.007 1.992h22.985c1.109 0 2.007-0.9 2.007-1.992v-7.016c0-1.1-0.898-1.992-2.007-1.992h-22.985zM8 19.5l-2-3.5h1l1.5 2.625 1.5-2.625h1l-2 3.5 2 3.5h-1l-1.5-2.625-1.5 2.625h-1l2-3.5zM17 22v1h-5v-7h1v6h4zM20.005 16c-1.107 0-2.005 0.888-2.005 2 0 1.105 0.888 2 2 2h0.991c0.557 0 1.009 0.444 1.009 1 0 0.552-0.443 1-0.999 1h-1.002c-0.552 0-0.999-0.456-0.999-0.996v-0.011h-1v0.006c0 1.105 0.894 2.001 2.005 2.001h0.99c1.107 0 2.005-0.888 2.005-2 0-1.105-0.888-2-2-2h-0.991c-0.557 0-1.009-0.444-1.009-1 0-0.552 0.443-1 0.999-1h1.002c0.552 0 0.999 0.453 0.999 1h1c0-1.105-0.894-2-2.005-2h-0.99zM26 19.5l-2-3.5h1l1.5 2.625 1.5-2.625h1l-2 3.5 2 3.5h-1l-1.5-2.625-1.5 2.625h-1l2-3.5z"></path>',
	        'document-xml': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM14 19.5l-2-3.5h1l1.5 2.625 1.5-2.625h1l-2 3.5 2 3.5h-1l-1.5-2.625-1.5 2.625h-1l2-3.5zM20.5 19l-1.5-3h-1v7h1v-5l1 2h1l1-2v5h1v-7h-1l-1.5 3zM29 22v1h-5v-7h1v6h4z"></path>',
	        'document-yml': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM15 20v3h-1v-3l-2-3v-1h1v1l1.5 2.25 1.5-2.25v-1h1v1l-2 3zM20.5 19l-1.5-3h-1v7h1v-5l1 2h1l1-2v5h1v-7h-1l-1.5 3zM29 22v1h-5v-7h1v6h4z"></path>',
	        'document-zip': '<path d="M21 13v-3l-6-7h-10.997c-1.106 0-2.003 0.898-2.003 2.007v22.985c0 1.109 0.891 2.007 1.997 2.007h15.005c1.103 0 1.997-0.898 1.997-1.991v-2.009h7.993c1.661 0 3.007-1.342 3.007-2.999v-7.002c0-1.656-1.336-2.999-3.007-2.999h-7.993zM20 26v2.007c0 0.548-0.448 0.993-1 0.993h-15c-0.545 0-1-0.446-1-0.995v-23.009c0-0.54 0.446-0.995 0.996-0.995h10.004v4.994c0 1.119 0.895 2.006 1.998 2.006h4.002v2h-7.993c-1.661 0-3.007 1.342-3.007 2.999v7.002c0 1.656 1.336 2.999 3.007 2.999h7.993zM15 4.5l4.7 5.5h-3.703c-0.546 0-0.997-0.452-0.997-1.009v-4.491zM11.995 14h17.011c1.092 0 1.995 0.892 1.995 1.992v7.016c0 1.092-0.893 1.992-1.995 1.992h-17.011c-1.092 0-1.995-0.892-1.995-1.992v-7.016c0-1.092 0.893-1.992 1.995-1.992zM14 22l4-5v-1h-5v1h4l-4 5v1h5v-1h-4zM20 17v5h-1v1h3v-1h-1v-5h1v-1h-3v1h1zM23 18v-2h2.995c1.111 0 2.005 0.895 2.005 2 0 1.112-0.898 2-2.005 2h-1.995v3h-1v-5zM24 17v2h2.001c0.552 0 0.999-0.444 0.999-1 0-0.552-0.443-1-0.999-1h-2.001z"></path>',
	        'back-arrow': '<path d="M365.499,174.8H54.253l68.205-68.205c6.198-6.198,6.198-16.273,0-22.47c-6.198-6.198-16.273-6.198-22.47,0L4.64,179.472l-0.095,0.159l-3.305,4.99L0,190.691v0.064l1.24,6.007l3.305,4.99l0.095,0.191l95.347,95.347c6.198,6.166,16.273,6.166,22.47,0c6.198-6.166,6.198-16.304,0-22.47l-68.205-68.237h311.246c8.74,0,15.891-7.119,15.891-15.891C381.39,181.919,374.239,174.8,365.499,174.8z"></path>',
	        'info': '<path d="M62.162,0c6.696,0,10.043,4.567,10.043,9.789c0,6.522-5.814,12.555-13.391,12.555c-6.344,0-10.045-3.752-9.869-9.947C48.945,7.176,53.35,0,62.162,0z M41.543,100c-5.287,0-9.164-3.262-5.463-17.615l6.07-25.457c1.057-4.077,1.23-5.707,0-5.707c-1.588,0-8.451,2.816-12.51,5.59L27,52.406C39.863,41.48,54.662,35.072,61.004,35.072c5.285,0,6.168,6.361,3.525,16.148L57.58,77.98c-1.234,4.729-0.703,6.359,0.527,6.359c1.586,0,6.787-1.963,11.896-6.041L73,82.377C60.488,95.1,46.83,100,41.543,100z"></path>',
	        'clippy': '<path d="M128 768h256v64H128v-64z m320-384H128v64h320v-64z m128 192V448L384 640l192 192V704h320V576H576z m-288-64H128v64h160v-64zM128 704h160v-64H128v64z m576 64h64v128c-1 18-7 33-19 45s-27 18-45 19H64c-35 0-64-29-64-64V192c0-35 29-64 64-64h192C256 57 313 0 384 0s128 57 128 128h192c35 0 64 29 64 64v320h-64V320H64v576h640V768zM128 256h512c0-35-29-64-64-64h-64c-35 0-64-29-64-64s-29-64-64-64-64 29-64 64-29 64-64 64h-64c-35 0-64 29-64 64z"></path>'
	    });
	}
	
	IconConfig.$inject = ['ngMdIconServiceProvider'];
	exports['default'] = IconConfig;
	module.exports = exports['default'];

/***/ },

/***/ 317:
/***/ function(module, exports) {

	'use strict';
	
	Router.$inject = ["$locationProvider", "$stateProvider", "$urlRouterProvider"];
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	function Router($locationProvider, $stateProvider, $urlRouterProvider) {
	    'ngInject';
	
	    // $locationProvider.html5Mode(true).hashPrefix('#');
	
	    // $urlRouterProvider.otherwise('/');
	
	    $stateProvider.state('home', {
	        url: '/',
	        template: '<app></app>'
	    }).state('index', {
	        url: '/index.html',
	        template: '<app></app>'
	    });
	}
	
	exports['default'] = Router;
	module.exports = exports['default'];

/***/ },

/***/ 318:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	__webpack_require__(296);
	
	var themeConfig = function themeConfig($mdThemingProvider) {
	    'ngInject';
	
	    $mdThemingProvider.theme('default').primaryPalette('blue').accentPalette('red');
	};
	themeConfig.$inject = ["$mdThemingProvider"];
	
	exports['default'] = themeConfig;
	module.exports = exports['default'];

/***/ },

/***/ 319:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _lodash = __webpack_require__(320);
	
	var _lodash2 = _interopRequireDefault(_lodash);
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _geoConverter = __webpack_require__(322);
	
	var _geoConverter2 = _interopRequireDefault(_geoConverter);
	
	var RipItService = (function () {
	    RipItService.$inject = ["$rootScope", "AppStateService", "HelpersService", "ParseProvider", "StorageService"];
	    function RipItService($rootScope, AppStateService, HelpersService, ParseProvider, StorageService) {
	        'ngInject';
	
	        _classCallCheck(this, RipItService);
	
	        this.$rootScope = $rootScope;
	        this.AppStateService = AppStateService;
	        this.HelpersService = HelpersService;
	        this.ParseProvider = ParseProvider;
	        this.StorageService = StorageService;
	
	        this.resultObjectMetaProperties = ['hitLength', 'indexes', 'label', 'count', 'docName', 'mainGroup'];
	        this.binsList = [];
	    }
	
	    _createClass(RipItService, [{
	        key: 'parse',
	        value: function parse() {
	            var _this = this;
	
	            _angular2['default'].forEach(this.StorageService.documents, function (doc, name) {
	                if (doc.valid) {
	                    if (_this.AppStateService.getState('needsRipping') || doc.needsParsing) {
	                        if (!_this.StorageService.documents[name].results) {
	                            _this.StorageService.documents[name].results = {};
	                        }
	                        _this.StorageService.documents[name].results = _this.ParseProvider.ripIt(doc);
	                        doc.needsParsing = false;
	                    }
	                }
	            });
	
	            // this.AppStateService.setState('needsRipping', false);
	            this.$rootScope.$broadcast('data-ripped');
	        }
	    }, {
	        key: 'generateBinsList',
	        value: function generateBinsList() {
	            var output = {};
	
	            for (var docName in this.StorageService.activeDocuments) {
	                if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
	                    var results = this.StorageService.activeDocuments[docName].results;
	                    // loop through each bin in the result set of the document
	                    for (var binName in results) {
	                        if (!output[binName]) {
	                            output[binName] = {
	                                name: binName,
	                                count: 0
	                            };
	                        }
	                        output[binName].count += results[binName].length;
	                    }
	                }
	            }
	            output = _lodash2['default'].values(output);
	
	            // ensure we do not lose the object reference
	            this.binsList.length = 0;
	            return Array.prototype.push.apply(this.binsList, output);
	
	            // let output = this.generateBinOverviewResultData();
	            // this.HelpersService.emptyObject(this.binsList);
	            // angular.merge(this.binsList, output);
	
	            // return output;
	        }
	    }, {
	        key: 'generateMatchMakerData',
	        value: function generateMatchMakerData() {
	            var matchMakerAll = {};
	
	            var binsList = _angular2['default'].copy(this.binsList);
	            var bins = [];
	
	            // for the All bin
	            var entityProps = new Set();
	            var docNameProps = new Set();
	            for (var bin in binsList) {
	                if (binsList.hasOwnProperty(bin)) {
	                    if (binsList[bin].name !== 'CUSTOM_REGEX_INPUT_DIRECTIVE') {
	                        bins.push(binsList[bin].name);
	                    }
	                }
	            }
	
	            for (var i = 0, len = bins.length; i < len; i++) {
	                var binData = this.generateBinResultData(bins[i]);
	                var metaProperties = ['Count', 'Document'];
	
	                var matchMaker = {};
	                var matchMakerTable = {};
	                var extractedEntityProperties = new Set();
	
	                for (var _i = 0, _len = binData.length; _i < _len; _i++) {
	                    // build up the unique key
	                    var uniqueKey = [];
	                    for (var d in binData[_i]) {
	                        if (binData[_i].hasOwnProperty(d)) {
	                            if (!metaProperties.includes(d)) {
	                                uniqueKey.push(binData[_i][d]);
	                                extractedEntityProperties.add(d);
	                            }
	                        }
	                    }
	                    uniqueKey = uniqueKey.join('|');
	
	                    if (!matchMaker[uniqueKey]) {
	                        matchMaker[uniqueKey] = {};
	                    }
	                    if (!matchMaker[uniqueKey][binData[_i].Document]) {
	                        matchMaker[uniqueKey][binData[_i].Document] = [];
	                    }
	                    matchMaker[uniqueKey][binData[_i].Document].push(binData[_i]);
	                }
	
	                // remove entities that have less than 2 matches between documents
	                for (var uniqueKey in matchMaker) {
	                    if (matchMaker.hasOwnProperty(uniqueKey)) {
	                        if (Object.keys(matchMaker[uniqueKey]).length < 2) {
	                            delete matchMaker[uniqueKey];
	                        }
	                    }
	                }
	                var headers = new Set();
	                var _iteratorNormalCompletion = true;
	                var _didIteratorError = false;
	                var _iteratorError = undefined;
	
	                try {
	                    for (var _iterator = extractedEntityProperties[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	                        var entity = _step.value;
	
	                        headers.add(entity);
	                        entityProps.add(entity);
	                    }
	                } catch (err) {
	                    _didIteratorError = true;
	                    _iteratorError = err;
	                } finally {
	                    try {
	                        if (!_iteratorNormalCompletion && _iterator['return']) {
	                            _iterator['return']();
	                        }
	                    } finally {
	                        if (_didIteratorError) {
	                            throw _iteratorError;
	                        }
	                    }
	                }
	
	                headers.add('Total');
	                for (var uniqueKey in matchMaker) {
	                    if (matchMaker.hasOwnProperty(uniqueKey)) {
	                        for (var u in matchMaker[uniqueKey]) {
	                            if (matchMaker[uniqueKey].hasOwnProperty(u)) {
	                                headers.add(u.replace(/\./g, ''));
	                                docNameProps.add(u.replace(/\./g, ''));
	                            }
	                        }
	                    }
	                }
	                var _defaultRow = {};
	
	                var _iteratorNormalCompletion2 = true;
	                var _didIteratorError2 = false;
	                var _iteratorError2 = undefined;
	
	                try {
	                    for (var _iterator2 = headers[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	                        var h = _step2.value;
	
	                        if (h === 'Total') {
	                            _defaultRow[h] = 0;
	                        } else {
	                            _defaultRow[h] = '';
	                        }
	                    }
	                } catch (err) {
	                    _didIteratorError2 = true;
	                    _iteratorError2 = err;
	                } finally {
	                    try {
	                        if (!_iteratorNormalCompletion2 && _iterator2['return']) {
	                            _iterator2['return']();
	                        }
	                    } finally {
	                        if (_didIteratorError2) {
	                            throw _iteratorError2;
	                        }
	                    }
	                }
	
	                for (var uniqueKey in matchMaker) {
	                    if (matchMaker.hasOwnProperty(uniqueKey)) {
	                        for (var u in matchMaker[uniqueKey]) {
	                            if (matchMaker[uniqueKey].hasOwnProperty(u)) {
	                                if (!matchMakerTable[uniqueKey]) {
	                                    matchMakerTable[uniqueKey] = _angular2['default'].copy(_defaultRow);
	                                    var _iteratorNormalCompletion3 = true;
	                                    var _didIteratorError3 = false;
	                                    var _iteratorError3 = undefined;
	
	                                    try {
	                                        for (var _iterator3 = extractedEntityProperties[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
	                                            var entity = _step3.value;
	
	                                            matchMakerTable[uniqueKey][entity] = matchMaker[uniqueKey][u][0][entity];
	                                        }
	                                    } catch (err) {
	                                        _didIteratorError3 = true;
	                                        _iteratorError3 = err;
	                                    } finally {
	                                        try {
	                                            if (!_iteratorNormalCompletion3 && _iterator3['return']) {
	                                                _iterator3['return']();
	                                            }
	                                        } finally {
	                                            if (_didIteratorError3) {
	                                                throw _iteratorError3;
	                                            }
	                                        }
	                                    }
	
	                                    matchMakerTable[uniqueKey][matchMaker[uniqueKey][u][0].Document.replace(/\./g, '')] = 'True';
	                                    matchMakerTable[uniqueKey].Total++;
	                                } else {
	                                    matchMakerTable[uniqueKey][matchMaker[uniqueKey][u][0].Document.replace(/\./g, '')] = 'True';
	                                    matchMakerTable[uniqueKey].Total++;
	                                }
	                            }
	                        }
	                    }
	                }
	
	                matchMakerAll[bins[i]] = _lodash2['default'].values(matchMakerTable);
	            }
	
	            // build all bin
	            var allHeaders = new Set();
	
	            for (var matchMaker in matchMakerAll) {
	                if (matchMakerAll.hasOwnProperty(matchMaker)) {
	                    for (var i = 0, len = matchMakerAll[matchMaker].length; i < len; i++) {
	                        for (var _name in matchMakerAll[matchMaker][i]) {
	                            if (matchMakerAll[matchMaker][i].hasOwnProperty(_name)) {
	                                if (Array.from(entityProps).includes(_name)) {
	                                    allHeaders.add(_name);
	                                }
	                            }
	                        }
	                    }
	                }
	            }
	
	            allHeaders.add('Total');
	            var _iteratorNormalCompletion4 = true;
	            var _didIteratorError4 = false;
	            var _iteratorError4 = undefined;
	
	            try {
	                for (var _iterator4 = docNameProps[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
	                    var docName = _step4.value;
	
	                    allHeaders.add(docName);
	                }
	            } catch (err) {
	                _didIteratorError4 = true;
	                _iteratorError4 = err;
	            } finally {
	                try {
	                    if (!_iteratorNormalCompletion4 && _iterator4['return']) {
	                        _iterator4['return']();
	                    }
	                } finally {
	                    if (_didIteratorError4) {
	                        throw _iteratorError4;
	                    }
	                }
	            }
	
	            console.log(allHeaders);
	
	            var defaultRow = {};
	
	            var _iteratorNormalCompletion5 = true;
	            var _didIteratorError5 = false;
	            var _iteratorError5 = undefined;
	
	            try {
	                for (var _iterator5 = allHeaders[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
	                    var h = _step5.value;
	
	                    defaultRow[h] = '';
	                }
	            } catch (err) {
	                _didIteratorError5 = true;
	                _iteratorError5 = err;
	            } finally {
	                try {
	                    if (!_iteratorNormalCompletion5 && _iterator5['return']) {
	                        _iterator5['return']();
	                    }
	                } finally {
	                    if (_didIteratorError5) {
	                        throw _iteratorError5;
	                    }
	                }
	            }
	
	            matchMakerAll.All = [];
	            for (var bin in matchMakerAll) {
	                if (matchMakerAll.hasOwnProperty(bin)) {
	                    if (bin !== 'All') {
	                        for (var i = 0, len = matchMakerAll[bin].length; i < len; i++) {
	                            matchMakerAll.All = matchMakerAll.All.concat(_angular2['default'].merge(_angular2['default'].copy(defaultRow), matchMakerAll[bin][i]));
	                        }
	                    }
	                }
	            }
	
	            return matchMakerAll;
	        }
	    }, {
	        key: 'generateBinOverviewResultData',
	        value: function generateBinOverviewResultData() {
	            var output = {};
	
	            // loop through all of the documents
	            for (var docName in this.StorageService.activeDocuments) {
	                if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
	                    var results = this.StorageService.activeDocuments[docName].results;
	
	                    // loop through each bin in the result set of the document
	                    for (var binName in results) {
	                        if (results.hasOwnProperty(binName)) {
	                            // initialize the bin result count
	                            if (!output[binName] && binName !== 'CUSTOM_REGEX_INPUT_DIRECTIVE') {
	                                output[binName] = {
	                                    name: binName,
	                                    uniqueEntities: new Set(),
	                                    totalEntityCount: 0,
	                                    documentCount: 0
	                                };
	                            }
	
	                            if (results[binName].length) {
	                                for (var i = 0, len = results[binName].length; i < len; i++) {
	                                    // get sum of all entities across all documents
	                                    var entity = results[binName][i].mainGroup || results[binName][i][binName];
	                                    output[binName].uniqueEntities.add(results[binName][i][entity]);
	
	                                    // get total count of entities
	                                    output[binName].totalEntityCount += Number(results[binName][i].count);
	                                }
	                                // increment the document count
	                                output[binName].documentCount++;
	                            }
	                        }
	                    }
	                }
	            }
	
	            for (var binName in output) {
	                if (output.hasOwnProperty(binName)) {
	                    output[binName].uniqueEntityCount = output[binName].uniqueEntities.size;
	                    delete output[binName].uniqueEntities;
	                }
	            }
	
	            return output;
	        }
	    }, {
	        key: 'generateBinResultData',
	        value: function generateBinResultData(binName) {
	            var output = [];
	            var tempObj = {};
	            var extractedEntityProperties = [];
	
	            // loop through Storage to get all documents
	            for (var docName in this.StorageService.activeDocuments) {
	                tempObj[docName] = {};
	                if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
	                    // get to the selected bin
	                    var bin = _angular2['default'].copy(this.StorageService.activeDocuments[docName].results[binName]);
	
	                    if (bin && bin.length) {
	                        for (var i = 0, len = bin.length; i < len; i++) {
	                            // get all of the result properties that are not the static meta properties (we only need to do this once and we only need the first element)
	                            if (!extractedEntityProperties.length) {
	                                for (var prop in bin[0]) {
	                                    if (bin[0].hasOwnProperty(prop)) {
	                                        if (!this.resultObjectMetaProperties.includes(prop)) {
	                                            extractedEntityProperties.push(prop);
	                                        }
	                                    }
	                                }
	                            }
	
	                            // creating unique key to compare entities across documents
	                            var uniqueEntity = [];
	
	                            for (var propCounter = 0, propLength = extractedEntityProperties.length; propCounter < propLength; propCounter++) {
	                                uniqueEntity.push(bin[i][extractedEntityProperties[propCounter]]);
	                            }
	                            var uniqueEntityStr = uniqueEntity.join('|');
	
	                            if (!tempObj[docName][uniqueEntityStr]) {
	                                tempObj[docName][uniqueEntityStr] = bin[i];
	                                // add proper formatted properties for Count and Document for the table object
	                                tempObj[docName][uniqueEntityStr].Document = bin[i].docName;
	                                tempObj[docName][uniqueEntityStr].Count = bin[i].count;
	
	                                // this is a temporary solution, we should probably have a post-rip process to handle this type of processing
	                                if (binName === 'Locations') {
	                                    var geoConverter = new _geoConverter2['default'](bin[i].Locations);
	                                    tempObj[docName][uniqueEntityStr].MGRS = geoConverter.mgrs;
	                                    tempObj[docName][uniqueEntityStr].Lat = geoConverter.geo.lat;
	                                    tempObj[docName][uniqueEntityStr].Lon = geoConverter.geo.lon;
	                                }
	
	                                // remove unnecessary/incorrect properties from the result object
	                                for (var prop in bin[i]) {
	                                    if (bin[i].hasOwnProperty(prop)) {
	                                        if (this.resultObjectMetaProperties.includes(prop)) {
	                                            delete tempObj[docName][uniqueEntityStr][prop];
	                                        }
	                                    }
	                                }
	                            } else {
	                                tempObj[docName][uniqueEntityStr].Count += bin[i].count;
	                            }
	                        }
	                    }
	                }
	            }
	
	            // convert back to array of objects
	            for (var docName in tempObj) {
	                if (tempObj.hasOwnProperty(docName)) {
	                    for (var uniqueEnitity in tempObj[docName]) {
	                        if (tempObj[docName].hasOwnProperty(uniqueEnitity)) {
	                            output.push(tempObj[docName][uniqueEnitity]);
	                        }
	                    }
	                }
	            }
	
	            return output;
	        }
	    }, {
	        key: 'generateDataForExport',
	        value: function generateDataForExport() {
	            var output = [];
	            var tempObj = {};
	            var extractedEntityProperties = [];
	
	            // loop through Storage to get all documents
	            for (var docName in this.StorageService.activeDocuments) {
	                if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
	                    for (var binName in this.StorageService.activeDocuments[docName].results) {
	                        if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
	                            var bin = this.StorageService.activeDocuments[docName].results[binName];
	
	                            // get all of the result properties that are not the static meta properties (we only need to do this once and we only need the first element)
	                            if (!extractedEntityProperties.length) {
	                                for (var prop in bin[0]) {
	                                    if (bin[0].hasOwnProperty(prop)) {
	                                        if (!this.resultObjectMetaProperties.includes(prop)) {
	                                            extractedEntityProperties.push(prop);
	                                        }
	                                    }
	                                }
	                            }
	
	                            for (var i = 0, len = bin.length; i < len; i++) {
	                                // creating unique key to compare entities across documents
	                                var uniqueEntity = [];
	
	                                for (var propCounter = 0, propLength = extractedEntityProperties.length; propCounter < propLength; propCounter++) {
	                                    uniqueEntity.push(bin[i][extractedEntityProperties[propCounter]]);
	                                }
	                                var uniqueEntityStr = uniqueEntity.join('|');
	
	                                if (!tempObj[uniqueEntityStr]) {
	                                    tempObj[uniqueEntityStr] = bin[i];
	                                    // add proper formatted properties for Count and Document for the table object
	                                    tempObj[uniqueEntityStr].Document = bin[i].docName;
	                                    tempObj[uniqueEntityStr].Count = bin[i].count;
	
	                                    // this is a temporary solution, we should probably have a post-rip process to handle this type of processing
	                                    if (binName === 'Locations') {
	                                        var geoConverter = new _geoConverter2['default'](bin[i].Locations);
	                                        tempObj[uniqueEntityStr].MGRS = geoConverter.mgrs;
	                                        tempObj[uniqueEntityStr].Lat = geoConverter.geo.lat;
	                                        tempObj[uniqueEntityStr].Lon = geoConverter.geo.lon;
	                                    }
	
	                                    // remove unnecessary/incorrect properties from the result object
	                                    for (var prop in bin[i]) {
	                                        if (bin[i].hasOwnProperty(prop)) {
	                                            if (this.resultObjectMetaProperties.includes(prop)) {
	                                                delete tempObj[uniqueEntityStr][prop];
	                                            }
	                                        }
	                                    }
	                                } else {
	                                    tempObj[uniqueEntityStr].Count += bin[i].count;
	                                }
	                            }
	                        }
	                    }
	                    console.log(tempObj);
	                }
	            }
	
	            return output;
	        }
	    }]);
	
	    return RipItService;
	})();
	
	exports['default'] = RipItService;
	module.exports = exports['default'];

/***/ },

/***/ 324:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _appStateServiceJs = __webpack_require__(325);
	
	var _appStateServiceJs2 = _interopRequireDefault(_appStateServiceJs);
	
	var _docTextractServiceJs = __webpack_require__(326);
	
	var _docTextractServiceJs2 = _interopRequireDefault(_docTextractServiceJs);
	
	var _helpersServiceJs = __webpack_require__(416);
	
	var _helpersServiceJs2 = _interopRequireDefault(_helpersServiceJs);
	
	var _htmlTrextractServiceJs = __webpack_require__(417);
	
	var _htmlTrextractServiceJs2 = _interopRequireDefault(_htmlTrextractServiceJs);
	
	var _jemaLoggerServiceJs = __webpack_require__(419);
	
	var _jemaLoggerServiceJs2 = _interopRequireDefault(_jemaLoggerServiceJs);
	
	var _orderByKeyFilterJs = __webpack_require__(420);
	
	var _orderByKeyFilterJs2 = _interopRequireDefault(_orderByKeyFilterJs);
	
	var _parseParseJs = __webpack_require__(421);
	
	var _parseParseJs2 = _interopRequireDefault(_parseParseJs);
	
	var _pdfTextractServiceJs = __webpack_require__(423);
	
	var _pdfTextractServiceJs2 = _interopRequireDefault(_pdfTextractServiceJs);
	
	var _pptTextractServiceJs = __webpack_require__(426);
	
	var _pptTextractServiceJs2 = _interopRequireDefault(_pptTextractServiceJs);
	
	var _resultsExportServiceJs = __webpack_require__(427);
	
	var _resultsExportServiceJs2 = _interopRequireDefault(_resultsExportServiceJs);
	
	var _storageServiceJs = __webpack_require__(488);
	
	var _storageServiceJs2 = _interopRequireDefault(_storageServiceJs);
	
	var _tableServiceJs = __webpack_require__(490);
	
	var _tableServiceJs2 = _interopRequireDefault(_tableServiceJs);
	
	var _xlsTextractServiceJs = __webpack_require__(491);
	
	var _xlsTextractServiceJs2 = _interopRequireDefault(_xlsTextractServiceJs);
	
	var commonModule = _angular2['default'].module('app.common', [_parseParseJs2['default'].name]).filter('orderByKey', _orderByKeyFilterJs2['default']).service('AppStateService', _appStateServiceJs2['default']).service('DocTextractService', _docTextractServiceJs2['default']).service('HelpersService', _helpersServiceJs2['default']).service('HTMLTextractService', _htmlTrextractServiceJs2['default']).service('JemaLoggerService', _jemaLoggerServiceJs2['default']).service('PdfTextractService', _pdfTextractServiceJs2['default']).service('PptTextractService', _pptTextractServiceJs2['default']).service('ResultsExportService', _resultsExportServiceJs2['default']).service('StorageService', _storageServiceJs2['default']).service('TableService', _tableServiceJs2['default']).service('XlsTextractService', _xlsTextractServiceJs2['default']);
	
	exports['default'] = commonModule;
	module.exports = exports['default'];

/***/ },

/***/ 325:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var AppStateService = (function () {
	    AppStateService.$inject = ["$rootScope"];
	    function AppStateService($rootScope) {
	        'ngInject';
	
	        _classCallCheck(this, AppStateService);
	
	        this.$rootScope = $rootScope;
	
	        this.state = {};
	        this.selectedDocs = new Set();
	        this.initialize();
	
	        // this.watchers();
	    }
	
	    _createClass(AppStateService, [{
	        key: 'setState',
	        value: function setState(appState, value) {
	            if (value === undefined) {
	                throw Error('Error: You must set a value for this method.');
	            }
	            if (!this.state[appState]) {
	                this.state[appState] = value;
	            }
	            this.state[appState] = value;
	        }
	    }, {
	        key: 'getState',
	        value: function getState(appState) {
	            return this.state[appState];
	        }
	    }, {
	        key: 'removeState',
	        value: function removeState(appState) {
	            delete this.state[appState];
	        }
	
	        // needsRipping() {
	        //     return this.state.needs;
	        // }
	
	        // watchers() {
	        // this.$rootScope.$watch(() => {
	        //     return this.state.needsRipping;
	        // }, (newValue, oldValue) => {
	        //     this.state.needsToBeRipped = true;
	        // }, true);
	        // }
	
	        // hacks for testing
	    }, {
	        key: 'initialize',
	        value: function initialize() {
	            var _this = this,
	                _arguments = arguments;
	
	            window.toggleState = function (appState) {
	                var value = arguments.length <= 1 || arguments[1] === undefined ? !_this.getState(appState) : arguments[1];
	                return (function () {
	                    this.setState(appState, value);
	                    console.log(this.getState(appState));
	                }).apply(_this, _arguments);
	            };
	            window.viewAppState = function () {
	                console.log(_this.state);
	            };
	            window.viewSelectedDocuments = function () {
	                console.log(_this.selectedDocs);
	            };
	        }
	    }]);
	
	    return AppStateService;
	})();
	
	exports['default'] = AppStateService;
	module.exports = exports['default'];

/***/ },

/***/ 326:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _jszip = __webpack_require__(327);
	
	var _jszip2 = _interopRequireDefault(_jszip);
	
	var DocTextractService = (function () {
	    DocTextractService.$inject = ["$q"];
	    function DocTextractService($q) {
	        'ngInject';
	
	        _classCallCheck(this, DocTextractService);
	
	        this.$q = $q;
	    }
	
	    _createClass(DocTextractService, [{
	        key: 'getTextAsync',
	        value: function getTextAsync(input, fileName, fileExtension) {
	            var deferred = this.$q.defer();
	
	            if (fileExtension === 'docx') {
	                // docx parsing
	                var newZip = new _jszip2['default']();
	                newZip.load(input);
	                var strippedText = newZip.file('word/document.xml').asText().replace(/<w:p[\s\S]+?>/g, 'This_Is_The_Delimeter').replace(/<[^\>]*\>/g, ' ').replace(/\s+/g, ' ').replace(/This_Is_The_Delimeter(?:This_Is_The_Delimeter+)?/g, '\n\n').trim();
	
	                deferred.resolve({ fileName: fileName, extension: fileExtension, content: strippedText });
	            } else if (fileExtension === 'doc') {
	                // not yet supported
	                //return '';
	            }
	
	            return deferred.promise;
	        }
	    }, {
	        key: 'getText',
	        value: function getText(input, fileExtension) {
	            if (fileExtension === 'docx') {
	                // docx parsing
	                var newZip = new _jszip2['default']();
	                newZip.load(input);
	                var strippedText = newZip.file('word/document.xml').asText().replace(/<w:p[\s\S]+?>/g, 'This_Is_The_Delimeter').replace(/<[^\>]*\>/g, ' ').replace(/\s+/g, ' ').replace(/This_Is_The_Delimeter(?:This_Is_The_Delimeter+)?/g, '\n\n').trim();
	                return strippedText;
	            } else if (fileExtension === 'doc') {
	                // not yet supported
	                return '';
	            }
	        }
	    }]);
	
	    return DocTextractService;
	})();
	
	exports['default'] = DocTextractService;
	module.exports = exports['default'];

/***/ },

/***/ 346:
/***/ function(module, exports) {

	/* (ignored) */

/***/ },

/***/ 358:
/***/ function(module, exports) {

	/* (ignored) */

/***/ },

/***/ 373:
/***/ function(module, exports) {

	/* (ignored) */

/***/ },

/***/ 416:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
		value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _lodash = __webpack_require__(320);
	
	var _lodash2 = _interopRequireDefault(_lodash);
	
	var HelpersService = (function () {
		HelpersService.$inject = ["$q", "$timeout"];
		function HelpersService($q, $timeout) {
			'ngInject';
	
			_classCallCheck(this, HelpersService);
	
			this._ = _lodash2['default'];
			this.$q = $q;
			this.$timeout = $timeout;
		}
	
		_createClass(HelpersService, [{
			key: 'size',
			value: function size(data) {
				return this._.size(data);
			}
		}, {
			key: 'toArray',
			value: function toArray(obj) {
				return this._.toArray(obj);
			}
		}, {
			key: 'safeApply',
			value: function safeApply(fn) {
				this.$timeout(fn);
			}
		}, {
			key: 'objectsAreSame',
			value: function objectsAreSame(obj1, obj2, propertiesToCheck) {
				var objectsAreSame = true;
				for (var i = 0, len = propertiesToCheck.length; i < len; i++) {
					if (obj1[propertiesToCheck[i]] !== obj2[propertiesToCheck[i]]) {
						objectsAreSame = false;
						break;
					}
				}
				return objectsAreSame;
			}
		}, {
			key: 'emptyObject',
			value: function emptyObject(obj) {
				for (var key in obj) {
					if (obj.hasOwnProperty(key)) {
						delete obj[key];
					}
				}
			}
		}], [{
			key: 'csvToArray',
			value: function csvToArray(csv) {
				var deferred = this.$q.defer();
				var rows = csv.split("\n");
				var data = [];
	
				var regex = new RegExp('(?:"([^"]*(?:""[^"]*)*)"|([^",\r\n]+)|,(?=,))', 'g');
				var matches = false;
				var tmp = [];
	
				angular.forEach(rows, function (row) {
					matches = false;
					tmp.length = 0;
					while (matches = regex.exec(row)) {
						tmp.push(matches[1] || matches[2] || '');
					}
					if (row.substr(row.length - 1, 1) === ',') {
						tmp.push('');
					}
	
					data.push(tmp);
				});
	
				deferred.resolve(data);
	
				return deferred.promise;
			}
		}]);
	
		return HelpersService;
	})();
	
	exports['default'] = HelpersService;
	module.exports = exports['default'];

/***/ },

/***/ 417:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _html2TextJs = __webpack_require__(418);
	
	var _html2TextJs2 = _interopRequireDefault(_html2TextJs);
	
	var HTMLTextractService = (function () {
	    HTMLTextractService.$inject = ["$q"];
	    function HTMLTextractService($q) {
	        'ngInject';
	
	        _classCallCheck(this, HTMLTextractService);
	
	        this.$q = $q;
	    }
	
	    _createClass(HTMLTextractService, [{
	        key: 'getTextAsync',
	        value: function getTextAsync(input) {
	            var fileName = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];
	            var fileExtension = arguments.length <= 2 || arguments[2] === undefined ? '' : arguments[2];
	
	            var deferred = this.$q.defer();
	            var docText = {};
	
	            var text = this.getText(input);
	
	            deferred.resolve({ fileName: fileName, extension: fileExtension, content: text });
	
	            return deferred.promise;
	        }
	    }, {
	        key: 'getText',
	        value: function getText(input) {
	            var fileName = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];
	            var fileExtension = arguments.length <= 2 || arguments[2] === undefined ? '' : arguments[2];
	
	            return (0, _html2TextJs2['default'])(input);
	        }
	    }]);
	
	    return HTMLTextractService;
	})();
	
	exports['default'] = HTMLTextractService;
	module.exports = exports['default'];

/***/ },

/***/ 418:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	exports['default'] = htmlToText;
	
	function htmlToText(html, extensions) {
	    var text = html;
	
	    if (extensions && extensions['preprocessing']) text = extensions['preprocessing'](text);
	
	    text = text
	    // Remove line breaks
	    .replace(/(?:\n|\r\n|\r)/ig, " ")
	    // Remove content in script tags.
	    .replace(/<\s*script[^>]*>[\s\S]*?<\/script>/mig, "")
	    // Remove content in style tags.
	    .replace(/<\s*style[^>]*>[\s\S]*?<\/style>/mig, "")
	    // Remove content in comments.
	    .replace(/<!--.*?-->/mig, "")
	    // Remove !DOCTYPE
	    .replace(/<!DOCTYPE.*?>/ig, "");
	
	    /* I scanned http://en.wikipedia.org/wiki/HTML_element for all html tags.
	    I put those tags that should affect plain text formatting in two categories:
	    those that should be replaced with two newlines and those that should be
	    replaced with one newline. */
	
	    if (extensions && extensions['tagreplacement']) text = extensions['tagreplacement'](text);
	
	    var doubleNewlineTags = ['p', 'h[1-6]', 'dl', 'dt', 'dd', 'ol', 'ul', 'dir', 'address', 'blockquote', 'center', 'div', 'hr', 'pre', 'form', 'textarea', 'table'];
	
	    var singleNewlineTags = ['li', 'del', 'ins', 'fieldset', 'legend', 'tr', 'th', 'caption', 'thead', 'tbody', 'tfoot'];
	
	    for (var i = 0; i < doubleNewlineTags.length; i++) {
	        var r = RegExp('</?\\s*' + doubleNewlineTags[i] + '[^>]*>', 'ig');
	        text = text.replace(r, '\n\n');
	    }
	
	    for (var i = 0; i < singleNewlineTags.length; i++) {
	        var r = RegExp('<\\s*' + singleNewlineTags[i] + '[^>]*>', 'ig');
	        text = text.replace(r, '\n');
	    }
	
	    // Replace <br> and <br/> with a single newline
	    text = text.replace(/<\s*br[^>]*\/?\s*>/ig, '\n');
	
	    text = text
	    // Remove all remaining tags.
	    .replace(/(<([^>]+)>)/ig, "")
	    // Trim rightmost whitespaces for all lines
	    .replace(/([^\n\S]+)\n/g, "\n").replace(/([^\n\S]+)$/, "")
	    // Make sure there are never more than two
	    // consecutive linebreaks.
	    .replace(/\n{2,}/g, "\n\n")
	    // Remove newlines at the beginning of the text.
	    .replace(/^\n+/, "")
	    // Remove newlines at the end of the text.
	    .replace(/\n+$/, "")
	    // Decode HTML entities.
	    .replace(/&([^;]+);/g, decodeHtmlEntity);
	
	    if (extensions && extensions['postprocessing']) text = extensions['postprocessing'](text);
	
	    return text;
	}
	
	function decodeHtmlEntity(m, n) {
	    // Determine the character code of the entity. Range is 0 to 65535
	    // (characters in JavaScript are Unicode, and entities can represent
	    // Unicode characters).
	    var code;
	
	    // Try to parse as numeric entity. This is done before named entities for
	    // speed because associative array lookup in many JavaScript implementations
	    // is a linear search.
	    if (n.substr(0, 1) == '#') {
	        // Try to parse as numeric entity
	        if (n.substr(1, 1) == 'x') {
	            // Try to parse as hexadecimal
	            code = parseInt(n.substr(2), 16);
	        } else {
	            // Try to parse as decimal
	            code = parseInt(n.substr(1), 10);
	        }
	    } else {
	        // Try to parse as named entity
	        code = ENTITIES_MAP[n];
	    }
	
	    // If still nothing, pass entity through
	    return code === undefined || code === NaN ? '&' + n + ';' : String.fromCharCode(code);
	}
	
	var ENTITIES_MAP = {
	    'nbsp': 160,
	    'iexcl': 161,
	    'cent': 162,
	    'pound': 163,
	    'curren': 164,
	    'yen': 165,
	    'brvbar': 166,
	    'sect': 167,
	    'uml': 168,
	    'copy': 169,
	    'ordf': 170,
	    'laquo': 171,
	    'not': 172,
	    'shy': 173,
	    'reg': 174,
	    'macr': 175,
	    'deg': 176,
	    'plusmn': 177,
	    'sup2': 178,
	    'sup3': 179,
	    'acute': 180,
	    'micro': 181,
	    'para': 182,
	    'middot': 183,
	    'cedil': 184,
	    'sup1': 185,
	    'ordm': 186,
	    'raquo': 187,
	    'frac14': 188,
	    'frac12': 189,
	    'frac34': 190,
	    'iquest': 191,
	    'Agrave': 192,
	    'Aacute': 193,
	    'Acirc': 194,
	    'Atilde': 195,
	    'Auml': 196,
	    'Aring': 197,
	    'AElig': 198,
	    'Ccedil': 199,
	    'Egrave': 200,
	    'Eacute': 201,
	    'Ecirc': 202,
	    'Euml': 203,
	    'Igrave': 204,
	    'Iacute': 205,
	    'Icirc': 206,
	    'Iuml': 207,
	    'ETH': 208,
	    'Ntilde': 209,
	    'Ograve': 210,
	    'Oacute': 211,
	    'Ocirc': 212,
	    'Otilde': 213,
	    'Ouml': 214,
	    'times': 215,
	    'Oslash': 216,
	    'Ugrave': 217,
	    'Uacute': 218,
	    'Ucirc': 219,
	    'Uuml': 220,
	    'Yacute': 221,
	    'THORN': 222,
	    'szlig': 223,
	    'agrave': 224,
	    'aacute': 225,
	    'acirc': 226,
	    'atilde': 227,
	    'auml': 228,
	    'aring': 229,
	    'aelig': 230,
	    'ccedil': 231,
	    'egrave': 232,
	    'eacute': 233,
	    'ecirc': 234,
	    'euml': 235,
	    'igrave': 236,
	    'iacute': 237,
	    'icirc': 238,
	    'iuml': 239,
	    'eth': 240,
	    'ntilde': 241,
	    'ograve': 242,
	    'oacute': 243,
	    'ocirc': 244,
	    'otilde': 245,
	    'ouml': 246,
	    'divide': 247,
	    'oslash': 248,
	    'ugrave': 249,
	    'uacute': 250,
	    'ucirc': 251,
	    'uuml': 252,
	    'yacute': 253,
	    'thorn': 254,
	    'yuml': 255,
	    'quot': 34,
	    'amp': 38,
	    'lt': 60,
	    'gt': 62,
	    'OElig': 338,
	    'oelig': 339,
	    'Scaron': 352,
	    'scaron': 353,
	    'Yuml': 376,
	    'circ': 710,
	    'tilde': 732,
	    'ensp': 8194,
	    'emsp': 8195,
	    'thinsp': 8201,
	    'zwnj': 8204,
	    'zwj': 8205,
	    'lrm': 8206,
	    'rlm': 8207,
	    'ndash': 8211,
	    'mdash': 8212,
	    'lsquo': 8216,
	    'rsquo': 8217,
	    'sbquo': 8218,
	    'ldquo': 8220,
	    'rdquo': 8221,
	    'bdquo': 8222,
	    'dagger': 8224,
	    'Dagger': 8225,
	    'permil': 8240,
	    'lsaquo': 8249,
	    'rsaquo': 8250,
	    'euro': 8364
	};
	module.exports = exports['default'];

/***/ },

/***/ 419:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _lodash = __webpack_require__(320);
	
	var _lodash2 = _interopRequireDefault(_lodash);
	
	var JemaLoggerService = (function () {
	    JemaLoggerService.$inject = ["$q", "$timeout"];
	    function JemaLoggerService($q, $timeout) {
	        'ngInject';
	
	        _classCallCheck(this, JemaLoggerService);
	
	        this._ = _lodash2['default'];
	        this.$q = $q;
	        this.$timeout = $timeout;
	    }
	
	    // this is not angular-ish, and I don't like
	
	    _createClass(JemaLoggerService, [{
	        key: 'log',
	        value: function log(url) {
	            var iframe = document.getElementById('log-iframe');
	
	            if (iframe) {
	                document.body.removeChild(iframe);
	            }
	
	            var newIframe = document.createElement('iframe');
	            newIframe.id = 'log-iframe';
	            // iframe.style.display = "none";
	            newIframe.src = url;
	            document.body.appendChild(newIframe);
	        }
	    }]);
	
	    return JemaLoggerService;
	})();
	
	exports['default'] = JemaLoggerService;
	module.exports = exports['default'];

/***/ },

/***/ 420:
/***/ function(module, exports) {

	'use strict';
	
	orderByKey.$inject = ["$filter"];
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	exports['default'] = orderByKey;
	
	function orderByKey($filter) {
	    'ngInject';
	
	    return function (items, field, reverse) {
	        var keys = $filter('orderBy')(Object.keys(items), field, reverse),
	            obj = {};
	        keys.forEach(function (key) {
	            obj[key] = items[key];
	        });
	        return obj;
	    };
	}
	
	module.exports = exports['default'];

/***/ },

/***/ 421:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _parseProviderJs = __webpack_require__(422);
	
	var _parseProviderJs2 = _interopRequireDefault(_parseProviderJs);
	
	var parseModule = _angular2['default'].module('ParseProvider', []).provider('ParseProvider', _parseProviderJs2['default']).config(["ParseProviderProvider", "APP_SETTINGS", function (ParseProviderProvider, APP_SETTINGS) {
	    'ngInject';
	
	    ParseProviderProvider.setConfig(APP_SETTINGS);
	}]);
	exports['default'] = parseModule;
	module.exports = exports['default'];

/***/ },

/***/ 422:
/***/ function(module, exports, __webpack_require__) {

	/* eslint-disable object-shorthand, no-loop-func */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var ParseProvider = (function () {
	    function ParseProvider() {
	        'ngInject';
	
	        _classCallCheck(this, ParseProvider);
	    }
	
	    _createClass(ParseProvider, [{
	        key: '$get',
	        value: function $get() {
	            var config = this.config;
	            var regexSet = [];
	            return {
	                setRegexsUsed: function setRegexsUsed() {
	                    if (regexSet.length > 0) {
	                        regexSet = [];
	                    }
	                    this.iterate(config.regexPatterns);
	                },
	                iterate: function iterate(nodeItem) {
	                    var _this = this;
	
	                    _angular2['default'].forEach(nodeItem, function (node) {
	                        if (node.selected) {
	                            if (node.children) {
	                                _this.iterate(node.children);
	                            } else {
	                                regexSet.push(node);
	                            }
	                        }
	                    });
	                },
	                getRegexsUsed: function getRegexsUsed() {
	                    return regexSet;
	                },
	                ripIt: function ripIt(document) {
	                    var docName = arguments.length <= 1 || arguments[1] === undefined ? document.displayName : arguments[1];
	                    return (function () {
	                        var _this2 = this;
	
	                        var resultObject = {};
	                        var regexes = this.getRegexsUsed();
	                        regexes.forEach(function (selection) {
	                            var label = undefined;
	                            if (selection.bin) {
	                                label = selection.bin;
	                            } else {
	                                label = selection.label;
	                            }
	
	                            if (!resultObject[label]) {
	                                resultObject[label] = [];
	                            }
	                            var output = [];
	
	                            if (selection.options) {
	                                _angular2['default'].forEach(selection.options, function (option) {
	                                    var mainGroup = option.mainGroup ? option.mainGroup : selection.label;
	                                    if (option.defaults) {
	                                        output = output.concat(_this2.extract(document, option, selection));
	                                        if (config.normalizer[selection.label]) {
	                                            _angular2['default'].forEach(config.normalizer, function (value, key) {
	                                                if (key.toLowerCase() === selection.label.toLowerCase()) {
	                                                    _this2.normalizeResults(output, value, key);
	                                                }
	                                            });
	                                        }
	                                    } else {
	                                        _angular2['default'].forEach(selection.patterns, function (pattern) {
	                                            var mainGroup = pattern.mainGroup ? pattern.mainGroup : selection.label;
	                                            output = output.concat(_this2.extract(document, pattern, selection));
	                                            var test;
	                                        });
	                                        _angular2['default'].forEach(config.normalizer, function (value, key) {
	                                            if (key.toLowerCase() === selection.label.toLowerCase()) {
	                                                _this2.normalizeResults(output, value, key);
	                                            }
	                                        });
	                                    }
	                                });
	                            } else {
	                                _angular2['default'].forEach(selection.patterns, function (pattern) {
	                                    var mainGroup = pattern.mainGroup ? pattern.mainGroup : selection.label;
	                                    output = output.concat(_this2.extract(document, pattern, selection));
	                                    var test;
	                                });
	                                _angular2['default'].forEach(config.normalizer, function (value, key) {
	                                    if (key.toLowerCase() === selection.label.toLowerCase()) {
	                                        _this2.normalizeResults(output, value, key);
	                                    }
	                                });
	                            }
	
	                            resultObject[label] = resultObject[label].concat(output);
	                        });
	                        resultObject = this.enforceDominance(resultObject);
	                        return resultObject;
	                    }).apply(this, arguments);
	                },
	                // extract(pattern, input, labelTag, groups, docName, mainGroup) {
	                extract: function extract(documentObj, optionsObj, binOptionsObj) {
	                    var output = [];
	                    var match = undefined;
	                    var matchObj = {};
	
	                    var mainGroup = optionsObj.mainGroup ? optionsObj.mainGroup : binOptionsObj.label;
	                    var label = undefined;
	                    if (binOptionsObj.bin) {
	                        label = binOptionsObj.bin;
	                        mainGroup = binOptionsObj.bin;
	                    } else {
	                        label = binOptionsObj.label;
	                    }
	
	                    if (optionsObj.regex.global) {
	                        var indexes = new Set();
	                        while (match = optionsObj.regex.exec(documentObj.content)) {
	                            if (!matchObj[match[0]]) {
	                                matchObj[match[0]] = {};
	                                matchObj[match[0]].docName = documentObj.displayName;
	                                matchObj[match[0]].label = label;
	                                matchObj[match[0]].indexes = new Set();
	                                matchObj[match[0]].indexes.add(match.index);
	                                matchObj[match[0]].hitLength = match[0].length;
	                                matchObj[match[0]].mainGroup = mainGroup;
	
	                                matchObj[match[0]].count = matchObj[match[0]].indexes.size;
	                                if (optionsObj.groups) {
	                                    for (var i = 0, len = optionsObj.groups.length; i < len; i++) {
	                                        if (match[i + 1]) {
	                                            matchObj[match[0]][optionsObj.groups[i]] = match[i + 1].trim();
	                                        } else {
	                                            console.log('ERROR: Regex group mismatch with results.');
	                                        }
	                                    }
	                                } else {
	                                    matchObj[match[0]][label] = match[0].trim();
	                                }
	                            } else {
	                                matchObj[match[0]].indexes.add(match.index);
	                                matchObj[match[0]].count = matchObj[match[0]].indexes.size;
	                            }
	                        }
	                    } else {
	                        match = documentObj.content.match(pattern);
	                        if (!matchObj[match[0]]) {
	                            matchObj[match[0]] = {};
	                            matchObj[match[0]].indexes = new Set();
	                            matchObj[match[0]].indexes.add(match.index);
	                            matchObj[match[0]].docName = documentObj.displayName;
	                            matchObj[match[0]].label = label;
	                            matchObj[match[0]].indexes = [match.index];
	                            matchObj[match[0]].hitLength = match[0].length;
	                            matchObj[match[0]].mainGroup = mainGroup;
	
	                            matchObj[match[0]].count = matchObj[match[0]].indexes.size;
	                            if (binOptionsObj.label) {
	                                for (var i = 0, len = optionsObj.groups.length; i < len; i++) {
	                                    if (match[i]) {
	                                        matchObj[match[0]][optionsObj.groups[i]] = match[i + 1].trim();
	                                    } else {
	                                        console.log('ERROR: Regex group mismatch with results.');
	                                    }
	                                }
	                                //} else if(){
	                            } else {
	                                    matchObj[match[0]][label] = match[0].trim();
	                                }
	                        } else {
	                            matchObj[match[0]].indexes.add(match.index);
	                            matchObj[match[0]].count = matchObj[match[0]].indexes.size;
	                        }
	                    }
	
	                    _angular2['default'].forEach(matchObj, function (obj) {
	                        output.push(obj);
	                    });
	
	                    return output;
	                },
	                enforceDominance: function enforceDominance(docResult) {
	                    // for each category, check for dominance
	                    _angular2['default'].forEach(this.getRegexsUsed(), function (selection) {
	                        if (selection.dominant) {
	                            // if dominant globally
	                            if (selection.dominant === true) {
	                                // for each result in dominant category
	                                _angular2['default'].forEach(docResult[selection.label], function (dResult) {
	                                    // for each result in full results
	                                    var dominate = true;
	                                    _angular2['default'].forEach(docResult, function (lResult) {
	                                        // skip self
	                                        if (lResult.length > 0 && !lResult[0][selection.label]) {
	                                            var _loop = function (i) {
	                                                // loop through results indexes
	                                                _angular2['default'].forEach(lResult[i - 1].indexes, function (index) {
	                                                    // loop through dominant results indexes
	                                                    _angular2['default'].forEach(dResult.indexes, function (dIndex) {
	                                                        // if result index falls in dominant index
	                                                        if (index >= dIndex && index <= dIndex + dResult.hitLength) {
	                                                            // only allow one splice per result
	                                                            if (dominate) {
	                                                                // remove result index
	                                                                lResult.splice(i - 1, 1);
	                                                            }
	                                                            dominate = false;
	                                                        }
	                                                    });
	                                                });
	                                            };
	
	                                            // reverse loop through result category
	                                            for (var i = lResult.length; i > 0; i--) {
	                                                _loop(i);
	                                            }
	                                        }
	                                        dominate = true;
	                                    });
	                                });
	                                // if selected dominance
	                            } else if (Array.isArray(selection.dominant)) {
	                                    // for each result in dominant category
	                                    _angular2['default'].forEach(docResult[selection.label], function (dResult) {
	                                        var dominate = true;
	                                        _angular2['default'].forEach(selection.dominant, function (dominantSelected) {
	                                            var sub = docResult[dominantSelected];
	                                            if (sub) {
	                                                var _loop2 = function (i) {
	                                                    // loop through results indexes
	                                                    _angular2['default'].forEach(sub[i - 1].indexes, function (index) {
	                                                        // loop through dominant results indexes
	                                                        _angular2['default'].forEach(dResult.indexes, function (dIndex) {
	                                                            // if result index falls in dominant index
	                                                            if (index >= dIndex && index <= dIndex + dResult.hitLength) {
	                                                                // only allow one splice per result
	                                                                if (dominate) {
	                                                                    // remove result index
	                                                                    sub.splice(i - 1, 1);
	                                                                }
	                                                                dominate = false;
	                                                            }
	                                                        });
	                                                    });
	                                                };
	
	                                                for (var i = sub.length; i > 0; i--) {
	                                                    _loop2(i);
	                                                }
	                                            }
	                                            dominate = true;
	                                        });
	                                    });
	                                }
	                        }
	                    });
	                    return docResult;
	                },
	                normalizeResults: function normalizeResults(unNormalized, normalizePatterns, valName) {
	                    // for each category hit
	                    _angular2['default'].forEach(unNormalized, function (uNameHit) {
	                        // for each normalizer pattern
	                        _angular2['default'].forEach(normalizePatterns, function (normRegex, finalFormat) {
	                            var builtRegexSet = new Set();
	                            // build regex
	                            _angular2['default'].forEach(normRegex, function (normRegexMember) {
	                                builtRegexSet.add('\\b' + normRegexMember + '\\b');
	                            });
	
	                            // remove ending pipe |
	                            var builtRegex = Array.from(builtRegexSet).join('|');
	                            // create regex
	                            var normPattern = new RegExp(builtRegex, 'ig');
	                            // grab value
	                            var value = uNameHit[valName];
	                            // will replace if matches
	                            uNameHit[valName] = value.replace(normPattern, finalFormat);
	                        });
	                    });
	                    return unNormalized;
	                }
	            };
	        }
	    }, {
	        key: 'setConfig',
	        value: function setConfig(config) {
	            this.config = config;
	        }
	    }]);
	
	    return ParseProvider;
	})();
	
	exports['default'] = ParseProvider;
	module.exports = exports['default'];

/***/ },

/***/ 423:
/***/ function(module, exports, __webpack_require__) {

	/* eslint-disable no-undef,no-loop-func */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _pdfjsDist = __webpack_require__(424);
	
	var _pdfjsDist2 = _interopRequireDefault(_pdfjsDist);
	
	var PdfTextractService = (function () {
	    PdfTextractService.$inject = ["$q"];
	    function PdfTextractService($q) {
	        'ngInject';
	
	        _classCallCheck(this, PdfTextractService);
	
	        this.$q = $q;
	    }
	
	    _createClass(PdfTextractService, [{
	        key: 'getTextAsync',
	        value: function getTextAsync(input, fileName, fileExtension) {
	            var deferred = this.$q.defer();
	            var docText = {};
	            _pdfjsDist2['default'].getDocument(input).then(function (pdfDocument) {
	                var _loop = function (_pageNum) {
	                    pdfDocument.getPage(_pageNum).then(function (page) {
	                        page.getTextContent().then(function (textContent) {
	                            var pageText = [];
	                            textContent.items.forEach(function (textItem) {
	                                pageText.push(textItem.str);
	                            });
	                            pageText = pageText.join(' ');
	                            docText[_pageNum] = pageText;
	                            if (Object.keys(docText).length === pdfDocument.numPages) {
	                                // we are done
	                                var docArr = [];
	                                for (_pageNum = 1; _pageNum <= pdfDocument.numPages; _pageNum++) {
	                                    docArr.push(docText[_pageNum]);
	                                }
	                                var text = docArr.join('\n\n');
	                                deferred.resolve({ fileName: fileName, extension: fileExtension, content: text });
	                            }
	                        });
	                    });
	                    pageNum = _pageNum;
	                };
	
	                for (var pageNum = 1; pageNum <= pdfDocument.numPages; pageNum++) {
	                    _loop(pageNum);
	                }
	            })['catch'](function (er) {
	                console.log(er);
	                deferred.reject(er);
	            });
	
	            return deferred.promise;
	        }
	    }]);
	
	    return PdfTextractService;
	})();
	
	exports['default'] = PdfTextractService;
	module.exports = exports['default'];

/***/ },

/***/ 426:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _jszip = __webpack_require__(327);
	
	var _jszip2 = _interopRequireDefault(_jszip);
	
	var PptTextractService = (function () {
	    PptTextractService.$inject = ["$q"];
	    function PptTextractService($q) {
	        'ngInject';
	
	        _classCallCheck(this, PptTextractService);
	
	        this.$q = $q;
	    }
	
	    _createClass(PptTextractService, [{
	        key: 'getText',
	        value: function getText(input, fileExtension) {
	            if (fileExtension === 'pptx') {
	                // pptx ripping
	                var newZip = new _jszip2['default']();
	                newZip.load(input);
	
	                var strippedText = [];
	                for (var key in newZip.files) {
	                    if (key.indexOf('ppt/slides/slide') !== -1) {
	                        strippedText.push(newZip.file(key).asText().replace(/<[^\>]*\>/g, ' '));
	                    }
	                }
	                strippedText = strippedText.join('\n\n');
	                return strippedText.replace(/\s+/g, ' ');
	            } else if (fileExtension === 'ppt') {
	                // not yet supported
	                return '';
	            }
	        }
	    }, {
	        key: 'getTextAsync',
	        value: function getTextAsync(input, fileName, fileExtension) {
	            var deferred = this.$q.defer();
	
	            if (fileExtension === 'pptx') {
	                // pptx ripping
	                var newZip = new _jszip2['default']();
	                newZip.load(input);
	
	                var strippedText = [];
	                for (var key in newZip.files) {
	                    if (key.indexOf('ppt/slides/slide') !== -1) {
	                        strippedText.push(newZip.file(key).asText().replace(/<[^\>]*\>/g, ' '));
	                    }
	                }
	                strippedText = strippedText.join('\n\n');
	                strippedText = strippedText.replace(/\s+/g, ' ');
	
	                deferred.resolve({ fileName: fileName, extension: fileExtension, content: strippedText });
	            } else if (fileExtension === 'ppt') {
	                // not yet supported
	                // return '';
	            }
	
	            return deferred.promise;
	        }
	    }]);
	
	    return PptTextractService;
	})();
	
	exports['default'] = PptTextractService;
	module.exports = exports['default'];

/***/ },

/***/ 427:
/***/ function(module, exports, __webpack_require__) {

	// import kmlConverter from './kml-converter/kml.converter.service.js';
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _xlsxXlsxJs = __webpack_require__(428);
	
	var _xlsxXlsxJs2 = _interopRequireDefault(_xlsxXlsxJs);
	
	var _filesaverJs = __webpack_require__(486);
	
	var _filesaverJs2 = _interopRequireDefault(_filesaverJs);
	
	var ResultsExportService = (function () {
	    ResultsExportService.$inject = ["$q", "RipItService", "StorageService"];
	    function ResultsExportService($q, RipItService, StorageService) {
	        'ngInject';
	
	        _classCallCheck(this, ResultsExportService);
	
	        this.$q = $q;
	        this.RipItService = RipItService;
	        this.StorageService = StorageService;
	        this.XLSX = _xlsxXlsxJs2['default'];
	    }
	
	    _createClass(ResultsExportService, [{
	        key: 'toANB',
	        value: function toANB(data) {
	            //
	        }
	    }, {
	        key: 'toKML',
	        value: function toKML(data) {
	            // console.log(this.StorageService.activeDocuments);
	        }
	    }, {
	        key: 'toXLSX',
	        value: function toXLSX() {
	            function Workbook() {
	                if (!(this instanceof Workbook)) {
	                    return new Workbook();
	                }
	                this.SheetNames = [];
	                this.Sheets = {};
	            }
	
	            var bins = this.RipItService.binsList;
	
	            var binData = {};
	
	            for (var i = 0, len = bins.length; i < len; i++) {
	                binData[bins[i].name] = this.RipItService.generateBinResultData(bins[i].name);
	            }
	
	            var workbook = new Workbook();
	
	            for (var bin in binData) {
	                if (binData.hasOwnProperty(bin)) {
	                    if (binData[bin]) {
	                        var worksheet = this.sheetFromArrayOfArrays(this.arrOobjectToArrArray(binData[bin]), null);
	                    }
	
	                    /* add worksheet to workbook */
	                    workbook.SheetNames.push(bin);
	                    workbook.Sheets[bin] = worksheet;
	                }
	            }
	
	            var workbookOutput = _xlsxXlsxJs2['default'].write(workbook, { bookType: 'xlsx', bookSST: true, type: 'binary' });
	            _filesaverJs2['default'].saveAs(new Blob([this.s2ab(workbookOutput)], { type: "application/octet-stream" }), 'RipIt-' + Date.now().toString().substr(-6) + '.xlsx');
	        }
	    }, {
	        key: 'arrOobjectToArrArray',
	        value: function arrOobjectToArrArray(data) {
	            var outerArr = [];
	            if (data && data[0]) {
	                var headers = Object.keys(data[0]);
	                outerArr.push(headers);
	
	                for (var i = 0, len = data.length; i < len; i++) {
	                    var row = [];
	                    for (var d in data[i]) {
	                        row.push(data[i][d]);
	                    }
	                    outerArr.push(row);
	                }
	            }
	            return outerArr;
	        }
	    }, {
	        key: 'datenum',
	        value: function datenum(v, date1904) {
	            if (date1904) {
	                v += 1462;
	            }
	            var epoch = Date.parse(v);
	            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
	        }
	    }, {
	        key: 's2ab',
	        value: function s2ab(s) {
	            var buf = new ArrayBuffer(s.length);
	            var view = new Uint8Array(buf);
	            for (var i = 0; i !== s.length; ++i) {
	                view[i] = s.charCodeAt(i) & 0xFF;
	            }
	            return buf;
	        }
	    }, {
	        key: 'sheetFromArrayOfArrays',
	        value: function sheetFromArrayOfArrays(data, opts) {
	            var worksheet = {};
	            var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
	            for (var R = 0; R !== data.length; ++R) {
	                for (var C = 0; C !== data[R].length; ++C) {
	                    if (range.s.r > R) {
	                        range.s.r = R;
	                    }
	                    if (range.s.c > C) {
	                        range.s.c = C;
	                    }
	                    if (range.e.r < R) {
	                        range.e.r = R;
	                    }
	                    if (range.e.c < C) {
	                        range.e.c = C;
	                    }
	                    var cell = { v: data[R][C] };
	                    if (cell.v === null) {
	                        continue;
	                    }
	                    var cell_ref = this.XLSX.utils.encode_cell({ c: C, r: R });
	
	                    if (typeof cell.v === 'number') {
	                        cell.t = 'n';
	                    } else if (typeof cell.v === 'boolean') {
	                        cell.t = 'b';
	                    } else if (cell.v instanceof Date) {
	                        cell.t = 'n';cell.z = this.XLSX.SSF._table[14];
	                        cell.v = this.datenum(cell.v);
	                    } else {
	                        cell.t = 's';
	                    }
	                    worksheet[cell_ref] = cell;
	                }
	            }
	            if (range.s.c < 10000000) {
	                worksheet['!ref'] = this.XLSX.utils.encode_range(range);
	            }
	            return worksheet;
	        }
	    }]);
	
	    return ResultsExportService;
	})();
	
	exports['default'] = ResultsExportService;
	module.exports = exports['default'];

/***/ },

/***/ 472:
/***/ function(module, exports) {

	/* (ignored) */

/***/ },

/***/ 488:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _dataRipitSampleDocumentTxt = __webpack_require__(489);
	
	var _dataRipitSampleDocumentTxt2 = _interopRequireDefault(_dataRipitSampleDocumentTxt);
	
	var StorageService = (function () {
	    StorageService.$inject = ["$rootScope", "AppStateService", "HelpersService"];
	    function StorageService($rootScope, AppStateService, HelpersService) {
	        'ngInject';
	
	        _classCallCheck(this, StorageService);
	
	        this.$rootScope = $rootScope;
	        this.AppStateService = AppStateService;
	        this.HelpersService = HelpersService;
	
	        this.documents = {};
	        this.activeDocuments = {};
	
	        this.count = {
	            activeDocument: 0,
	            totalDocument: 0
	        };
	
	        this.sampleDocument = {
	            'Sample Document': {
	                name: 'Sample Document',
	                displayName: 'Sample Document',
	                content: _dataRipitSampleDocumentTxt2['default'],
	                needsParsing: true,
	                uploaded: false,
	                valid: true,
	                selected: false
	            }
	        };
	
	        this.initialize();
	        this.watchers();
	        this.listeners();
	    }
	
	    _createClass(StorageService, [{
	        key: 'getActiveDocuments',
	        value: function getActiveDocuments() {
	            var _this = this;
	
	            this.HelpersService.emptyObject(this.activeDocuments);
	            if (this.AppStateService.selectedDocs.size) {
	                this.AppStateService.selectedDocs.forEach(function (docName) {
	                    _this.activeDocuments[docName] = _this.documents[docName];
	                });
	            } else {
	                // show all tabs as selected when none are selected
	                for (var docName in this.documents) {
	                    if (this.documents.hasOwnProperty(docName)) {
	                        if (this.documents[docName].valid) {
	                            this.activeDocuments[docName] = this.documents[docName];
	                        }
	                    }
	                }
	            }
	            return this.activeDocuments;
	        }
	    }, {
	        key: 'setData',
	        value: function setData(documents) {
	            this.data = documents;
	        }
	    }, {
	        key: 'getData',
	        value: function getData() {
	            return this.documents;
	        }
	    }, {
	        key: 'listeners',
	        value: function listeners() {
	            var _this2 = this;
	
	            this.$rootScope.$on('files-changed', function () {
	                _this2.activeDocuments = _this2.getActiveDocuments();
	                // this.AppStateService.setState('needsRipping', true);
	            });
	        }
	    }, {
	        key: 'watchers',
	        value: function watchers() {}
	
	        // hacks for testing
	    }, {
	        key: 'initialize',
	        value: function initialize() {
	            var _this3 = this;
	
	            window.activeDocuments = function () {
	                console.log(_this3.getActiveDocuments());
	            };
	            window.viewDocuments = function () {
	                console.log(_this3.documents);
	                return _this3.documents;
	            };
	        }
	    }]);
	
	    return StorageService;
	})();
	
	exports['default'] = StorageService;
	module.exports = exports['default'];

/***/ },

/***/ 489:
/***/ function(module, exports) {

	module.exports = "Welcome to the RipIt tool. Paste any and all text into this pane to extract various data fields.\n\nEmail: Any email following the format address@domain.com excluding *.gov and *.mil\n    Examples:\n    test@gmail.com.\n    example@gmail.com example@gmail.com example@gmail.com\n    joeschmoe@gmail.com\n\nMSISDN/IMSI/IMEI: Any number found to be between 9 and 16 characters in length that does not begin with 1\n    Examples:\n    0700088836\n    Bad guy X uses number 1234567890125\n\nSocial Media User IDs: Any twitter address beginning with the @ symbol, Any Facebook ID number\n    Examples:\n    @example Tweet to example\n    Mention @example\n    @test@some_guy\n    FB ID 1234567890\n    FACEBOOK ID 2345678901\n\nHashTags: Any hashtag beginning with the # symbol and followed by 5 or more characters\n    Examples:\n    #winning#tigerblood\n    #tigerblood\n\nLocations:\n    Examples:\n    MGRS 42SVC1271622605, must by 8 or 10 digits\n    DD 33.639700°  68.058806°\n    DD 53.12345 68.12345\n    DMS 33°38\\'22.92\"N  68° 3\\'31.70\"E\n    Degrees, decimal minutes 33° 38.382\\'N 68° 3.528\\'E\n    DMS (FBI) 361338.17N/363220.27E\n    DMS (NSA) 361033N 0364028E\n\nTPN Numbers: Any number preceded by TPN or TIDE Number\n    Examples:\n    TPN 12345679\n    TPN: 987654321\n    TIDE Number 9876543210\n\nObjective Names: Any set of capital letters following the term OBJ\n    Examples:\n    OBJ VOLTRON\n\nReport Serial Numbers: Designed to return only the serial number for the report, not report mentions\n    Examples:\n    SERIAL:  1/AB/111111-11\n    SERIAL: TDX-111/111111-11\n    SUBJECT: (U) IIR 1 222 3333 44\n\nWorld Wide Web Addresses:\n    www.google.com\n    http://www.cnn.com/us\n    https://jquery.com/\n\nTowers:\n    123.123.1234.12345\n    123.123.98765.98765\n    987-654-3210-98765\n    987-654-32109-87654\n"

/***/ },

/***/ 490:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var TableService = (function () {
	    function TableService() {
	        'ngInject';
	
	        _classCallCheck(this, TableService);
	
	        this.initialize();
	        this.watchers();
	        this.listeners();
	    }
	
	    _createClass(TableService, [{
	        key: 'initialize',
	        value: function initialize() {
	            var columnDefs = [];
	            this.defaultGridOptions = {
	                columnDefs: columnDefs,
	                rowData: [],
	                enableFilter: true,
	                enableSorting: true,
	                enableColResize: true
	            };
	        }
	    }, {
	        key: 'listeners',
	        value: function listeners() {
	            //
	        }
	    }, {
	        key: 'watchers',
	        value: function watchers() {
	            //
	        }
	    }, {
	        key: 'generateTableHeaders',
	        value: function generateTableHeaders(data, sortObj) {
	            var header = [];
	            if (data && data[0]) {
	                for (var d in data[0]) {
	                    if (data[0].hasOwnProperty(d)) {
	                        var row = {
	                            headerName: d,
	                            field: d
	                        };
	
	                        if (sortObj && sortObj.colName === d) {
	                            row.sortingOrder = ['asc', 'desc', null];
	                        }
	
	                        header.push(row);
	                    }
	                }
	            }
	            return header;
	        }
	    }]);
	
	    return TableService;
	})();
	
	exports['default'] = TableService;
	module.exports = exports['default'];

/***/ },

/***/ 491:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _xlsxXlsxJs = __webpack_require__(428);
	
	var _xlsxXlsxJs2 = _interopRequireDefault(_xlsxXlsxJs);
	
	var XlsTextractService = (function () {
	    XlsTextractService.$inject = ["$q"];
	    function XlsTextractService($q) {
	        'ngInject';
	
	        _classCallCheck(this, XlsTextractService);
	
	        this.$q = $q;
	    }
	
	    _createClass(XlsTextractService, [{
	        key: 'getTextAsync',
	        value: function getTextAsync(input, fileName, fileExtension) {
	            var deferred = this.$q.defer();
	
	            var unstructuredResult = [];
	            if (fileExtension === 'xlsx' || fileExtension === 'xls') {
	                (function () {
	                    /*    xlsx parsing               */
	                    var workbook = _xlsxXlsxJs2['default'].read(input, { type: 'binary' });
	
	                    var sheetNameList = workbook.SheetNames;
	                    sheetNameList.forEach(function (y) {
	                        /* iterate through sheets */
	                        var worksheet = workbook.Sheets[y];
	                        var csv = _xlsxXlsxJs2['default'].utils.sheet_to_csv(worksheet);
	
	                        unstructuredResult.push(csv);
	                    });
	                })();
	            }
	
	            unstructuredResult = unstructuredResult.join('\n\n');
	
	            deferred.resolve({ fileName: fileName, extension: fileExtension, content: unstructuredResult });
	
	            return deferred.promise;
	        }
	    }]);
	
	    return XlsTextractService;
	})();
	
	exports['default'] = XlsTextractService;
	module.exports = exports['default'];

/***/ },

/***/ 492:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _documentDocumentJs = __webpack_require__(493);
	
	var _documentDocumentJs2 = _interopRequireDefault(_documentDocumentJs);
	
	var _fileManagerFileManagerJs = __webpack_require__(503);
	
	var _fileManagerFileManagerJs2 = _interopRequireDefault(_fileManagerFileManagerJs);
	
	var _resultOutputMatchMakerMatchMakerJs = __webpack_require__(516);
	
	var _resultOutputMatchMakerMatchMakerJs2 = _interopRequireDefault(_resultOutputMatchMakerMatchMakerJs);
	
	var _navbarNavbarJs = __webpack_require__(522);
	
	var _navbarNavbarJs2 = _interopRequireDefault(_navbarNavbarJs);
	
	var _queryBuilderQueryBuilderJs = __webpack_require__(528);
	
	var _queryBuilderQueryBuilderJs2 = _interopRequireDefault(_queryBuilderQueryBuilderJs);
	
	var _resultOutputResultOutputJs = __webpack_require__(545);
	
	var _resultOutputResultOutputJs2 = _interopRequireDefault(_resultOutputResultOutputJs);
	
	var _resultOutputResultsResultsJs = __webpack_require__(551);
	
	var _resultOutputResultsResultsJs2 = _interopRequireDefault(_resultOutputResultsResultsJs);
	
	var _ripItActionsRipItActionsJs = __webpack_require__(639);
	
	var _ripItActionsRipItActionsJs2 = _interopRequireDefault(_ripItActionsRipItActionsJs);
	
	var _treeTreeJs = __webpack_require__(645);
	
	var _treeTreeJs2 = _interopRequireDefault(_treeTreeJs);
	
	var _treeCustomRegexInputCustomRegexInputJs = __webpack_require__(654);
	
	var _treeCustomRegexInputCustomRegexInputJs2 = _interopRequireDefault(_treeCustomRegexInputCustomRegexInputJs);
	
	var componentModule = _angular2['default'].module('app.components', [_documentDocumentJs2['default'].name, _fileManagerFileManagerJs2['default'].name, _resultOutputMatchMakerMatchMakerJs2['default'].name, _navbarNavbarJs2['default'].name, _queryBuilderQueryBuilderJs2['default'].name, _resultOutputResultOutputJs2['default'].name, _resultOutputResultsResultsJs2['default'].name, _ripItActionsRipItActionsJs2['default'].name, _treeTreeJs2['default'].name, _treeCustomRegexInputCustomRegexInputJs2['default'].name]);
	
	exports['default'] = componentModule;
	module.exports = exports['default'];

/***/ },

/***/ 493:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	__webpack_require__(494);
	
	__webpack_require__(496);
	
	var _commonCommonJs = __webpack_require__(324);
	
	var _commonCommonJs2 = _interopRequireDefault(_commonCommonJs);
	
	var _documentComponentJs = __webpack_require__(498);
	
	var _documentComponentJs2 = _interopRequireDefault(_documentComponentJs);
	
	var documentModule = _angular2['default'].module('document', [_commonCommonJs2['default'].name, 'contenteditable']).directive('document', _documentComponentJs2['default']);
	
	exports['default'] = documentModule;
	module.exports = exports['default'];

/***/ },

/***/ 498:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _documentHtml = __webpack_require__(499);
	
	var _documentHtml2 = _interopRequireDefault(_documentHtml);
	
	var _documentControllerJs = __webpack_require__(500);
	
	var _documentControllerJs2 = _interopRequireDefault(_documentControllerJs);
	
	__webpack_require__(501);
	
	var documentComponent = function documentComponent() {
	    return {
	        template: _documentHtml2['default'],
	        controller: _documentControllerJs2['default'],
	        restrict: 'E',
	        controllerAs: 'vm',
	        scope: {},
	        bindToController: true
	    };
	};
	
	exports['default'] = documentComponent;
	module.exports = exports['default'];

/***/ },

/***/ 499:
/***/ function(module, exports) {

	module.exports = "<md-card>\n    <div class=\"section-headers\">Documents</div>\n    <md-card-content class=\"no-padding\">\n        <md-tabs md-border-bottom md-autoselect md-stretch-tabs=\"never\" class=\"document-container-inner-height\">\n            <md-tab ng-repeat=\"document in vm.StorageService.activeDocuments\" ng-disabled=\"vm.disableTab(document)\" class=\"documents-container\">\n                <md-tab-label class=\"no-select\">\n                    {{document.displayName}}\n                </md-tab-label>\n                <md-tab-body>\n\n                    <span ng-if=\"vm.AppStateService.getState('editMode')\" class=\"button-container\">\n                        <md-button ng-click=\"vm.cancel()\" class=\"md-raised document-button\">\n                            Cancel\n                        </md-button>\n                        <md-button ng-disabled=\"!document.content.trim().length\" ng-click=\"vm.save(document.content)\" class=\"md-raised document-button\">\n                            Save\n                        </md-button>\n                    </span>\n\n                    <div ng-model=\"document.content\"\n                        contenteditable=\"{{vm.AppStateService.getState('editMode') ? vm.AppStateService.getState('editMode') : 'false'}}\"\n                        strip-br=\"false\"\n                        no-line-breaks=\"false\"\n                        select-non-editable=\"false\"\n                        move-caret-to-end-on-change=\"true\"\n                        strip-tag=\"true\"\n\n                        style=\"height:calc(60vh - 113px);white-space:pre\"\n                        ng-class=\"{'no-edit-mode':!vm.AppStateService.getState('editMode'), 'edit-mode':vm.AppStateService.getState('editMode')}\"></div>\n                </md-tab-body>\n            </md-tab>\n        </md-tabs>\n    </md-card-content>\n</md-card>"

/***/ },

/***/ 500:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var DocumentController = (function () {
	    DocumentController.$inject = ["$rootScope", "AppStateService", "FileManagerService", "HTMLTextractService", "StorageService", "$scope"];
	    function DocumentController($rootScope, AppStateService, FileManagerService, HTMLTextractService, StorageService, $scope) {
	        'ngInject';
	
	        _classCallCheck(this, DocumentController);
	
	        this.$rootScope = $rootScope;
	        this.AppStateService = AppStateService;
	        this.FileManagerService = FileManagerService;
	        this.HTMLTextractService = HTMLTextractService;
	        this.StorageService = StorageService;
	        this.$scope = $scope;
	
	        this.listeners();
	        this.watchers();
	    }
	
	    _createClass(DocumentController, [{
	        key: 'save',
	        value: function save(content) {
	            // this.HTMLTextractService.getTextAsync(content).then((text) => {
	            //     this.StorageService.documents[this.AppStateService.getState('documentToEdit')].content = text.content;
	            //     this.AppStateService.setState('editBackup', false);
	            //     this.AppStateService.setState('editContent', false);
	            //     this.AppStateService.setState('editMode', false);
	            //     this.AppStateService.setState('documentToEdit', false);
	            //     // this.AppStateService.setState('needsRipping', false);
	            // });
	            this.StorageService.documents[this.AppStateService.getState('documentToEdit')].content = content;
	            this.AppStateService.setState('editBackup', false);
	            this.AppStateService.setState('editContent', false);
	            this.AppStateService.setState('editMode', false);
	            this.AppStateService.setState('documentToEdit', false);
	        }
	    }, {
	        key: 'cancel',
	        value: function cancel(document) {
	            if (this.AppStateService.getState('editBackup')) {
	                this.StorageService.documents[this.AppStateService.getState('documentToEdit')].content = this.AppStateService.getState('editBackup');
	                this.AppStateService.setState('editBackup', false);
	                this.AppStateService.setState('editcontent', false);
	                this.AppStateService.setState('editMode', false);
	                this.AppStateService.setState('documentToEdit', false);
	            } else {
	                delete this.StorageService.documents[this.AppStateService.getState('documentToEdit')];
	                this.AppStateService.selectedDocs['delete'](this.AppStateService.getState('documentToEdit'));
	                this.AppStateService.setState('editMode', false);
	                this.AppStateService.setState('documentToEdit', false);
	            }
	            this.$rootScope.$broadcast('files-changed');
	        }
	    }, {
	        key: 'handlePaste',
	        value: function handlePaste(evt, document) {
	            document.content = this.HTMLTextractService.getText(evt.clipboardData.getData('text/plain'));
	            console.log(document.content);
	
	            // var item = evt.clipboardData.items[0];
	            // item.getAsString((data) => {
	            //     document.content = this.HTMLTextractService.getText(data);
	            // });
	
	            return document;
	        }
	    }, {
	        key: 'disableTab',
	        value: function disableTab(document) {
	            return this.AppStateService.getState('editMode') && this.AppStateService.documentToEdit !== document.name;
	        }
	    }, {
	        key: 'closeTab',
	        value: function closeTab(document) {
	            var index = this.AppStateService.selectedDocs.indexOf(document.name);
	            this.AppStateService.selectedDocs.splice(index, 1);
	            this.FileManagerService.deselectDocument(document);
	        }
	    }, {
	        key: 'listeners',
	        value: function listeners() {
	            //
	        }
	    }, {
	        key: 'watchers',
	        value: function watchers() {
	            //
	        }
	    }]);
	
	    return DocumentController;
	})();
	
	exports['default'] = DocumentController;
	module.exports = exports['default'];

/***/ },

/***/ 501:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(502);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(305)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./document.scss", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./document.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 502:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(304)();
	// imports
	
	
	// module
	exports.push([module.id, ".red {\n  background-color: red; }\n\n.yellow {\n  background-color: yellow; }\n\n.lightblue {\n  background-color: lightblue; }\n\n.green {\n  background-color: green; }\n\n[contenteditable] {\n  overflow: auto;\n  white-space: nowrap; }\n\n.button-container {\n  position: absolute;\n  right: 30px; }\n\n.document-button {\n  margin-left: 0;\n  margin-right: 3px;\n  z-index: 9999; }\n\n.no-edit-mode {\n  padding: 8px; }\n\n.edit-mode {\n  padding: 5px;\n  border: 3px dashed gray;\n  background: white; }\n\n.document-container-inner-height {\n  height: calc(60vh - 64px); }\n\n.document-close {\n  fill: #e2e2e2;\n  cursor: pointer;\n  position: absolute;\n  top: 15px;\n  right: 3px; }\n\n.document-close:hover {\n  fill: #ffab40; }\n\nmd-tab-item {\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  user-select: none; }\n", ""]);
	
	// exports


/***/ },

/***/ 503:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _fileManagerComponent = __webpack_require__(504);
	
	var _fileManagerComponent2 = _interopRequireDefault(_fileManagerComponent);
	
	__webpack_require__(509);
	
	__webpack_require__(311);
	
	__webpack_require__(511);
	
	__webpack_require__(513);
	
	var _fileManagerServiceJs = __webpack_require__(515);
	
	var _fileManagerServiceJs2 = _interopRequireDefault(_fileManagerServiceJs);
	
	var fileManagerModule = _angular2['default'].module('fileManager', ['ngFileUpload', 'ngMdIcons', 'simple-context-menu']).directive('fileManager', _fileManagerComponent2['default']).service('FileManagerService', _fileManagerServiceJs2['default']);
	
	exports['default'] = fileManagerModule;
	module.exports = exports['default'];

/***/ },

/***/ 504:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _fileManagerHtml = __webpack_require__(505);
	
	var _fileManagerHtml2 = _interopRequireDefault(_fileManagerHtml);
	
	var _fileManagerControllerJs = __webpack_require__(506);
	
	var _fileManagerControllerJs2 = _interopRequireDefault(_fileManagerControllerJs);
	
	__webpack_require__(507);
	
	var fileManagerComponent = function fileManagerComponent() {
	    return {
	        template: _fileManagerHtml2['default'],
	        controller: _fileManagerControllerJs2['default'],
	        restrict: 'E',
	        controllerAs: 'vm',
	        scope: {},
	        bindToController: true
	    };
	};
	
	exports['default'] = fileManagerComponent;
	module.exports = exports['default'];

/***/ },

/***/ 505:
/***/ function(module, exports) {

	module.exports = "<md-card>\n    <div class=\"section-headers\">\n        <span>\n            File Manager\n        </span>\n            <span class=\"pull-right hvr-grow\" style=\"margin-top: 3px;\">\n                <ng-md-icon class=\"\" icon=\"info\" size=\"16\" view-box=\"0 0 100 100\" style=\"fill: #212121\" ng-click=\"vm.showFileManagerInfo($event)\"></ng-md-icon>\n            </span>\n    </div>\n    <div class=\"file-manager-content\">\n        <md-card-actions>\n            <span>\n                <md-button ngf-select=\"\" ngf-drop=\"\" ngf-change=\"vm.uploadFiles($files, $invalidFiles)\" ngf-multiple=\"true\"\n                        class=\"md-raised\" ngf-drop-available=\"dropAvailable\" ngf-accept=\"vm.accept\" ngf-pattern=\"vm.FileManagerService.accept\">\n                    File Upload\n                </md-button>\n                <md-button ng-disabled=\"vm.AppStateService.getState('editMode')\" class=\"md-raised\" ng-click=\"vm.pasteText()\">Paste Text</md-button>\n            </span>\n            <div class=\"pull-right\" style=\"padding-right:8px;\">\n                <md-button class=\"md-raised md-primary\" ng-click=\"vm.ripIt($event); vm.JemaLoggerService.log('62494598?app=RipIt2&name=RipIt2&desc=RipIt clicked&payload=RipIt clicked')\">Rip It</md-button>\n                {{vm.needsRipping}}\n            </div>\n        </md-card-actions>\n        <div style=\"height:calc(50vh - 25px - 69px - 64px); overflow-y: auto; margin-top:8px;padding-left: 8px;\">\n        <span title=\"{{item.displayName}}\" class=\"icon-with-text no-select\" ng-repeat=\"item in vm.StorageService.documents\"\n              ng-style=\"vm.determineStyle(item)\" ng-click=\"vm.toggleSelected(item)\"\n              context-menu=\"vm.contextMenuOptions\" context-item-disabled=\"vm.disableContextMenuItem(item)\" item-name=\"item.displayName\">\n            <ng-md-icon icon=\"{{vm.setIcon(item)}}\" size=\"48\" style=\"{{item.valid ? 'fill:black;' : 'fill:red;'}}\" view-box=\"0 0 32 32\">\n            </ng-md-icon>\n            <div style=\"white-space:nowrap;overflow:hidden;text-overflow:ellipsis;text-align:center;\">{{item.displayName}}</div>\n        </span>\n        </div>\n    </div>\n    <div class=\"section-footer\">\n        <span class=\"pull-right\"><em><span ng-if=\"vm.count.activeDocument !== vm.count.totalDocument\">{{vm.count.activeDocument}} / </span>{{vm.count.totalDocument}} documents</em></span>\n    </div>\n</md-card>\n"

/***/ },

/***/ 506:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var FileManagerController = (function () {
	    FileManagerController.$inject = ["$mdDialog", "$rootScope", "$scope", "AppStateService", "FileManagerService", "HelpersService", "JemaLoggerService", "RipItService", "ParseProvider", "StorageService"];
	    function FileManagerController($mdDialog, $rootScope, $scope, AppStateService, FileManagerService, HelpersService, JemaLoggerService, RipItService, ParseProvider, StorageService) {
	        'ngInject';
	
	        var _this = this;
	
	        _classCallCheck(this, FileManagerController);
	
	        this.$mdDialog = $mdDialog;
	        this.$rootScope = $rootScope;
	        this.$scope = $scope;
	        this.AppStateService = AppStateService;
	        this.FileManagerService = FileManagerService;
	        this.HelpersService = HelpersService;
	        this.JemaLoggerService = JemaLoggerService;
	        this.ParseProvider = ParseProvider;
	        this.RipItService = RipItService;
	        this.StorageService = StorageService;
	
	        this.needsRipping = this.RipItService.needsRipping;
	
	        this.count = {
	            activeDocument: 0,
	            totalDocument: 0
	        };
	
	        this.contextMenuOptions = [{
	            displayName: 'Edit',
	            action: function action(name, $event) {
	                return _this.clickedEdit(name, $event);
	            }
	        }, {
	            displayName: 'Delete',
	            action: function action(name, $event) {
	                return _this.clickedDelete(name, $event);
	            }
	        }];
	
	        this.listeners();
	
	        this.watchers();
	    }
	
	    _createClass(FileManagerController, [{
	        key: 'ripIt',
	        value: function ripIt(evt) {
	            this.ParseProvider.setRegexsUsed(); // TODO: we should find a better way to do this
	            if (Object.keys(this.StorageService.documents).length === 0) {
	                this.showLoadSampleConfirm(evt);
	            } else if (this.AppStateService.getState('editMode')) {
	                this.showCannotRip(evt);
	            } else {
	                this.RipItService.parse();
	            }
	        }
	    }, {
	        key: 'clickedDelete',
	        value: function clickedDelete(toDelete, evt) {
	            if (this.AppStateService.getState('editMode')) {
	                // to not allow delete if in edit mode
	                this.deleteAlert(evt);
	            } else {
	                return this.FileManagerService.deleteDocument(toDelete);
	            }
	        }
	    }, {
	        key: 'clickedEdit',
	        value: function clickedEdit(toEdit, evt) {
	            if (this.AppStateService.getState('editMode')) {
	                // do not allow edit if already in edit mode
	                this.editAlert(evt);
	            } else {
	                return this.FileManagerService.editDocument(toEdit);
	            }
	        }
	    }, {
	        key: 'disableContextMenuItem',
	        value: function disableContextMenuItem(item) {
	            var isDisabled = false;
	            if (this.AppStateService.getState('editMode')) {
	                isDisabled = true;
	            } else if (item.uploaded) {
	                isDisabled = true;
	            }
	
	            return isDisabled;
	        }
	    }, {
	        key: 'deleteAlert',
	        value: function deleteAlert(ev) {
	            this.$mdDialog.show(this.$mdDialog.alert().clickOutsideToClose(true).title('Cannot Delete').textContent('You can\'t delete a document in Edit Mode.').ariaLabel('Cannot Delete').ok('Got it!').targetEvent(ev));
	        }
	    }, {
	        key: 'editAlert',
	        value: function editAlert(ev) {
	            this.$mdDialog.show(this.$mdDialog.alert().clickOutsideToClose(true).title('Cannot Edit').textContent('Please \'Cancel\' or \'Save\' current document.').ariaLabel('Already editing').ok('Got it!').targetEvent(ev));
	        }
	    }, {
	        key: 'showFileManagerInfo',
	        value: function showFileManagerInfo(evt) {
	            this.$mdDialog.show({
	                controller: function controller($scope, FileManagerService) {
	                    $scope.supportedFileTypes = FileManagerService.supportedFileTypes;
	                    console.log($scope.supportedFileTypes);
	                },
	                template: '\n                <md-dialog aria-label="Mango (Fruit)" style="width:500px;" ng-cloak>\n                    <md-toolbar>\n                        <div class="md-toolbar-tools">\n                            <h2>File Upload</h2>\n                        </div>\n                    </md-toolbar>\n                    <md-dialog-content>\n                        <div class="md-dialog-content">\n                            <p class="info-text">\n                            The RipIt supports the following file upload formats:\n                            </p>\n                            <ul style="list-style:disc">\n                                <li ng-repeat="type in supportedFileTypes">{{type}}</li>\n                            </ul>\n                            <p class="info-text">\n                            If you need to rip text from a file that is not currently supported, you can use the Paste Text option in RipIt and copy and paste your text from the unsupported document. After clicking Save, RipIt will be able to treat the document as if it had been uploaded.\n                            </p>\n                        </div>\n                    </md-dialog-content>\n                </md-dialog>\n            ',
	                parent: _angular2['default'].element(document.body),
	                targetEvent: evt,
	                clickOutsideToClose: true,
	                fullscreen: false
	            });
	        }
	    }, {
	        key: 'toggleSelected',
	        value: function toggleSelected(item) {
	            return this.FileManagerService.toggleSelected(item);
	        }
	    }, {
	        key: 'uploadFiles',
	        value: function uploadFiles(files, errFiles) {
	            return this.FileManagerService.uploadFilesAsync(files, errFiles);
	        }
	    }, {
	        key: 'pasteText',
	        value: function pasteText() {
	            return this.FileManagerService.pasteText();
	        }
	
	        // dynamic styling
	    }, {
	        key: 'determineStyle',
	        value: function determineStyle(item) {
	            var styleObj = {};
	
	            if (item.selected) {
	                styleObj['background-color'] = 'rgba(0, 0, 0, 0.2)';
	            }
	            if (!item.content || item.content === '') {
	                styleObj.opacity = 0.4;
	            }
	
	            return styleObj;
	        }
	    }, {
	        key: 'setIcon',
	        value: function setIcon(item) {
	            var icon = '';
	            if (!item.valid) {
	                icon = 'document-error';
	            } else if (item.uploaded) {
	                icon = 'document-' + item.extension;
	            } else {
	                icon = 'document';
	            }
	            return icon;
	        }
	    }, {
	        key: 'showLoadSampleConfirm',
	        value: function showLoadSampleConfirm(evt) {
	            var _this2 = this;
	
	            var confirm = this.$mdDialog.confirm().title('No documents to Rip').textContent('Would you like to load the RipIt sample document?').ariaLabel('Question modal').targetEvent(evt).ok('Yes, please').cancel('Oops, nevermind');
	            this.$mdDialog.show(confirm).then(function () {
	                _this2.StorageService.documents = _angular2['default'].copy(_this2.StorageService.sampleDocument);
	                _this2.$rootScope.$broadcast('files-changed');
	                _this2.RipItService.parse();
	            });
	        }
	    }, {
	        key: 'showCannotRip',
	        value: function showCannotRip(evt) {
	            this.$mdDialog.show(this.$mdDialog.alert()
	            // .parent(angular.element(document.querySelector('#popupContainer')))
	            .clickOutsideToClose(true).title('Edit in Progress').textContent('You must Save your document or Cancel.').ariaLabel('Alert Dialog').ok('Ok').targetEvent(evt));
	        }
	    }, {
	        key: 'listeners',
	        value: function listeners() {
	            var _this3 = this;
	
	            this.$scope.$on('files-changed', function () {
	                // TODO: this needs to move to StorageService
	                _this3.count.activeDocument = Object.keys(_this3.StorageService.activeDocuments).length;
	                _this3.count.totalDocument = Object.keys(_this3.StorageService.documents).length;
	                _this3.HelpersService.safeApply();
	            });
	        }
	    }, {
	        key: 'watchers',
	        value: function watchers() {
	            //
	        }
	    }]);
	
	    return FileManagerController;
	})();
	
	exports['default'] = FileManagerController;
	module.exports = exports['default'];

/***/ },

/***/ 507:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(508);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(305)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./fileManager.scss", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./fileManager.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 508:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(304)();
	// imports
	
	
	// module
	exports.push([module.id, ".file-manager-content {\n  padding: 8px 0; }\n\n.drop-box {\n  background: #F8F8F8;\n  border: 5px dashed #DDD;\n  width: 200px;\n  height: 50px;\n  text-align: center;\n  padding: 10px;\n  margin-left: 10px; }\n\n.right {\n  position: absolute;\n  right: 0px; }\n\n#contextmenu-node {\n  position: absolute;\n  background-color: white;\n  border: solid #CCCCCC 1px;\n  padding: 0px;\n  z-index: 1000; }\n\n.contextmenu-item {\n  margin: 0.5em;\n  padding-left: 0.5em; }\n\n.contextmenu-item:hover {\n  background-color: #CCCCCC;\n  cursor: default; }\n\n.icon-with-text {\n  text-align: justify;\n  display: inline-block;\n  padding: 16px 8px 0 8px;\n  width: 72px;\n  margin-right: 5px; }\n\nspan:focus {\n  outline: none;\n  border: 0; }\n\n.section-footer {\n  height: 20px;\n  color: black;\n  padding: 0 5px;\n  font-size: 14px;\n  line-height: 23px;\n  background: #e2e2e2; }\n", ""]);
	
	// exports


/***/ },

/***/ 515:
/***/ function(module, exports, __webpack_require__) {

	/* eslint-disable no-param-reassign, no-unneeded-ternary */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var SUPPORTED_FILE_TYPES = ['csv', 'docx', 'html', 'kml', 'pdf', 'pptx', 'txt', 'xlsx', 'xml']; //'xls',
	var SUPPORTED_TEXT_TYPES = ['txt', 'xml', 'kml', 'html', 'csv'];
	
	var FileManagerService = (function () {
	    FileManagerService.$inject = ["$q", "$rootScope", "AppStateService", "DocTextractService", "PdfTextractService", "PptTextractService", "StorageService", "XlsTextractService"];
	    function FileManagerService($q, $rootScope, AppStateService, DocTextractService, PdfTextractService, PptTextractService, StorageService, XlsTextractService) {
	        'ngInject';
	
	        _classCallCheck(this, FileManagerService);
	
	        this.$q = $q;
	        this.$rootScope = $rootScope;
	        this.AppStateService = AppStateService;
	        this.DocTextractService = DocTextractService;
	        this.PdfTextractService = PdfTextractService;
	        this.PptTextractService = PptTextractService;
	        this.StorageService = StorageService;
	        this.XlsTextractService = XlsTextractService;
	        this.accept = '*';
	        this.supportedFileTypes = SUPPORTED_FILE_TYPES;
	        this.supportedTextTypes = SUPPORTED_TEXT_TYPES;
	
	        // this.needsRipping = this.AppStateService.getState('needsRipping');
	    }
	
	    _createClass(FileManagerService, [{
	        key: 'selectDocument',
	        value: function selectDocument(document) {
	            this.AppStateService.selectedDocs.add(document.displayName);
	            document.selected = true;
	
	            this.$rootScope.$broadcast('files-changed');
	        }
	    }, {
	        key: 'deselectDocument',
	        value: function deselectDocument(document) {
	            this.AppStateService.selectedDocs['delete'](document.displayName);
	            document.selected = false;
	
	            this.$rootScope.$broadcast('files-changed');
	        }
	    }, {
	        key: 'toggleSelected',
	        value: function toggleSelected(document) {
	            // only allow selection changes if not in edit mode
	            if (!this.AppStateService.getState('editMode')) {
	                if (document.valid) {
	                    if (!document.selected) {
	                        this.selectDocument(document);
	                    } else {
	                        this.deselectDocument(document);
	                    }
	
	                    this.$rootScope.$broadcast('files-changed');
	                }
	            }
	        }
	    }, {
	        key: 'pasteText',
	        value: function pasteText() {
	            // first, de-selected all
	            for (var docName in this.StorageService.activeDocuments) {
	                if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
	                    this.StorageService.activeDocuments[docName].selected = false;
	                }
	            }
	
	            var randomDocName = 'PastedDoc1';
	            while (this.StorageService.documents[randomDocName]) {
	                var pattern = /\d+$/g;
	                if (randomDocName.match(pattern)) {
	                    var docNumber = parseInt(randomDocName.match(pattern)[0], 10);
	                    docNumber++;
	                    randomDocName = randomDocName.replace(pattern, docNumber);
	                }
	            }
	
	            this.StorageService.documents[randomDocName] = {
	                displayName: randomDocName,
	                name: randomDocName.replace(/[.]/g, ''),
	                content: '',
	                needsParsing: true,
	                uploaded: false,
	                valid: true,
	                selected: false
	            };
	
	            this.AppStateService.setState('editMode', true);
	            this.AppStateService.setState('documentToEdit', randomDocName);
	            this.$rootScope.$broadcast('files-changed');
	        }
	    }, {
	        key: 'deleteDocument',
	        value: function deleteDocument(toDelete) {
	            this.deselectDocument(this.StorageService.documents[toDelete]);
	            delete this.StorageService.documents[toDelete];
	            this.$rootScope.$broadcast('files-changed');
	        }
	    }, {
	        key: 'editDocument',
	        value: function editDocument(toEdit) {
	            this.selectDocument(this.StorageService.documents[toEdit]);
	
	            this.AppStateService.setState('documentToEdit', toEdit);
	            var content = _angular2['default'].copy(this.StorageService.documents[toEdit].content);
	            this.AppStateService.setState('editMode', true);
	            this.AppStateService.setState('editBackup', content);
	
	            this.$rootScope.$broadcast('edit-document', {
	                content: content
	            });
	        }
	    }, {
	        key: 'uploadFilesAsync',
	        value: function uploadFilesAsync(files) {
	            var _this = this;
	
	            var defered = this.$q.defer();
	            var promises = [];
	
	            var done = false;
	            _angular2['default'].forEach(files, function (file, index) {
	                if (index === files.length - 1) {
	                    done = true;
	                }
	
	                promises.push(_this.uploadSingleFileAsync(file));
	            });
	
	            if (done) {
	                this.$q.all(promises).then(function () {
	                    _this.$rootScope.$broadcast('files-changed');
	                })['catch'](function (e) {
	                    // console.error(e);
	                    defered.reject(e);
	                });
	            }
	            return defered.promise;
	        }
	    }, {
	        key: 'uploadSingleFileAsync',
	        value: function uploadSingleFileAsync(file) {
	            var _this2 = this;
	
	            var defered = this.$q.defer();
	            var promises = [];
	
	            var reader = new FileReader();
	            var extension = file.name.substr(file.name.lastIndexOf('.') + 1);
	            if (this.supportedFileTypes.indexOf(extension) >= 0) {
	                // IE doesnt support read as binary string
	                // reader.readAsBinaryString(file);
	                // reader.readAsArrayBuffer(file);
	                if (this.supportedTextTypes.includes(extension)) {
	                    reader.readAsText(file);
	                } else {
	                    reader.readAsArrayBuffer(file);
	                }
	
	                reader.onload = function (onLoadEvent) {
	                    if (_this2.supportedTextTypes.indexOf(extension) >= 0) {
	                        promises.push(_this2.$q.when({
	                            fileName: file.name,
	                            extension: extension,
	                            content: onLoadEvent.target.result
	                        }));
	                    } else if (extension === 'doc' || extension === 'docx') {
	                        promises.push(_this2.DocTextractService.getTextAsync(onLoadEvent.target.result, file.name, extension));
	                    } else if (extension === 'ppt' || extension === 'pptx') {
	                        promises.push(_this2.PptTextractService.getTextAsync(onLoadEvent.target.result, file.name, extension));
	                    } else if (extension === 'xls' || extension === 'xlsx') {
	                        var bstr = _this2.processExcelFiles(onLoadEvent.target.result);
	                        promises.push(_this2.XlsTextractService.getTextAsync(bstr, file.name, extension));
	                    } else if (extension === 'pdf') {
	                        promises.push(_this2.PdfTextractService.getTextAsync(onLoadEvent.target.result, file.name, extension));
	                    }
	                    _this2.processFiles(promises).then(function () {
	                        defered.resolve(_this2.StorageService.documents);
	                    })['catch'](function (e) {
	                        // console.error(e)
	                        defered.reject(e);
	                    });
	                };
	            } else {
	                // add a blank, invalid file (placeholder)
	                var invalidFile = { fileName: file.name, extension: extension, content: '', valid: false };
	                promises.push(this.$q.when(invalidFile));
	
	                this.processFiles(promises).then(function () {
	                    defered.resolve(_this2.StorageService.documents);
	                })['catch'](function (e) {
	                    // console.error(e)
	                    defered.reject(e);
	                });
	            }
	
	            return defered.promise;
	        }
	    }, {
	        key: 'processFiles',
	        value: function processFiles(promises) {
	            var _this3 = this;
	
	            var deferred = this.$q.defer();
	            this.$q.all(promises).then(function (results) {
	                results.forEach(function (r) {
	                    _this3.StorageService.documents[r.fileName] = {
	                        name: r.fileName.replace(/[\.]/g, ''),
	                        displayName: r.fileName,
	                        content: r.content,
	                        needsParsing: true,
	                        uploaded: true,
	                        valid: r.valid === false ? false : true,
	                        extension: r.extension,
	                        selected: false
	                    };
	                });
	                deferred.resolve(true);
	            })['catch'](function (e) {
	                // console.error(e);
	                deferred.reject(e);
	            });
	
	            return deferred.promise;
	        }
	    }, {
	        key: 'processExcelFiles',
	        value: function processExcelFiles(result) {
	            /* convert ArrayBuffer to binary string */
	            var data = new Uint8Array(result);
	            var arr = [];
	            for (var i = 0; i !== data.length; ++i) {
	                arr[i] = String.fromCharCode(data[i]);
	            }
	            return arr.join('');
	        }
	    }]);
	
	    return FileManagerService;
	})();
	
	exports['default'] = FileManagerService;
	module.exports = exports['default'];

/***/ },

/***/ 516:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _matchMakerComponentJs = __webpack_require__(517);
	
	var _matchMakerComponentJs2 = _interopRequireDefault(_matchMakerComponentJs);
	
	var matchMakerModule = _angular2['default'].module('matchMaker', []).component('matchMaker', _matchMakerComponentJs2['default']);
	
	exports['default'] = matchMakerModule;
	module.exports = exports['default'];

/***/ },

/***/ 517:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _matchMakerHtml = __webpack_require__(518);
	
	var _matchMakerHtml2 = _interopRequireDefault(_matchMakerHtml);
	
	var _matchMakerControllerJs = __webpack_require__(519);
	
	var _matchMakerControllerJs2 = _interopRequireDefault(_matchMakerControllerJs);
	
	__webpack_require__(520);
	
	var matchMakerComponent = {
	    restrict: 'E',
	    bindings: {},
	    template: _matchMakerHtml2['default'],
	    controller: _matchMakerControllerJs2['default']
	};
	
	exports['default'] = matchMakerComponent;
	module.exports = exports['default'];

/***/ },

/***/ 518:
/***/ function(module, exports) {

	module.exports = "<div ng-if=\"!$ctrl.AppStateService.getState('showMatchMakerTable')\" style=\"overflow-y: auto;height:calc(40vh - 89px);padding:8px;\">\n    <div ng-if=\"$ctrl.HelpersService.size($ctrl.StorageService.activeDocuments) < 2\">\n        You must have two or more documents selected to use MatchMaker.\n    </div>\n    <div ng-if=\"$ctrl.HelpersService.size($ctrl.StorageService.activeDocuments) >= 2\">\n        <div ng-repeat=\"(name, bin) in $ctrl.matchMakerOverviewBins | orderByKey:'+'\" class=\"bin-container\" ng-click=\"$ctrl.viewMatchMakerResult(name)\">\n            <table style=\"border: 1px solid black;font-size:16px;\">\n\n                <tr>\n                    <td style=\"width: 100px\">{{bin.length}}</td>\n                </tr>\n            </table>\n            <div class=\"bin-container-title\">{{name}}</div>\n        </div>\n    </div>\n</div>\n<div ng-if=\"$ctrl.AppStateService.getState('showMatchMakerTable')\">\n    <div ng-if=\"$ctrl.HelpersService.size($ctrl.StorageService.activeDocuments) < 2\">\n        You must have two or more documents selected to use MatchMaker.\n    </div>\n    <div ng-if=\"$ctrl.HelpersService.size($ctrl.StorageService.activeDocuments) >= 2\">\n        <div class=\"bin-table-banner\">\n            <span style=\"position: absolute;left: 10px;padding-top: 5px\">\n                <button class=\"results-back-btn\" aria-label=\"Back\" ng-click=\"$ctrl.closeMatchMakerTable()\">\n                    Back to MatchMaker\n                </button>\n            </span>\n            <span class=\"bin-table-title\">{{$ctrl.AppStateService.getState('currentBinView')}}</span>\n\n        </div>\n        <div ag-grid=\"$ctrl.gridOptions\" class=\"ag-fresh\" style=\"height: calc(40vh - 126px);\"></div>\n    </div>\n\n</div>"

/***/ },

/***/ 519:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _lodash = __webpack_require__(320);
	
	var _lodash2 = _interopRequireDefault(_lodash);
	
	var MatchMakerController = (function () {
	    MatchMakerController.$inject = ["$scope", "$timeout", "AppStateService", "HelpersService", "RipItService", "StorageService", "TableService"];
	    function MatchMakerController($scope, $timeout, AppStateService, HelpersService, RipItService, StorageService, TableService) {
	        'ngInject';
	
	        _classCallCheck(this, MatchMakerController);
	
	        this.$scope = $scope;
	        this.$timeout = $timeout;
	        this.AppStateService = AppStateService;
	        this.HelpersService = HelpersService;
	        this.RipItService = RipItService;
	        this.StorageService = StorageService;
	        this.TableService = TableService;
	
	        this.initialize();
	        this.listeners();
	    }
	
	    _createClass(MatchMakerController, [{
	        key: 'initialize',
	        value: function initialize() {
	            this.matchMakerOverviewBins = [];
	            this.description = 'Quickly identify common entities between two or more documents.';
	            this.gridOptions = _angular2['default'].copy(this.TableService.defaultGridOptions);
	        }
	    }, {
	        key: 'generateMatchMakerData',
	        value: function generateMatchMakerData() {
	            return this.RipItService.generateMatchMakerData();
	        }
	    }, {
	        key: 'viewMatchMakerResult',
	        value: function viewMatchMakerResult(binName) {
	            this.showMatchMakerTable = true;
	            this.AppStateService.setState('showMatchMakerTable', true);
	            this.AppStateService.setState('currentBinView', binName);
	
	            var binData = this.generateMatchMakerData()[binName];
	            this.generateTableData(binData);
	        }
	    }, {
	        key: 'generateTableData',
	        value: function generateTableData(binData) {
	            var _this = this;
	
	            this.$timeout(function () {
	                _this.gridOptions.api.setColumnDefs(_this.TableService.generateTableHeaders(binData));
	                _this.gridOptions.api.setRowData(binData);
	                _this.gridOptions.api.sizeColumnsToFit();
	            });
	        }
	    }, {
	        key: 'generateMatchMakerResultData',
	        value: function generateMatchMakerResultData(binName) {
	
	            if (binName.toUpperCase() === 'ALL') {
	                var allMatchMakerTable = [];
	                var binsList = _angular2['default'].copy(this.RipItService.binsList);
	                for (var bin in binsList) {
	                    if (binsList.hasOwnProperty(bin)) {
	                        var binResultData = this.generateMatchMakerResultData(binsList[bin].name);
	                        console.log(binsList[bin].name, binResultData);
	                        allMatchMakerTable.concat(binResultData);
	                    }
	                }
	
	                console.log(allMatchMakerTable);
	            }
	
	            var binData = this.RipItService.generateBinResultData(binName);
	            var metaProperties = ['Count', 'Document'];
	
	            var matchMaker = {};
	            var matchMakerTable = {};
	
	            var extractedEntityProperties = new Set();
	
	            for (var i = 0, len = binData.length; i < len; i++) {
	                // build up the unique key
	                var uniqueKey = [];
	                for (var d in binData[i]) {
	                    if (binData[i].hasOwnProperty(d)) {
	                        if (!metaProperties.includes(d)) {
	                            uniqueKey.push(binData[i][d]);
	                            extractedEntityProperties.add(d);
	                        }
	                    }
	                }
	                uniqueKey = uniqueKey.join('|');
	
	                if (!matchMaker[uniqueKey]) {
	                    matchMaker[uniqueKey] = {};
	                }
	                if (!matchMaker[uniqueKey][binData[i].Document]) {
	                    matchMaker[uniqueKey][binData[i].Document] = [];
	                }
	                matchMaker[uniqueKey][binData[i].Document].push(binData[i]);
	            }
	
	            // remove entities that have less than 2 matches between documents
	            for (var uniqueKey in matchMaker) {
	                if (matchMaker.hasOwnProperty(uniqueKey)) {
	                    if (Object.keys(matchMaker[uniqueKey]).length < 2) {
	                        delete matchMaker[uniqueKey];
	                    }
	                }
	            }
	            var headers = new Set();
	            var _iteratorNormalCompletion = true;
	            var _didIteratorError = false;
	            var _iteratorError = undefined;
	
	            try {
	                for (var _iterator = extractedEntityProperties[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	                    var entity = _step.value;
	
	                    headers.add(entity);
	                }
	            } catch (err) {
	                _didIteratorError = true;
	                _iteratorError = err;
	            } finally {
	                try {
	                    if (!_iteratorNormalCompletion && _iterator['return']) {
	                        _iterator['return']();
	                    }
	                } finally {
	                    if (_didIteratorError) {
	                        throw _iteratorError;
	                    }
	                }
	            }
	
	            headers.add('Total');
	            for (var uniqueKey in matchMaker) {
	                if (matchMaker.hasOwnProperty(uniqueKey)) {
	                    for (var u in matchMaker[uniqueKey]) {
	                        if (matchMaker[uniqueKey].hasOwnProperty(u)) {
	                            headers.add(u.replace(/\./g, ''));
	                        }
	                    }
	                }
	            }
	            var defaultRow = {};
	
	            var _iteratorNormalCompletion2 = true;
	            var _didIteratorError2 = false;
	            var _iteratorError2 = undefined;
	
	            try {
	                for (var _iterator2 = headers[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	                    var h = _step2.value;
	
	                    if (h === 'Total') {
	                        defaultRow[h] = 0;
	                    } else {
	                        defaultRow[h] = '';
	                    }
	                }
	            } catch (err) {
	                _didIteratorError2 = true;
	                _iteratorError2 = err;
	            } finally {
	                try {
	                    if (!_iteratorNormalCompletion2 && _iterator2['return']) {
	                        _iterator2['return']();
	                    }
	                } finally {
	                    if (_didIteratorError2) {
	                        throw _iteratorError2;
	                    }
	                }
	            }
	
	            for (var uniqueKey in matchMaker) {
	                if (matchMaker.hasOwnProperty(uniqueKey)) {
	                    for (var u in matchMaker[uniqueKey]) {
	                        if (matchMaker[uniqueKey].hasOwnProperty(u)) {
	                            if (!matchMakerTable[uniqueKey]) {
	                                matchMakerTable[uniqueKey] = _angular2['default'].copy(defaultRow);
	                                var _iteratorNormalCompletion3 = true;
	                                var _didIteratorError3 = false;
	                                var _iteratorError3 = undefined;
	
	                                try {
	                                    for (var _iterator3 = extractedEntityProperties[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
	                                        var entity = _step3.value;
	
	                                        matchMakerTable[uniqueKey][entity] = matchMaker[uniqueKey][u][0][entity];
	                                    }
	                                } catch (err) {
	                                    _didIteratorError3 = true;
	                                    _iteratorError3 = err;
	                                } finally {
	                                    try {
	                                        if (!_iteratorNormalCompletion3 && _iterator3['return']) {
	                                            _iterator3['return']();
	                                        }
	                                    } finally {
	                                        if (_didIteratorError3) {
	                                            throw _iteratorError3;
	                                        }
	                                    }
	                                }
	
	                                matchMakerTable[uniqueKey][matchMaker[uniqueKey][u][0].Document.replace(/\./g, '')] = 'True';
	                                matchMakerTable[uniqueKey].Total++;
	                            } else {
	                                matchMakerTable[uniqueKey][matchMaker[uniqueKey][u][0].Document.replace(/\./g, '')] = 'True';
	                                matchMakerTable[uniqueKey].Total++;
	                            }
	                        }
	                    }
	                }
	            }
	            matchMakerTable = _lodash2['default'].values(matchMakerTable);
	
	            console.log(matchMakerTable);
	
	            return matchMakerTable;
	        }
	    }, {
	        key: 'closeMatchMakerTable',
	        value: function closeMatchMakerTable() {
	            this.AppStateService.setState('showMatchMakerTable', false);
	            this.AppStateService.setState('currentBinView', '');
	        }
	    }, {
	        key: 'listeners',
	        value: function listeners() {
	            var _this2 = this;
	
	            this.$scope.$on('files-changed', function () {
	                if (_this2.AppStateService.getState('ResultsView') === 'MatchMaker') {
	                    console.log(1);
	                    if (_this2.AppStateService.getState('showMatchMakerTable')) {
	                        console.log(2);
	                        var binData = _this2.generateMatchMakerData()[_this2.AppStateService.getState('currentBinView')];
	                        _this2.generateTableData(binData);
	                    } else {
	                        console.log(3);
	                        _this2.matchMakerOverviewBins = _this2.generateMatchMakerData();
	                    }
	                }
	            });
	
	            this.$scope.$on('data-ripped', function () {
	                if (_this2.AppStateService.getState('ResultsView') === 'MatchMaker') {
	                    _this2.matchMakerOverviewBins = _this2.generateMatchMakerData();
	                }
	            });
	
	            this.$scope.$on('build-results', function () {
	                if (_this2.AppStateService.getState('ResultsView') === 'MatchMaker') {
	                    _this2.matchMakerOverviewBins = _this2.generateMatchMakerData();
	                }
	            });
	        }
	    }]);
	
	    return MatchMakerController;
	})();
	
	exports['default'] = MatchMakerController;
	module.exports = exports['default'];

/***/ },

/***/ 520:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(521);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(305)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/sass-loader/index.js!./matchMaker.scss", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/sass-loader/index.js!./matchMaker.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 521:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(304)();
	// imports
	
	
	// module
	exports.push([module.id, "\n", ""]);
	
	// exports


/***/ },

/***/ 522:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _navbarComponentJs = __webpack_require__(523);
	
	var _navbarComponentJs2 = _interopRequireDefault(_navbarComponentJs);
	
	var navbarModule = _angular2['default'].module('navbar', []).component('navbar', _navbarComponentJs2['default']);
	
	exports['default'] = navbarModule;
	module.exports = exports['default'];

/***/ },

/***/ 523:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _navbarHtml = __webpack_require__(524);
	
	var _navbarHtml2 = _interopRequireDefault(_navbarHtml);
	
	var _navbarController = __webpack_require__(525);
	
	var _navbarController2 = _interopRequireDefault(_navbarController);
	
	__webpack_require__(526);
	
	var navbarComponent = {
	    restrict: 'E',
	    bindings: {},
	    template: _navbarHtml2['default'],
	    controller: _navbarController2['default']
	};
	
	exports['default'] = navbarComponent;
	module.exports = exports['default'];

/***/ },

/***/ 524:
/***/ function(module, exports) {

	module.exports = "<md-toolbar style=\"min-height: 50px;\">\n    <div class=\"md-toolbar-tools\" style=\"height: 50px;\">\n        <a ng-href=\"#\"><img src=\"images/ripit-logo-50.png\" alt=\"RipIt Logo\" style=\"height:40px\"></a>\n        <span layout=\"row\" flex layout-align=\"end center\"><a ng-href=\"mailto:{{$ctrl.email}}\"\n                                                             class=\"md-caption\">Feedback</a></span>\n        <span class=\"logo-container\">\n            <img src=\"images/team_phoenix_logo_40.png\" alt=\"Team Phoenix Logo\">\n        </span>\n        <span class=\"logo-container\">\n            <img src=\"images/jida_seal-40.png\" alt=\"JIDA Logo\">\n        </span>\n    </div>\n</md-toolbar>\n"

/***/ },

/***/ 525:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var NavbarController = function NavbarController(APP_SETTINGS) {
	    'ngInject';
	
	    _classCallCheck(this, NavbarController);
	
	    this.email = APP_SETTINGS.feedbackEmail;
	};
	NavbarController.$inject = ["APP_SETTINGS"];
	
	exports['default'] = NavbarController;
	module.exports = exports['default'];

/***/ },

/***/ 526:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(527);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(305)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./navbar.scss", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./navbar.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 527:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(304)();
	// imports
	
	
	// module
	exports.push([module.id, ".logo-container {\n  margin-left: 5px; }\n", ""]);
	
	// exports


/***/ },

/***/ 528:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _ngclipboard = __webpack_require__(529);
	
	var _ngclipboard2 = _interopRequireDefault(_ngclipboard);
	
	var _queryBuilderComponentJs = __webpack_require__(539);
	
	var _queryBuilderComponentJs2 = _interopRequireDefault(_queryBuilderComponentJs);
	
	var _queryBuilderServiceJs = __webpack_require__(544);
	
	var _queryBuilderServiceJs2 = _interopRequireDefault(_queryBuilderServiceJs);
	
	var queryBuilderModule = _angular2['default'].module('queryBuilder', [_ngclipboard2['default']]).component('queryBuilder', _queryBuilderComponentJs2['default']).service('QueryBuilderService', _queryBuilderServiceJs2['default']);
	
	exports['default'] = queryBuilderModule;
	module.exports = exports['default'];

/***/ },

/***/ 539:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _queryBuilderHtml = __webpack_require__(540);
	
	var _queryBuilderHtml2 = _interopRequireDefault(_queryBuilderHtml);
	
	var _queryBuilderController = __webpack_require__(541);
	
	var _queryBuilderController2 = _interopRequireDefault(_queryBuilderController);
	
	__webpack_require__(542);
	
	var queryBuilderComponent = {
	    restrict: 'E',
	    bindings: {},
	    template: _queryBuilderHtml2['default'],
	    controller: _queryBuilderController2['default']
	};
	
	exports['default'] = queryBuilderComponent;
	module.exports = exports['default'];

/***/ },

/***/ 540:
/***/ function(module, exports) {

	module.exports = "<md-card style=\"height: calc(50vh - 50px + 8px + 8px);\">\n    <div class=\"section-headers\">Query Builder</div>\n    <md-card-content class=\"small-padding\" style=\"overflow-y: auto;overflow-x: hidden;\">\n        <md-input-container class=\"md-block\">\n            <label>Bin</label>\n            <md-select ng-model=\"$ctrl.queryBuilderForm.bins.selected\" ng-change=\"$ctrl.generateAllowedFormats($ctrl.queryBuilderForm.bins.selected)\" aria-label=\"Delimiter Selector\">\n                <md-option ng-repeat=\"bin in $ctrl.queryBuilderForm.bins.options | orderBy:'name'\" value=\"{{bin.name}}\">\n                    {{bin.name}} ({{bin.count}})\n                </md-option>\n            </md-select>\n        </md-input-container>\n        <md-input-container class=\"md-block\">\n            <label>Format</label>\n            <md-select ng-model=\"$ctrl.queryBuilderForm.format.selected\" ng-change=\"$ctrl.formatQuery($ctrl.queryBuilderForm.bins, $ctrl.queryBuilderForm.format, $ctrl.queryBuilderForm.delimiter)\" aria-label=\"Query View Selector\">\n                <md-option ng-repeat=\"queryOptions in $ctrl.queryBuilderForm.format.options\" value=\"{{queryOptions.value}}\">\n                    {{queryOptions.label}}\n                </md-option>\n            </md-select>\n        </md-input-container>\n        <div ng-if=\"$ctrl.queryBuilderForm.format.selected === 'customFormat'\">\n            <div class=\"row\">\n                <div class=\"col-xs-4\">\n                    <md-input-container class=\"md-block\" style=\"margin-bottom: 0;\">\n                        <label>Pattern</label>\n                        <input ng-model=\"$ctrl.queryBuilderForm.format.custom.pattern\" ng-trim=\"false\" ng-change=\"$ctrl.formatQuery($ctrl.queryBuilderForm.bins, $ctrl.queryBuilderForm.format, $ctrl.queryBuilderForm.delimiter)\">\n                        <div class=\"hint\">RegEx</div>\n                    </md-input-container>\n                </div>\n                <div class=\"col-xs-4\">\n                    <md-input-container class=\"md-block\" style=\"margin-bottom: 0;\">\n                        <label>Replace</label>\n                        <input ng-model=\"$ctrl.queryBuilderForm.format.custom.replace\" ng-trim=\"false\" ng-change=\"$ctrl.formatQuery($ctrl.queryBuilderForm.bins, $ctrl.queryBuilderForm.format, $ctrl.queryBuilderForm.delimiter)\">\n                        <div class=\"hint\">RegEx</div>\n                    </md-input-container>\n                </div>\n                <div class=\"col-xs-4\">\n                    <md-input-container class=\"md-block\" style=\"margin-bottom: 0;\">\n                        <label>Flags</label>\n                        <input ng-model=\"$ctrl.queryBuilderForm.format.custom.flags\" ng-change=\"$ctrl.formatQuery($ctrl.queryBuilderForm.bins, $ctrl.queryBuilderForm.format, $ctrl.queryBuilderForm.delimiter)\">\n                    </md-input-container>\n                </div>\n            </div>\n        </div>\n        <md-input-container class=\"md-block\">\n            <label>Delimiter</label>\n            <md-select ng-model=\"$ctrl.queryBuilderForm.delimiter.selected\" ng-change=\"$ctrl.formatQuery($ctrl.queryBuilderForm.bins, $ctrl.queryBuilderForm.format, $ctrl.queryBuilderForm.delimiter)\" aria-label=\"Delimiter Selector\">\n                <md-option ng-repeat=\"option in $ctrl.queryBuilderForm.delimiter.options track by $index\" value=\"{{option.value}}\">\n                    {{option.label}}\n                </md-option>\n            </md-select>\n        </md-input-container>\n        <div ng-if=\"$ctrl.queryBuilderForm.delimiter.selected === 'customDelimiter'\">\n            <md-input-container class=\"md-block\">\n                <label>Custom Delimiter</label>\n                <input ng-model=\"$ctrl.queryBuilderForm.delimiter.custom.pattern\" ng-trim=\"false\" ng-change=\"$ctrl.formatQuery($ctrl.queryBuilderForm.bins, $ctrl.queryBuilderForm.format, $ctrl.queryBuilderForm.delimiter)\">\n                <div class=\"hint\">RegEx</div>\n            </md-input-container>\n        </div>\n        <div class=\"query-viewer-container\">\n            <span style=\"position: absolute;top: 5px;right: 0px;\">\n                <span style=\"background-color: white;padding: 8px 2px 2px 5px;margin-right: 17px;\">\n                    <ng-md-icon icon=\"clippy\" size=\"18\" view-box=\"0 0 1024 896\" ngclipboard data-clipboard-action=\"copy\" data-clipboard-target=\"#query-viewer\" ngclipboard-success=\"$ctrl.onSuccess(e);\" aria-label=\"Copy To Clipboard Button\"></ng-md-icon>\n                </span>\n                <!--<md-button class=\"md-icon-button\" ngclipboard data-clipboard-action=\"copy\" data-clipboard-target=\"#query-viewer\" ngclipboard-success=\"$ctrl.onSuccess(e);\" aria-label=\"Copy To Clipboard Button\">\n                    <ng-md-icon icon=\"clippy\" size=\"18\" view-box=\"0 0 1024 896\"></ng-md-icon>\n                </md-button>-->\n            </span>\n            <textarea id=\"query-viewer\" ng-model=\"$ctrl.queryBuilderForm.queryViewer\" class=\"query-viewer\" readonly></textarea>\n        </div>\n    </md-card-content>\n</md-card>\n"

/***/ },

/***/ 541:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var QueryBuilderController = (function () {
	    QueryBuilderController.$inject = ["$mdToast", "$rootScope", "QueryBuilderService", "RipItService", "StorageService"];
	    function QueryBuilderController($mdToast, $rootScope, QueryBuilderService, RipItService, StorageService) {
	        'ngInject';
	
	        _classCallCheck(this, QueryBuilderController);
	
	        this.$mdToast = $mdToast;
	        this.$rootScope = $rootScope;
	        this.QueryBuilderService = QueryBuilderService;
	        this.RipItService = RipItService;
	        this.StorageService = StorageService;
	
	        this.queryFormatOptionsMaster = this.RipItService.queryFormatOptionsMaster;
	        this.queryBuilderForm = this.QueryBuilderService.queryBuilderForm;
	
	        this.listeners();
	    }
	
	    _createClass(QueryBuilderController, [{
	        key: 'getQueryOutput',
	        value: function getQueryOutput(queryView) {
	            return this.QueryBuilderService.getQueryOutput(queryView);
	        }
	    }, {
	        key: 'formatQuery',
	        value: function formatQuery(binObj, formatObj, delimiterObj) {
	            return this.QueryBuilderService.formatQuery(binObj, formatObj, delimiterObj);
	        }
	    }, {
	        key: 'generateAllowedFormats',
	        value: function generateAllowedFormats(binName) {
	            return this.QueryBuilderService.generateAllowedFormats(binName);
	        }
	    }, {
	        key: 'onSuccess',
	        value: function onSuccess(evt) {
	            evt.clearSelection();
	
	            this.$mdToast.show(this.$mdToast.simple().textContent('Query Copied!').position('top right').hideDelay(3000));
	        }
	    }, {
	        key: 'listeners',
	        value: function listeners() {
	            var _this = this;
	
	            this.$rootScope.$on('files-changed', function () {
	                if (_this.queryBuilderForm) {
	                    _this.formatQuery(_this.queryBuilderForm.bins, _this.queryBuilderForm.format, _this.queryBuilderForm.delimiter);
	                }
	            });
	        }
	    }]);
	
	    return QueryBuilderController;
	})();
	
	exports['default'] = QueryBuilderController;
	module.exports = exports['default'];

/***/ },

/***/ 542:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(543);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(305)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./queryBuilder.scss", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./queryBuilder.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 543:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(304)();
	// imports
	
	
	// module
	exports.push([module.id, ".query-viewer-container {\n  height: 100px;\n  width: 100%;\n  position: relative; }\n\n.query-viewer {\n  height: 100px;\n  width: 100%;\n  resize: none;\n  border: 1px solid rgba(0, 0, 0, 0.12); }\n\ntextarea[disabled] {\n  background-color: white; }\n\n.hint {\n  /* Position the hint */\n  position: absolute;\n  left: 2px;\n  right: auto;\n  bottom: 7px;\n  /* Copy styles from ng-messages */\n  font-size: 12px;\n  line-height: 14px;\n  transition: all 0.3s cubic-bezier(0.55, 0, 0.55, 0.2);\n  /* Set our own color */\n  color: grey; }\n", ""]);
	
	// exports


/***/ },

/***/ 544:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var QueryBuilderService = (function () {
	    QueryBuilderService.$inject = ["$rootScope", "RipItService", "StorageService"];
	    function QueryBuilderService($rootScope, RipItService, StorageService) {
	        'ngInject';
	
	        _classCallCheck(this, QueryBuilderService);
	
	        this.$rootScope = $rootScope;
	        this.RipItService = RipItService;
	        this.StorageService = StorageService;
	
	        this.queryFormatOptionsMaster = [{ label: 'All', value: 'all', excludedBins: ['ALL SELECTORS'] }, { label: 'All selectors', value: 'selectorAll', includedBins: ['ALL SELECTORS'] }, { label: 'Prepend with *', value: 'last9PrependWithAsterisk', includedBins: ['ALL SELECTORS'] }, { label: 'Harmony', value: 'harmony', includedBins: ['ALL SELECTORS'] }, { label: 'Custom', value: 'customFormat' }];
	
	        this.queryDelimiterOptions = [{ label: 'Comma', value: ',' }, { label: 'AND', value: ' AND ' }, { label: 'OR', value: ' OR ' }, { label: 'New Line', value: '\n' }, { label: 'Custom', value: 'customDelimiter' }];
	
	        this.queryBinsOptions = this.RipItService.binsList;
	        this.queryFormatOptions = _angular2['default'].copy(this.queryFormatOptionsMaster);
	
	        this.queryBuilderForm = {
	            bins: {
	                options: this.queryBinsOptions,
	                selected: this.queryBinsOptions[0] ? this.queryBinsOptions[0].value : ''
	            },
	            format: {
	                options: this.queryFormatOptions,
	                selected: this.queryFormatOptions[0].value,
	                custom: {
	                    pattern: '',
	                    replace: '',
	                    flags: 'ig'
	                }
	            },
	            delimiter: {
	                options: this.queryDelimiterOptions,
	                selected: this.queryDelimiterOptions[0].value,
	                custom: {
	                    pattern: ''
	                }
	            },
	            queryViewer: ''
	        };
	
	        this.listeners();
	    }
	
	    _createClass(QueryBuilderService, [{
	        key: 'generateAllowedFormats',
	        value: function generateAllowedFormats(binName) {
	            var allOptions = _angular2['default'].copy(this.queryFormatOptionsMaster);
	            var options = [];
	
	            allOptions.forEach(function (option) {
	                if (option.includedBins && option.includedBins.includes(binName.toUpperCase())) {
	                    options.push(option);
	                } else if (option.excludedBins && !option.excludedBins.includes(binName.toUpperCase())) {
	                    options.push(option);
	                } else if (!option.includedBins && !option.excludedBins) {
	                    options.push(option);
	                }
	            });
	
	            this.queryBuilderForm.format.options = options;
	            this.queryBuilderForm.format.selected = this.queryBuilderForm.format.options[0].value;
	
	            // and now run because that what people want
	            this.formatQuery(this.queryBuilderForm.bins, this.queryBuilderForm.format, this.queryBuilderForm.delimiter);
	        }
	    }, {
	        key: 'formatQuery',
	        value: function formatQuery(binObj, formatObj, delimiterObj) {
	            if (binObj.selected) {
	                var binData = this.generateBinData(binObj.selected);
	                var formattedBinData = this.formatBinData(formatObj, binData);
	                formattedBinData = this.joinBinData(formattedBinData, delimiterObj);
	                this.queryBuilderForm.queryViewer = formattedBinData;
	            }
	        }
	    }, {
	        key: 'generateBinsList',
	        value: function generateBinsList() {
	            return this.RipItService.generateBinsList();
	        }
	    }, {
	        key: 'generateBinData',
	        value: function generateBinData(binName) {
	            var output = [];
	            var uniqueEntities = new Set();
	
	            // loop through Storage to get all documents
	            for (var docName in this.StorageService.activeDocuments) {
	                if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
	                    // get to the selected bin
	                    var bin = _angular2['default'].copy(this.StorageService.activeDocuments[docName].results[binName]);
	
	                    for (var i = 0, len = bin.length; i < len; i++) {
	                        uniqueEntities.add(bin[i][bin[i].mainGroup]);
	                    }
	                }
	            }
	
	            // convert back to array
	            output = Array.from(uniqueEntities);
	
	            return output;
	        }
	    }, {
	        key: 'joinBinData',
	        value: function joinBinData(binData, delimiterObj) {
	            var output = undefined;
	            if (delimiterObj.selected === 'customDelimiter') {
	                output = binData[0];
	                for (var i = 1, len = binData.length; i < len; i++) {
	                    var pattern = _angular2['default'].copy(delimiterObj.custom.pattern);
	                    pattern = pattern.replace(/\\n/g, '\n').replace(/\\t/g, '\t').replace(/\\r/g, '\r');
	                    output += pattern + binData[i];
	                }
	            } else {
	                output = binData.join(delimiterObj.selected);
	            }
	
	            return output;
	        }
	    }, {
	        key: 'formatBinData',
	        value: function formatBinData(formatObj, binData) {
	            var output = undefined;
	
	            if (this[formatObj.selected] && typeof this[formatObj.selected] === 'function') {
	                output = this[formatObj.selected](binData, formatObj);
	            }
	
	            return output;
	        }
	    }, {
	        key: 'all',
	        value: function all(binData) {
	            return binData.map(function (value) {
	                return '"' + value + '"';
	            });
	        }
	    }, {
	        key: 'selectorAll',
	        value: function selectorAll(binData) {
	            return binData.map(function (value) {
	                return '' + value;
	            });
	        }
	    }, {
	        key: 'last9PrependWithAsterisk',
	        value: function last9PrependWithAsterisk(binData) {
	            return binData.map(function (value) {
	                return '*' + value.substr(-9);
	            });
	        }
	    }, {
	        key: 'harmony',
	        value: function harmony(binData) {
	            return binData.map(function (value) {
	                return value.substr(-9) + ' OR 0' + value.substr(-9) + ' OR ' + value;
	            });
	        }
	    }, {
	        key: 'customFormat',
	        value: function customFormat(binData, formatObj) {
	            var output = [];
	            if (formatObj.custom.pattern !== '') {
	                (function () {
	                    var re = new RegExp(formatObj.custom.pattern, formatObj.custom.flags);
	
	                    binData.forEach(function (value) {
	                        var replacement = value.replace(re, formatObj.custom.replace);
	                        output.push(replacement);
	                    });
	                })();
	            }
	            return output;
	        }
	    }, {
	        key: 'listeners',
	        value: function listeners() {
	            var _this = this;
	
	            this.$rootScope.$on('data-ripped', function () {
	                _this.generateBinsList();
	            });
	        }
	    }]);
	
	    return QueryBuilderService;
	})();
	
	exports['default'] = QueryBuilderService;
	module.exports = exports['default'];

/***/ },

/***/ 545:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	  value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _resultOutputComponentJs = __webpack_require__(546);
	
	var _resultOutputComponentJs2 = _interopRequireDefault(_resultOutputComponentJs);
	
	var resultOutputModule = _angular2['default'].module('resultOutput', []).directive('resultOutput', _resultOutputComponentJs2['default']);
	
	exports['default'] = resultOutputModule;
	module.exports = exports['default'];

/***/ },

/***/ 546:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _resultOutputHtml = __webpack_require__(547);
	
	var _resultOutputHtml2 = _interopRequireDefault(_resultOutputHtml);
	
	var _resultOutputControllerJs = __webpack_require__(548);
	
	var _resultOutputControllerJs2 = _interopRequireDefault(_resultOutputControllerJs);
	
	__webpack_require__(549);
	
	var resultOutputComponent = function resultOutputComponent() {
	    return {
	        template: _resultOutputHtml2['default'],
	        controller: _resultOutputControllerJs2['default'],
	        restrict: 'E',
	        controllerAs: 'vm',
	        scope: {},
	        bindToController: true
	    };
	};
	
	exports['default'] = resultOutputComponent;
	module.exports = exports['default'];

/***/ },

/***/ 547:
/***/ function(module, exports) {

	module.exports = "<md-card style=\"height: calc(100% - 16px)\">\n    <md-card-content class=\"no-padding\">\n        <md-tabs md-selected=\"vm.AppStateService.state.selectedTab\" md-stretch-tabs=\"always\" class=\"result-container-inner-height\">\n            <md-tab id=\"tab1\">\n                <md-tab-label>\n                    Results\n                    <span>\n                        <md-button ng-click=\"vm.showInfo($event)\" class=\"md-icon-button\" style=\"position:absolute;padding-top:0;margin-top:-2px;margin-left: -6px;\" aria-label=\"Info\">\n                            <ng-md-icon icon=\"info\" style=\"fill:#cdcdcd\" size=\"18\" view-box=\"0 0 100 100\"></ng-md-icon>\n                        </md-button>\n                    </span>\n                </md-tab-label>\n                <md-tab-body>\n                    <results></results>\n                </md-tab-body>\n            </md-tab>\n            <md-tab label=\"MatchMaker\" ng-disabled=\"\" class=\"result-container-inner-height\">\n                <match-maker></match-maker>\n            </md-tab>\n        </md-tabs>\n    </md-card-content>\n</md-card>\n\n"

/***/ },

/***/ 548:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var ResultOutputController = (function () {
	    ResultOutputController.$inject = ["$mdDialog", "$rootScope", "$scope", "AppStateService"];
	    function ResultOutputController($mdDialog, $rootScope, $scope, AppStateService) {
	        'ngInject';
	
	        _classCallCheck(this, ResultOutputController);
	
	        this.$mdDialog = $mdDialog;
	        this.$scope = $scope;
	        this.$rootScope = $rootScope;
	        this.AppStateService = AppStateService;
	        this.selectedTab = this.AppStateService.state.selectedTab;
	
	        this.watchers();
	    }
	
	    _createClass(ResultOutputController, [{
	        key: 'watchers',
	        value: function watchers() {
	            var _this = this;
	
	            this.$scope.$watch(function () {
	                return _this.AppStateService.state.selectedTab;
	            }, function () {
	                if (_this.AppStateService.state.selectedTab === 0) {
	                    _this.AppStateService.setState('ResultsView', 'Results');
	                } else if (_this.AppStateService.state.selectedTab === 1) {
	                    _this.AppStateService.setState('ResultsView', 'MatchMaker');
	                }
	                _this.$rootScope.$broadcast('build-results');
	            }, true);
	        }
	    }, {
	        key: 'showInfo',
	        value: function showInfo(evt) {
	            this.$mdDialog.show({
	                template: '\n                <md-dialog aria-label="Results Dialog" ng-cloak>\n                    <md-dialog-content>\n                        <div class="md-dialog-content">\n                            <h2>Results Section</h2>\n                            <p style="width:250px">\n                                These results provide a high level overview of all extracted entities across all documents and extraction bin.\n                            </p>\n                            <p>\n                                <div>Legend:</div>\n                                <div class="bin-container" style="font-size: 20px;height:initial;">\n                                    <table style="border: 1px solid black;">\n                                        <tr>\n                                            <td style="width:75px">Unique</td>\n                                            <td style="width:75px">Total</td>\n                                        </tr>\n                                        <tr>\n                                            <td colspan="2">Documents</td>\n                                        </tr>\n                                    </table>\n                                </div>\n                            </p>\n                        </div>\n                    </md-dialog-content>\n                </md-dialog>\n            ',
	                parent: _angular2['default'].element(document.body),
	                targetEvent: evt,
	                clickOutsideToClose: true,
	                fullscreen: true
	            });
	        }
	    }]);
	
	    return ResultOutputController;
	})();
	
	exports['default'] = ResultOutputController;
	module.exports = exports['default'];

/***/ },

/***/ 549:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(550);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(305)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./resultOutput.scss", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./resultOutput.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 550:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(304)();
	// imports
	
	
	// module
	exports.push([module.id, ".resultOutput {\n  color: red; }\n\n.result-container-inner-height {\n  height: calc(40vh - 40px); }\n\n.results-back-btn {\n  font-weight: 500;\n  text-transform: uppercase;\n  font-weight: 500;\n  font-size: 14px;\n  font-style: inherit;\n  font-variant: inherit;\n  font-family: inherit;\n  color: #FFFFFF;\n  border: none;\n  background-color: #FFAB40;\n  border-radius: 3px;\n  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26); }\n", ""]);
	
	// exports


/***/ },

/***/ 551:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _resultsComponentJs = __webpack_require__(552);
	
	var _resultsComponentJs2 = _interopRequireDefault(_resultsComponentJs);
	
	var _agGrid = __webpack_require__(557);
	
	var _agGrid2 = _interopRequireDefault(_agGrid);
	
	__webpack_require__(633);
	
	__webpack_require__(635);
	
	__webpack_require__(637);
	
	_agGrid2['default'].initialiseAgGridWithAngular1(_angular2['default']);
	
	var resultsModule = _angular2['default'].module('results', ['agGrid']).directive('results', _resultsComponentJs2['default']);
	
	exports['default'] = resultsModule;
	module.exports = exports['default'];

/***/ },

/***/ 552:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _resultsHtml = __webpack_require__(553);
	
	var _resultsHtml2 = _interopRequireDefault(_resultsHtml);
	
	var _resultsControllerJs = __webpack_require__(554);
	
	var _resultsControllerJs2 = _interopRequireDefault(_resultsControllerJs);
	
	__webpack_require__(555);
	
	var resultsComponent = function resultsComponent() {
	    return {
	        template: _resultsHtml2['default'],
	        controller: _resultsControllerJs2['default'],
	        restrict: 'E',
	        controllerAs: 'vm',
	        scope: {},
	        bindToController: true
	    };
	};
	
	exports['default'] = resultsComponent;
	module.exports = exports['default'];

/***/ },

/***/ 553:
/***/ function(module, exports) {

	module.exports = "<div ng-if=\"!vm.AppStateService.getState('showResultTable')\" style=\"overflow-y: auto;height:calc(40vh - 89px);padding:8px;\">\n    <div ng-repeat=\"bin in vm.HelpersService.toArray(vm.binData) | orderBy: 'name'\" class=\"bin-container\" ng-click=\"vm.viewBinResult(bin.name)\">\n        <table style=\"border: 1px solid black;font-size:16px\">\n            <tr>\n                <td style=\"width: 50px\">{{bin.uniqueEntityCount}}</td>\n                <td style=\"width: 50px\">{{bin.totalEntityCount}}</td>\n            </tr>\n            <tr>\n                <td colspan=\"2\">{{bin.documentCount}}</td>\n            </tr>\n        </table>\n        <div class=\"bin-container-title\">{{bin.name}}</div>\n    </div>\n</div>\n<div ng-if=\"vm.AppStateService.getState('showResultTable')\">\n    <div class=\"bin-table-banner\">\n        <span style=\"position: absolute;left: 10px;padding-top: 5px\">\n            <button class=\"results-back-btn\" aria-label=\"Back\" ng-click=\"vm.closeBinResults()\">\n                Back to Results\n            </button>\n        </span>\n        <span class=\"bin-table-title\">{{vm.AppStateService.getState('currentBinView')}}</span>\n\n    </div>\n    <div ag-grid=\"vm.gridOptions\" class=\"ag-fresh\" style=\"height: calc(40vh - 126px);\"></div>\n</div>\n"

/***/ },

/***/ 554:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var ResultsController = (function () {
	    ResultsController.$inject = ["$mdDialog", "$scope", "$timeout", "AppStateService", "HelpersService", "RipItService", "StorageService", "TableService"];
	    function ResultsController($mdDialog, $scope, $timeout, AppStateService, HelpersService, RipItService, StorageService, TableService) {
	        'ngInject';
	
	        _classCallCheck(this, ResultsController);
	
	        this.$scope = $scope;
	        this.$timeout = $timeout;
	        this.AppStateService = AppStateService;
	        this.HelpersService = HelpersService;
	        this.RipItService = RipItService;
	        this.StorageService = StorageService;
	        this.TableService = TableService;
	        this.$mdDialog = $mdDialog;
	
	        this.binData = {};
	
	        this.gridOptions = angular.copy(this.TableService.defaultGridOptions);
	
	        this.initialize();
	        this.listeners();
	        //this.watchers();
	    }
	
	    _createClass(ResultsController, [{
	        key: 'initialize',
	        value: function initialize() {
	            //
	        }
	    }, {
	        key: 'viewBinResult',
	        value: function viewBinResult(binName) {
	            this.showResultTable = true;
	            this.AppStateService.setState('showResultTable', true);
	            this.AppStateService.setState('currentBinView', binName);
	
	            this.generateTableData(binName);
	        }
	    }, {
	        key: 'generateTableData',
	        value: function generateTableData(binName) {
	            var _this = this;
	
	            this.$timeout(function () {
	                var binResultData = _this.generateBinResultData(binName);
	                _this.gridOptions.api.setColumnDefs(_this.TableService.generateTableHeaders(binResultData, { colName: 'Count' }));
	                _this.gridOptions.api.setRowData(binResultData);
	                _this.gridOptions.api.sizeColumnsToFit();
	            });
	        }
	    }, {
	        key: 'closeBinResults',
	        value: function closeBinResults() {
	            this.AppStateService.setState('showResultTable', false);
	            this.AppStateService.setState('currentBinView', '');
	            this.binData = this.generateBinOverviewResultData();
	        }
	    }, {
	        key: 'generateBinOverviewResultData',
	        value: function generateBinOverviewResultData() {
	            return this.RipItService.generateBinOverviewResultData();
	        }
	    }, {
	        key: 'generateBinResultData',
	        value: function generateBinResultData(binName) {
	            return this.RipItService.generateBinResultData(binName);
	        }
	    }, {
	        key: 'generateTableHeaders',
	        value: function generateTableHeaders(data, sortObj) {
	            return this.RipItService.generateTableHeaders(data, sortObj);
	        }
	    }, {
	        key: 'watchers',
	        value: function watchers() {
	            var _this2 = this;
	
	            this.$scope.$watch(function () {
	                return _this2.StorageService.activeDocuments;
	            }, function () {
	                _this2.binData = _this2.generateBinOverviewResultData();
	            }, true);
	        }
	    }, {
	        key: 'listeners',
	        value: function listeners() {
	            var _this3 = this;
	
	            // TODO: this is a hack for now, and needs to be changed
	            this.$scope.$root.$onMany(['files-changed', 'data-ripped', 'build-results'], function () {
	                if (_this3.AppStateService.getState('ResultsView') === 'Results') {
	                    if (_this3.AppStateService.getState('showResultTable')) {
	                        _this3.generateTableData(_this3.AppStateService.getState('currentBinView'));
	                    } else {
	                        _this3.binData = _this3.generateBinOverviewResultData();
	                    }
	                }
	            });
	        }
	    }]);
	
	    return ResultsController;
	})();
	
	exports['default'] = ResultsController;
	module.exports = exports['default'];

/***/ },

/***/ 555:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(556);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(305)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/sass-loader/index.js!./results.scss", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/sass-loader/index.js!./results.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 556:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(304)();
	// imports
	
	
	// module
	exports.push([module.id, ".bin-container {\n  height: 85px;\n  width: 101px;\n  display: inline-block;\n  font-size: 24px;\n  margin-right: 5px;\n  margin-bottom: 10px;\n  vertical-align: top; }\n\n.bin-container > table,\n.bin-container > table td {\n  border: 1px solid black;\n  table-layout: fixed;\n  text-align: center; }\n\n.bin-container-title {\n  font-size: 14px;\n  text-align: center; }\n\n.bin-table-banner {\n  text-align: center;\n  height: 30px; }\n\n.bin-table-banner:before,\n.bin-table-banner:after {\n  content: \" \";\n  display: table; }\n\n.bin-table-banner:after {\n  clear: both; }\n\n.bin-table-title {\n  font-size: 24px;\n  font-weight: bold; }\n", ""]);
	
	// exports


/***/ },

/***/ 639:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _ripitActionsComponentJs = __webpack_require__(640);
	
	var _ripitActionsComponentJs2 = _interopRequireDefault(_ripitActionsComponentJs);
	
	var ripitActionsModule = _angular2['default'].module('ripitActions', []).component('ripitActions', _ripitActionsComponentJs2['default']);
	
	exports['default'] = ripitActionsModule;
	module.exports = exports['default'];

/***/ },

/***/ 640:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _ripitActionsHtml = __webpack_require__(641);
	
	var _ripitActionsHtml2 = _interopRequireDefault(_ripitActionsHtml);
	
	var _ripitActionsController = __webpack_require__(642);
	
	var _ripitActionsController2 = _interopRequireDefault(_ripitActionsController);
	
	__webpack_require__(643);
	
	var ripitActionsComponent = {
	    restrict: 'E',
	    bindings: {},
	    template: _ripitActionsHtml2['default'],
	    controller: _ripitActionsController2['default']
	};
	
	exports['default'] = ripitActionsComponent;
	module.exports = exports['default'];

/***/ },

/***/ 641:
/***/ function(module, exports) {

	module.exports = "<md-card>\n    <md-card-content class=\"small-padding\">\n        <md-menu md-offset=\"0 -7\">\n            <md-button aria-label=\"Open demo menu\" class=\"md-button md-raised\" ng-click=\"$mdOpenMenu($event)\">\n                Export\n            </md-button>\n            <md-menu-content width=\"2\">\n                <md-menu-item>\n                    <md-button class=\"\" ng-click=\"$ctrl.exportXLSX($event)\">Excel</md-button>\n                </md-menu-item>\n                <md-menu-item>\n                    <md-button class=\"\" ng-click=\"$ctrl.exportKML($event)\">KML (Coming Soon)</md-button>\n                </md-menu-item>\n                <md-menu-item>\n                    <md-button class=\"\" ng-click=\"$ctrl.exportANB($event)\">ANB (Coming Soon)</md-button>\n                </md-menu-item>\n            </md-menu-content>\n        </md-menu>\n\n    </md-card-content>\n</md-card>"

/***/ },

/***/ 642:
/***/ function(module, exports, __webpack_require__) {

	/* eslint-disable consistent-return */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var RipitActionsController = (function () {
	    RipitActionsController.$inject = ["$mdDialog", "$q", "$rootScope", "RipItService", "StorageService", "ParseProvider", "ResultsExportService", "$scope"];
	    function RipitActionsController($mdDialog, $q, $rootScope, RipItService, StorageService, ParseProvider, ResultsExportService, $scope) {
	        'ngInject';
	
	        _classCallCheck(this, RipitActionsController);
	
	        this.$mdDialog = $mdDialog;
	        this.$q = $q;
	        this.$scope = $scope;
	        this.$rootScope = $rootScope;
	        this.RipItService = RipItService;
	        this.StorageService = StorageService;
	        this.ParseProvider = ParseProvider;
	        this.ResultsExportService = ResultsExportService;
	    }
	
	    _createClass(RipitActionsController, [{
	        key: 'exportXLSX',
	        value: function exportXLSX(evt) {
	            if (!this.RipItService.binsList.length) {
	                this.$mdDialog.show(this.$mdDialog.alert().clickOutsideToClose(true).title('No Data to Export').textContent('Please add files and click RipIt to enable the Export feature').ariaLabel('Alert Modal').ok('OK').targetEvent(evt));
	                return false;
	            }
	
	            this.ResultsExportService.toXLSX();
	        }
	    }, {
	        key: 'exportKML',
	        value: function exportKML(evt) {
	            this.$mdDialog.show(this.$mdDialog.alert().clickOutsideToClose(true).title('KML Export').textContent('This feature is not implemented at this time.').ariaLabel('Alert Modal').ok('OK').targetEvent(evt));
	            return false;
	        }
	    }, {
	        key: 'exportANB',
	        value: function exportANB(evt) {
	            this.$mdDialog.show(this.$mdDialog.alert().clickOutsideToClose(true).title('ANB Export').textContent('This feature is not implemented at this time.').ariaLabel('Alert Modal').ok('OK').targetEvent(evt));
	            return false;
	        }
	    }]);
	
	    return RipitActionsController;
	})();
	
	exports['default'] = RipitActionsController;
	module.exports = exports['default'];

/***/ },

/***/ 643:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(644);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(305)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./ripitActions.scss", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./ripitActions.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 644:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(304)();
	// imports
	
	
	// module
	exports.push([module.id, ".ripitActions {\n  color: red; }\n", ""]);
	
	// exports


/***/ },

/***/ 645:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	__webpack_require__(295);
	
	var _treeComponent = __webpack_require__(646);
	
	var _treeComponent2 = _interopRequireDefault(_treeComponent);
	
	__webpack_require__(651);
	
	var treeModule = _angular2['default'].module('tree', ['ui.router', 'ivh.treeview']).directive('tree', _treeComponent2['default']).config(["ivhTreeviewOptionsProvider", function (ivhTreeviewOptionsProvider) {
	    'ngInject';
	    ivhTreeviewOptionsProvider.set({
	        idAttribute: 'id',
	        labelAttribute: 'label',
	        childrenAttribute: 'children',
	        selectedAttribute: 'selected',
	        useCheckboxes: true,
	        expandToDepth: 0,
	        indeterminateAttribute: '__ivhTreeviewIndeterminate',
	        defaultSelectedState: true,
	        validate: true,
	        twistieExpandedTpl: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28" width="28" height="28" style="margin-bottom:-10px;"><path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"></path></svg>',
	        twistieCollapsedTpl: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28" width="28" height="28" style="margin-bottom:-10px;"><path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"></path></path></svg>',
	        twistieLeafTpl: '',
	        // nodeTpl: ['<div class="ivh-treeview-node-content" title="{{trvw.label(node)}}">', '<span ivh-treeview-toggle>', '<span class="ivh-treeview-twistie-wrapper" ivh-treeview-twistie></span>', '</span>', '<span class="ivh-treeview-checkbox-wrapper" ng-if="trvw.useCheckboxes()"', 'ivh-treeview-checkbox>', '</span>', '<span class="ivh-treeview-node-label" ivh-treeview-toggle>', '{{trvw.label(node)}}', '</span>','<div>{{ node }}</div>', '<div ivh-treeview-children></div>', '</div>'].join('\n')nodeTpl: ['<div class="ivh-treeview-node-content" title="{{trvw.label(node)}}">', '<span ivh-treeview-toggle>', '<span class="ivh-treeview-twistie-wrapper" ivh-treeview-twistie></span>', '</span>', '<span class="ivh-treeview-checkbox-wrapper" ng-if="trvw.useCheckboxes()"', 'ivh-treeview-checkbox>', '</span>', '<span class="ivh-treeview-node-label" ivh-treeview-toggle>', '{{trvw.label(node)}}', '</span>','<div>{{ node }}</div>', '<div ivh-treeview-children></div>', '</div>'].join('\n')
	        nodeTpl: '\n                <div class="ivh-treeview-node-content" title="{{trvw.label(node)}}">\n                    <span ng-if="trvw.label(node) !== \'CUSTOM_REGEX_INPUT_DIRECTIVE\'" ivh-treeview-toggle style="padding-right:5px">\n                        <span ng-if="trvw.label(node) !== \'CUSTOM_REGEX_INPUT_DIRECTIVE\'" class="ivh-treeview-twistie-wrapper" ivh-treeview-twistie></span>\n                    </span>\n                    <span ng-if="trvw.label(node) !== \'CUSTOM_REGEX_INPUT_DIRECTIVE\'" class="ivh-treeview-checkbox-wrapper" ng-if="trvw.useCheckboxes()" ivh-treeview-checkbox ></span>\n                    <span  class="ivh-treeview-node-label" ivh-treeview-toggle>\n                        <span ng-if="trvw.label(node) !== \'CUSTOM_REGEX_INPUT_DIRECTIVE\'">\n                            {{trvw.label(node)}}\n                        </span>\n                        <div ng-if="trvw.label(node) === \'CUSTOM_REGEX_INPUT_DIRECTIVE\'">\n                            <custom-regex-input></custom-regex-input>\n                        </div>\n                    </span>\n                    <ul style="padding-left:31px">\n                        <li ng-repeat="node in node.options">\n                            {{ node.label + \' \' }}<md-checkbox class="" aria-label="close" ng-model="node.defaults"></md-checkbox>\n                        </li>\n                    </ul>\n                    <div ivh-treeview-children></div>\n                </div>\n            '
	    });
	}]);
	
	exports['default'] = treeModule;
	module.exports = exports['default'];

/***/ },

/***/ 646:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _treeHtml = __webpack_require__(647);
	
	var _treeHtml2 = _interopRequireDefault(_treeHtml);
	
	var _treeControllerJs = __webpack_require__(648);
	
	var _treeControllerJs2 = _interopRequireDefault(_treeControllerJs);
	
	__webpack_require__(649);
	
	__webpack_require__(651);
	
	__webpack_require__(652);
	
	var treeComponent = function treeComponent() {
	    return {
	        template: _treeHtml2['default'],
	        controller: _treeControllerJs2['default'],
	        restrict: 'E',
	        controllerAs: 'vm',
	        scope: {},
	        bindToController: true
	    };
	};
	
	exports['default'] = treeComponent;
	module.exports = exports['default'];

/***/ },

/***/ 647:
/***/ function(module, exports) {

	module.exports = "<md-card style=\"height:calc(100% - 16px)\">\n    <div class=\"section-headers\">\n        <span>\n            Extraction Selector\n        </span>\n\n    </div>\n    <md-card-content class=\"small-padding\" style=\"overflow-y: auto;overflow-x: hidden;\">\n        <md-card-actions>\n            <md-button class=\"md-raised\" ng-click=\"vm.selectAll()\" aria-label=\"Select All\">\n                Select All\n            </md-button>\n            <md-button class=\"md-raised\" ng-click=\"vm.deselectAll()\" aria-label=\"Clear All\">\n                Clear All\n            </md-button>\n            <!--<md-button class=\"md-icon-button md-raised\" ng-click=\"vm.selectAll()\" aria-label=\"Select All\">-->\n                <!--<ng-md-icon icon=\"select_all\" size=\"24\" view-box=\"0 0 24 24\"></ng-md-icon>-->\n            <!--</md-button>-->\n            <!--<md-button class=\"md-icon-button md-raised\" ng-click=\"vm.deselectAll()\" aria-label=\"Clear All\">-->\n                <!--<ng-md-icon icon=\"clear_all\" size=\"24\" view-box=\"0 0 24 24\"></ng-md-icon>-->\n            <!--</md-button>-->\n        </md-card-actions>\n        <div\n            ivh-treeview=\"vm.treeData\"\n            ivh-treeview-on-toggle=\"vm.traverseCallback(ivhNode, ivhIsExpanded, ivhTree)\"\n            ivh-treeview-on-cb-change=\"vm.changeCallback(ivhNode, ivhIsSelected, ivhTree)\">\n        </div>\n\n    </md-card-content>\n</md-card>\n"

/***/ },

/***/ 648:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var TreeController = (function () {
	    TreeController.$inject = ["$mdDialog", "APP_SETTINGS", "AppStateService", "ivhTreeviewMgr", "StorageService"];
	    function TreeController($mdDialog, APP_SETTINGS, AppStateService, ivhTreeviewMgr, StorageService) {
	        'ngInject';
	
	        _classCallCheck(this, TreeController);
	
	        this.$mdDialog = $mdDialog;
	        this.APP_SETTINGS = APP_SETTINGS;
	        this.AppStateService = AppStateService;
	        this.ivhTreeviewMgr = ivhTreeviewMgr;
	        this.StorageService = StorageService;
	
	        this.treeData = this.APP_SETTINGS.regexPatterns;
	        this.isOpen = false;
	    }
	
	    _createClass(TreeController, [{
	        key: 'selectAll',
	        value: function selectAll() {
	            this.ivhTreeviewMgr.selectAll(this.treeData);
	        }
	    }, {
	        key: 'deselectAll',
	        value: function deselectAll() {
	            this.ivhTreeviewMgr.deselectAll(this.treeData);
	        }
	    }, {
	        key: 'changeCallback',
	        value: function changeCallback() {
	            this.AppStateService.setState('needsRipping', true);
	        }
	    }]);
	
	    return TreeController;
	})();
	
	exports['default'] = TreeController;
	module.exports = exports['default'];

/***/ },

/***/ 649:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(650);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(305)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./tree.scss", function() {
				var newContent = require("!!./../../../../node_modules/css-loader/index.js!./../../../../node_modules/sass-loader/index.js!./tree.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 650:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(304)();
	// imports
	
	
	// module
	exports.push([module.id, ".tree {\n  color: red; }\n\nul {\n  list-style: none;\n  padding-left: 15px; }\n\n.ivh-treeview-twistie {\n  width: 18px;\n  display: inline-block; }\n\nmd-checkbox.md-checked.blue .md-icon {\n  background-color: rgba(16, 108, 220, 0.87); }\n", ""]);
	
	// exports


/***/ },

/***/ 654:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	// import 'SVG-Morpheus';
	
	__webpack_require__(311);
	
	var _customRegexInputComponentJs = __webpack_require__(655);
	
	var _customRegexInputComponentJs2 = _interopRequireDefault(_customRegexInputComponentJs);
	
	var customRegexInputModule = _angular2['default'].module('customRegexInput', ['ngMdIcons']).directive('customRegexInput', _customRegexInputComponentJs2['default']);
	
	exports['default'] = customRegexInputModule;
	module.exports = exports['default'];

/***/ },

/***/ 655:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _customRegexInputHtml = __webpack_require__(656);
	
	var _customRegexInputHtml2 = _interopRequireDefault(_customRegexInputHtml);
	
	var _customRegexInputControllerJs = __webpack_require__(657);
	
	var _customRegexInputControllerJs2 = _interopRequireDefault(_customRegexInputControllerJs);
	
	__webpack_require__(658);
	
	var customRegexInputComponent = function customRegexInputComponent() {
	    return {
	        template: _customRegexInputHtml2['default'],
	        controller: _customRegexInputControllerJs2['default'],
	        restrict: 'E',
	        controllerAs: 'vm',
	        scope: {},
	        bindToController: true
	    };
	};
	
	exports['default'] = customRegexInputComponent;
	module.exports = exports['default'];

/***/ },

/***/ 656:
/***/ function(module, exports) {

	module.exports = "<div class=\"row\" style=\"margin:5px; border:1px solid #cdcdcd\">\n    <div class=\"col-xs-12\">\n        <div class=\"row\">\n            <div class=\"col-xs-7\">\n                <div class=\"row\">\n                    <div class=\"col-xs-12\">\n                        <span class=\"exp-decorator\">/</span>\n                        <input ng-model=\"vm.result.pattern\" type=\"text\" class=\"regex-input\">\n                        <span class=\"exp-decorator\">/</span>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-xs-12\">\n                        <md-select aria-label=\"custom\" ng-model=\"vm.result.bin\" style=\"margin:0\">\n                            <md-option value=\"Custom\" selected=\"\">Custom</md-option>\n                            <md-option ng-repeat=\"bin in vm.bins\" value=\"{{bin.label}}\">\n                            {{bin.label}}\n                            </md-option>\n                        </md-select>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-xs-3\">\n                <div class=\"regex-flags-container\">\n                    <div ng-repeat=\"(flag, value) in vm.flags\" style=\"line-height:0;font-size:0\">\n                        <ng-md-icon icon=\"{{value ? 'check_box' : 'check_box_outline_blank'}}\" size=\"16\"\n                                    ng-click=\"vm.selectFlag(flag)\"></ng-md-icon>\n\t\t\t<span class=\"flag\">\n\t\t\t\t{{flag}}\n\t\t\t</span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-xs-2\">\n                <ng-md-icon icon=\"done\" size=\"48\" style=\"fill:{{vm.result.pattern ? '#00E676' : '#757575'}}\"\n                            ng-click=\"vm.save(vm.result)\"></ng-md-icon>\n            </div>\n        </div>\n    </div>\n</div>\n\n"

/***/ },

/***/ 657:
/***/ function(module, exports, __webpack_require__) {

	/* eslint-disable no-param-reassign */
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _angular = __webpack_require__(293);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var CustomRegexInputController = (function () {
	    CustomRegexInputController.$inject = ["APP_SETTINGS"];
	    function CustomRegexInputController(APP_SETTINGS) {
	        'ngInject';
	
	        _classCallCheck(this, CustomRegexInputController);
	
	        this.CUSTOM_BIN = 'Custom';
	
	        this.regexdata = APP_SETTINGS.regexPatterns;
	        this.bins = [];
	        this.buildBins(this.regexdata);
	
	        this.defaultResult = {
	            flags: '',
	            regex: '',
	            bin: this.CUSTOM_BIN
	        };
	        this.defaultFlags = {
	            'ignore case': true,
	            global: true,
	            multiline: false
	        };
	        this.flags = _angular2['default'].copy(this.defaultFlags);
	        this.result = _angular2['default'].copy(this.defaultResult);
	    }
	
	    _createClass(CustomRegexInputController, [{
	        key: 'selectFlag',
	        value: function selectFlag(flag) {
	            if (this.flags[flag]) {
	                this.flags[flag] = false;
	            } else {
	                this.flags[flag] = true;
	            }
	        }
	    }, {
	        key: 'buildBins',
	        value: function buildBins(nodeItem) {
	            var _this = this;
	
	            _angular2['default'].forEach(nodeItem, function (node) {
	                if (node.children) {
	                    _this.buildBins(node.children);
	                } else {
	                    _this.bins.push(node);
	                }
	            });
	
	            this.bins.splice(this.bins.indexOf('custom-child'), 1);
	
	            return this.bins;
	        }
	    }, {
	        key: 'save',
	        value: function save(result) {
	            if (result.pattern) {
	                // this is the format of the node structure that the
	                result.patterns = [{
	                    name: result.pattern,
	                    regex: this.parseRegex(result)
	                }];
	                result.selected = true;
	                result.label = (result.pattern.length > 10 ? '' + result.pattern.substr(0, 10) : result.pattern.substr(0, 10)) + ' (' + result.bin + ')';
	
	                // clean up the result object before it is inserted into the regex array
	                delete result.flags;
	                delete result.regex;
	                delete result.pattern;
	
	                // new pattern to regex object (this could probably be made more configurable)
	                _angular2['default'].forEach(this.regexdata, function (data) {
	                    if (data.label === 'Custom') {
	                        // set Custom's checkbox to true when adding a custom regex
	                        data.selected = true;
	                        data.children.push(result);
	                    }
	                });
	
	                // reset form variables
	                this.flags = _angular2['default'].copy(this.defaultFlags);
	                this.result = _angular2['default'].copy(this.defaultResult);
	            }
	        }
	    }, {
	        key: 'parseRegex',
	        value: function parseRegex(result) {
	            var regExp = new RegExp(result.pattern, this.parseFlags(this.flags));
	            return regExp;
	        }
	    }, {
	        key: 'parseFlags',
	        value: function parseFlags(flags) {
	            var flagStr = '';
	            _angular2['default'].forEach(flags, function (value, flag) {
	                if (flag === 'ignore case' && value === true) {
	                    flagStr += 'i';
	                } else if (flag === 'global' && value === true) {
	                    flagStr += 'g';
	                } else if (flag === 'multiline' && value === true) {
	                    flagStr += 'm';
	                }
	            });
	            return flagStr;
	        }
	    }]);
	
	    return CustomRegexInputController;
	})();
	
	exports['default'] = CustomRegexInputController;
	module.exports = exports['default'];

/***/ },

/***/ 658:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(659);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(305)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/sass-loader/index.js!./customRegexInput.scss", function() {
				var newContent = require("!!./../../../../../node_modules/css-loader/index.js!./../../../../../node_modules/sass-loader/index.js!./customRegexInput.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 659:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(304)();
	// imports
	
	
	// module
	exports.push([module.id, ".exp-decorator {\n  color: #B9BABF;\n  font-weight: 700; }\n\n.regex-input-container {\n  width: 70%;\n  float: left; }\n\n.regex-input {\n  font-size: 12pt;\n  width: 90%;\n  border: none;\n  padding: 0; }\n\n.regex-flags-container {\n  width: 85px;\n  display: inline-block; }\n\n.flag {\n  font-size: 10pt;\n  line-height: 0; }\n", ""]);
	
	// exports


/***/ },

/***/ 660:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _appHtml = __webpack_require__(661);
	
	var _appHtml2 = _interopRequireDefault(_appHtml);
	
	__webpack_require__(662);
	
	var appComponent = {
	    template: _appHtml2['default'],
	    restrict: 'E'
	};
	
	exports['default'] = appComponent;
	module.exports = exports['default'];

/***/ },

/***/ 661:
/***/ function(module, exports) {

	module.exports = "<!--Anything you want to be on every page, place it in this file-->\n<navbar></navbar>\n\n<div class=\"app container-fluid\">\n    <div class=\"row\">\n        <div class=\"col-xs-3\">\n            <div class=\"row\">\n                <div class=\"col-xs-12 file-manager-container uncol\">\n                    <file-manager></file-manager>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-xs-12 query-builder-container uncol\">\n                    <query-builder></query-builder>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"col-xs-9\">\n            <div class=\"row\">\n                <div class=\"col-xs-8 uncol document-container\">\n                    <document></document>\n                </div>\n                <div class=\"col-xs-4\">\n                    <div class=\"row\">\n                        <div class=\"col-xs-12 uncol ripit-actions-container\">\n                            <ripit-actions></ripit-actions>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-xs-12 uncol regex-tree-container\">\n                            <tree></tree>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-xs-12 uncol result-container\">\n                    <result-output></result-output>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ },

/***/ 662:
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(663);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(305)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../node_modules/css-loader/index.js!./../../node_modules/sass-loader/index.js!./app.scss", function() {
				var newContent = require("!!./../../node_modules/css-loader/index.js!./../../node_modules/sass-loader/index.js!./app.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },

/***/ 663:
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(304)();
	// imports
	
	
	// module
	exports.push([module.id, "body {\n  min-width: 1200px; }\n\n.app {\n  height: calc(100vh - 50px);\n  background-color: #EFEFEF; }\n\n.file-manager-container {\n  height: calc(50vh - 32px); }\n\n.query-builder-container {\n  height: calc(50vh - 18px); }\n\n.document-container {\n  height: calc(60vh - 25px); }\n\n.result-container {\n  height: calc(40vh - 32px); }\n\n.ripit-actions-container {\n  height: 75px; }\n\n.regex-tree-container {\n  height: calc(60vh - 93px);\n  overflow-y: auto; }\n\n.see {\n  border: 1px dashed black; }\n\n.section-headers {\n  height: 30px;\n  color: black;\n  text-shadow: 0 2px 2px rgba(0, 0, 0, 0.3);\n  padding: 0 5px;\n  font-size: 16px;\n  line-height: 23px;\n  font-weight: bold;\n  background: #e2e2e2;\n  background: -moz-linear-gradient(top, #e2e2e2 0%, #dbdbdb 78%, #d1d1d1 80%, #fefefe 100%);\n  background: -webkit-gradient(left top, left bottom, color-stop(0%, #e2e2e2), color-stop(78%, #dbdbdb), color-stop(80%, #d1d1d1), color-stop(100%, #fefefe));\n  background: -webkit-linear-gradient(top, #e2e2e2 0%, #dbdbdb 78%, #d1d1d1 80%, #fefefe 100%);\n  background: -o-linear-gradient(top, #e2e2e2 0%, #dbdbdb 78%, #d1d1d1 80%, #fefefe 100%);\n  background: -ms-linear-gradient(top, #e2e2e2 0%, #dbdbdb 78%, #d1d1d1 80%, #fefefe 100%);\n  background: linear-gradient(to bottom, #e2e2e2 0%, #dbdbdb 78%, #d1d1d1 80%, #fefefe 100%); }\n\n.no-padding {\n  padding: 0; }\n\n.small-padding {\n  padding: 8px; }\n\n.full-height {\n  height: 100%; }\n\n.full-height-8 {\n  height: calc(100% - 8px); }\n\n.full-height-8 {\n  height: calc(100% - 16px); }\n\n.no-select {\n  -webkit-user-select: none;\n  /* Chrome all / Safari all */\n  -moz-user-select: none;\n  /* Firefox all */\n  -ms-user-select: none;\n  /* IE 10+ */\n  user-select: none;\n  /* Likely future */ }\n\n.pointer {\n  cursor: pointer !important; }\n\n.unrow {\n  margin-right: 0 !important;\n  margin-left: 0 !important; }\n\n.uncol {\n  padding-right: 0 !important;\n  padding-left: 0 !important; }\n\n:focus {\n  outline: none; }\n\n.hvr-grow {\n  display: inline-block;\n  vertical-align: middle;\n  transform: translateZ(0);\n  box-shadow: 0 0 1px transparent;\n  backface-visibility: hidden;\n  -moz-osx-font-smoothing: grayscale;\n  transition-duration: 0.3s;\n  transition-property: transform; }\n\n.hvr-grow:hover, .hvr-grow:focus, .hvr-grow:active {\n  transform: scale(1.1); }\n\n.ag-fresh .ag-body .ag-row-odd {\n  background-color: rgba(33, 150, 243, 0.1); }\n\n.ag-font-style {\n  -webkit-user-select: inherit;\n  -moz-user-select: inherit;\n  -ms-user-select: inherit;\n  user-select: inherit; }\n\nmd-tabs:not(.md-no-tab-content):not(.md-dynamic-height) {\n  min-height: 0; }\n\n.info-text {\n  font-size: 1.6rem;\n  font-weight: 400;\n  letter-spacing: 0.010em;\n  line-height: 1.6em;\n  margin: 0.8em 0 1.6em; }\n\n#log-iframe {\n  position: absolute;\n  top: -100px;\n  height: 1px;\n  width: 1px;\n  left: -100px;\n  visibility: hidden; }\n", ""]);
	
	// exports


/***/ }

});
//# sourceMappingURL=app.bundle.js.map