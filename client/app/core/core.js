import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-material';
import ngMdIcons from 'angular-material-icons';

import constantsModule from './constants.js';
import configsModule from './configs/configs.js';

import RipItService from './ripit.service.js';

let coreModule = angular
    .module('ripit.core', [
        uiRouter,
        ngMdIcons,
        configsModule.name,
        constantsModule.name
    ])

    .service('RipItService', RipItService);

export default coreModule;
