import _ from 'lodash';
import angular from 'angular';
import GeoConverter from 'geo-converter';

class RipItService {
    constructor($rootScope, AppStateService, HelpersService, ParseProvider, StorageService) {
        'ngInject';

        this.$rootScope = $rootScope;
        this.AppStateService = AppStateService;
        this.HelpersService = HelpersService;
        this.ParseProvider = ParseProvider;
        this.StorageService = StorageService;

        this.resultObjectMetaProperties = ['hitLength', 'indexes', 'label', 'count', 'docName', 'mainGroup'];
        this.binsList = [];
    }

    parse() {
        angular.forEach(this.StorageService.documents, (doc, name) => {
            if (doc.valid) {
                if (this.AppStateService.getState('needsRipping') || doc.needsParsing) {
                    if (!this.StorageService.documents[name].results) {
                        this.StorageService.documents[name].results = {};
                    }
                    this.StorageService.documents[name].results = this.ParseProvider.ripIt(doc);
                    doc.needsParsing = false;
                }
            }
        });

        // this.AppStateService.setState('needsRipping', false);
        this.$rootScope.$broadcast('data-ripped');
    }

    generateBinsList() {
        let output = {};

        for (let docName in this.StorageService.activeDocuments) {
            if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
                let results = this.StorageService.activeDocuments[docName].results;
                // loop through each bin in the result set of the document
                for (let binName in results) {
                    if (!output[binName]) {
                        output[binName] = {
                            name: binName,
                            count: 0
                        };
                    }
                    output[binName].count += results[binName].length;
                }
            }
        }
        output = _.values(output);

        // ensure we do not lose the object reference
        this.binsList.length = 0;
        return Array.prototype.push.apply(this.binsList, output);

        // let output = this.generateBinOverviewResultData();
        // this.HelpersService.emptyObject(this.binsList);
        // angular.merge(this.binsList, output);

        // return output;
    }

    generateMatchMakerData() {
        let matchMakerAll = {};

        let binsList = angular.copy(this.binsList);
        let bins = [];

        // for the All bin
        let entityProps = new Set();
        let docNameProps = new Set();
        for (let bin in binsList) {
            if (binsList.hasOwnProperty(bin)) {
                if (binsList[bin].name !== 'CUSTOM_REGEX_INPUT_DIRECTIVE') {
                    bins.push(binsList[bin].name);
                }
            }
        }

        for (let i = 0, len = bins.length; i < len; i++) {
            let binData = this.generateBinResultData(bins[i]);
            let metaProperties = ['Count', 'Document'];

            let matchMaker = {};
            let matchMakerTable = {};
            let extractedEntityProperties = new Set();

            for (let i = 0, len = binData.length; i < len; i++) {
                // build up the unique key
                let uniqueKey = [];
                for (let d in binData[i]) {
                    if (binData[i].hasOwnProperty(d)) {
                        if (!metaProperties.includes(d)) {
                            uniqueKey.push(binData[i][d]);
                            extractedEntityProperties.add(d);

                        }
                    }
                }
                uniqueKey = uniqueKey.join('|');

                if (!matchMaker[uniqueKey]) {
                    matchMaker[uniqueKey] = {};
                }
                if (!matchMaker[uniqueKey][binData[i].Document]) {
                    matchMaker[uniqueKey][binData[i].Document] = [];
                }
                matchMaker[uniqueKey][binData[i].Document].push(binData[i])
            }

            // remove entities that have less than 2 matches between documents
            for (let uniqueKey in matchMaker) {
                if (matchMaker.hasOwnProperty(uniqueKey)) {
                    if (Object.keys(matchMaker[uniqueKey]).length < 2) {
                        delete matchMaker[uniqueKey];
                    }
                }
            }
            let headers = new Set();
            for (let entity of extractedEntityProperties) {
                headers.add(entity);
                entityProps.add(entity);
            }

            headers.add('Total');
            for (let uniqueKey in matchMaker) {
                if (matchMaker.hasOwnProperty(uniqueKey)) {
                    for (let u in matchMaker[uniqueKey]) {
                        if (matchMaker[uniqueKey].hasOwnProperty(u)) {
                            headers.add(u.replace(/\./g, ''));
                            docNameProps.add(u.replace(/\./g, ''));
                        }
                    }
                }
            }
            let defaultRow = {};

            for (let h of headers) {
                if (h === 'Total') {
                    defaultRow[h] = 0;
                } else {
                    defaultRow[h] = '';
                }
            }
            for (let uniqueKey in matchMaker) {
                if (matchMaker.hasOwnProperty(uniqueKey)) {
                    for (let u in matchMaker[uniqueKey]) {
                        if (matchMaker[uniqueKey].hasOwnProperty(u)) {
                            if (!matchMakerTable[uniqueKey]) {
                                matchMakerTable[uniqueKey] = angular.copy(defaultRow);
                                for (let entity of extractedEntityProperties) {
                                    matchMakerTable[uniqueKey][entity] = matchMaker[uniqueKey][u][0][entity];
                                }
                                matchMakerTable[uniqueKey][matchMaker[uniqueKey][u][0].Document.replace(/\./g, '')] = 'True';
                                matchMakerTable[uniqueKey].Total++;
                            } else {
                                matchMakerTable[uniqueKey][matchMaker[uniqueKey][u][0].Document.replace(/\./g, '')] = 'True';
                                matchMakerTable[uniqueKey].Total++;
                            }
                        }
                    }
                }
            }

            matchMakerAll[bins[i]] = _.values(matchMakerTable);
        }


        // build all bin
        let allHeaders = new Set();

        for (let matchMaker in matchMakerAll) {
            if (matchMakerAll.hasOwnProperty(matchMaker)) {
                for (let i = 0, len = matchMakerAll[matchMaker].length; i < len; i++) {
                    for (let name in matchMakerAll[matchMaker][i]) {
                        if (matchMakerAll[matchMaker][i].hasOwnProperty(name)) {
                            if (Array.from(entityProps).includes(name)) {
                                allHeaders.add(name);
                            }
                        }
                    }
                }
            }
        }

        allHeaders.add('Total');
        for (let docName of docNameProps) {
            allHeaders.add(docName);
        }
        console.log(allHeaders);

        let defaultRow = {};

        for (let h of allHeaders) {
            defaultRow[h] = '';
        }

        matchMakerAll.All = [];
        for (let bin in matchMakerAll) {
            if (matchMakerAll.hasOwnProperty(bin)) {
                if (bin !== 'All') {
                    for (let i = 0, len = matchMakerAll[bin].length; i < len; i++) {
                        matchMakerAll.All = matchMakerAll.All.concat(angular.merge(angular.copy(defaultRow), matchMakerAll[bin][i]));
                    }
                }
            }
        }

        return matchMakerAll;
    }

    generateBinOverviewResultData() {
        let output = {};

        // loop through all of the documents
        for (let docName in this.StorageService.activeDocuments) {
            if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
                let results = this.StorageService.activeDocuments[docName].results;

                // loop through each bin in the result set of the document
                for (let binName in results) {
                    if (results.hasOwnProperty(binName)) {
                        // initialize the bin result count
                        if (!output[binName] && binName !== 'CUSTOM_REGEX_INPUT_DIRECTIVE') {
                            output[binName] = {
                                name: binName,
                                uniqueEntities: new Set(),
                                totalEntityCount: 0,
                                documentCount: 0
                            };
                        }

                        if (results[binName].length) {
                            for (let i = 0, len = results[binName].length; i < len; i++) {
                                // get sum of all entities across all documents
                                let entity = results[binName][i].mainGroup || results[binName][i][binName];
                                output[binName].uniqueEntities.add(results[binName][i][entity]);

                                // get total count of entities
                                output[binName].totalEntityCount += Number(results[binName][i].count);
                            }
                            // increment the document count
                            output[binName].documentCount++;
                        }
                    }
                }
            }
        }

        for (let binName in output) {
            if (output.hasOwnProperty(binName)) {
                output[binName].uniqueEntityCount = output[binName].uniqueEntities.size;
                delete output[binName].uniqueEntities;
            }
        }

        return output;
    }

    generateBinResultData(binName) {
        let output = [];
        let tempObj = {};
        let extractedEntityProperties = [];

        // loop through Storage to get all documents
        for (let docName in this.StorageService.activeDocuments) {
            tempObj[docName] = {};
            if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
                // get to the selected bin
                let bin = angular.copy(this.StorageService.activeDocuments[docName].results[binName]);

                if (bin && bin.length) {
                    for (let i = 0, len = bin.length; i < len; i++) {
                        // get all of the result properties that are not the static meta properties (we only need to do this once and we only need the first element)
                        if (!extractedEntityProperties.length) {
                            for (let prop in bin[0]) {
                                if (bin[0].hasOwnProperty(prop)) {
                                    if (!this.resultObjectMetaProperties.includes(prop)) {
                                        extractedEntityProperties.push(prop);
                                    }
                                }
                            }
                        }

                        // creating unique key to compare entities across documents
                        let uniqueEntity = [];

                        for (let propCounter = 0, propLength = extractedEntityProperties.length; propCounter < propLength; propCounter++) {
                            uniqueEntity.push(bin[i][extractedEntityProperties[propCounter]]);
                        }
                        let uniqueEntityStr = uniqueEntity.join('|');

                        if (!tempObj[docName][uniqueEntityStr]) {
                            tempObj[docName][uniqueEntityStr] = bin[i];
                            // add proper formatted properties for Count and Document for the table object
                            tempObj[docName][uniqueEntityStr].Document = bin[i].docName;
                            tempObj[docName][uniqueEntityStr].Count = bin[i].count;

                            // this is a temporary solution, we should probably have a post-rip process to handle this type of processing
                            if (binName === 'Locations') {
                                let geoConverter = new GeoConverter(bin[i].Locations);
                                tempObj[docName][uniqueEntityStr].MGRS = geoConverter.mgrs;
                                tempObj[docName][uniqueEntityStr].Lat = geoConverter.geo.lat;
                                tempObj[docName][uniqueEntityStr].Lon = geoConverter.geo.lon;
                            }

                            // remove unnecessary/incorrect properties from the result object
                            for (let prop in bin[i]) {
                                if (bin[i].hasOwnProperty(prop)) {
                                    if (this.resultObjectMetaProperties.includes(prop)) {
                                        delete tempObj[docName][uniqueEntityStr][prop];
                                    }
                                }
                            }
                        } else {
                            tempObj[docName][uniqueEntityStr].Count += bin[i].count;
                        }
                    }
                }
            }
        }

        // convert back to array of objects
        for (let docName in tempObj) {
            if (tempObj.hasOwnProperty(docName)) {
                for (let uniqueEnitity in tempObj[docName]) {
                    if (tempObj[docName].hasOwnProperty(uniqueEnitity)) {
                        output.push(tempObj[docName][uniqueEnitity])
                    }
                }
            }
        }

        return output;
    }

    generateDataForExport() {
        let output = [];
        let tempObj = {};
        let extractedEntityProperties = [];

        // loop through Storage to get all documents
        for (let docName in this.StorageService.activeDocuments) {
            if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
                for (let binName in this.StorageService.activeDocuments[docName].results) {
                    if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
                        let bin = this.StorageService.activeDocuments[docName].results[binName];

                        // get all of the result properties that are not the static meta properties (we only need to do this once and we only need the first element)
                        if (!extractedEntityProperties.length) {
                            for (let prop in bin[0]) {
                                if (bin[0].hasOwnProperty(prop)) {
                                    if (!this.resultObjectMetaProperties.includes(prop)) {
                                        extractedEntityProperties.push(prop);
                                    }
                                }
                            }
                        }

                        for (let i = 0, len = bin.length; i < len; i++) {
                            // creating unique key to compare entities across documents
                            let uniqueEntity = [];

                            for (let propCounter = 0, propLength = extractedEntityProperties.length; propCounter < propLength; propCounter++) {
                                uniqueEntity.push(bin[i][extractedEntityProperties[propCounter]]);
                            }
                            let uniqueEntityStr = uniqueEntity.join('|');

                            if (!tempObj[uniqueEntityStr]) {
                                tempObj[uniqueEntityStr] = bin[i];
                                // add proper formatted properties for Count and Document for the table object
                                tempObj[uniqueEntityStr].Document = bin[i].docName;
                                tempObj[uniqueEntityStr].Count = bin[i].count;

                                // this is a temporary solution, we should probably have a post-rip process to handle this type of processing
                                if (binName === 'Locations') {
                                    let geoConverter = new GeoConverter(bin[i].Locations);
                                    tempObj[uniqueEntityStr].MGRS = geoConverter.mgrs;
                                    tempObj[uniqueEntityStr].Lat = geoConverter.geo.lat;
                                    tempObj[uniqueEntityStr].Lon = geoConverter.geo.lon;
                                }

                                // remove unnecessary/incorrect properties from the result object
                                for (let prop in bin[i]) {
                                    if (bin[i].hasOwnProperty(prop)) {
                                        if (this.resultObjectMetaProperties.includes(prop)) {
                                            delete tempObj[uniqueEntityStr][prop];
                                        }
                                    }
                                }
                            } else {
                                tempObj[uniqueEntityStr].Count += bin[i].count;
                            }
                        }
                    }
                }
                console.log(tempObj);
            }
        }

        return output;
    }
}

export default RipItService;
