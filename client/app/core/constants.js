import angular from 'angular';

import APP_SETTINGS from '../config/settings.js';

let constantsModule = angular.module('ripit.constants', [])
    .constant('APP_SETTINGS', APP_SETTINGS);

export default constantsModule;
