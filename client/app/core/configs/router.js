function Router($locationProvider, $stateProvider, $urlRouterProvider) {
    'ngInject';

    // $locationProvider.html5Mode(true).hashPrefix('#');

    // $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home', {
            url: '/',
            template: '<app></app>'
        })
        .state('index', {
            url: '/index.html',
            template: '<app></app>'
        });
}

export default Router;
