import 'angular-material';

let themeConfig = ($mdThemingProvider) => {
    'ngInject';

    $mdThemingProvider.theme('default')
        .primaryPalette('blue')
        .accentPalette('red');
};

export default themeConfig;
