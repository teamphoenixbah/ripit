import angular from 'angular';

import IconConfig from './icon-config.js';
import Router from './router.js';
import Theme from './theme.js';

let configsModule = angular.module('ripit.configs', [])
    .config(IconConfig)
    .config(Router)
    .config(Theme);

export default configsModule;
