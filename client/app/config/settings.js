/* eslint-disable no-var, spaced-comment */
export default {
    feedbackEmail: 'test2@test.test',
    regexHelpEmail: '',
    catapultBaseUrl: '',
    dirtyWords: [],
    normalizer: {
        'User Names': {
            Facebook: ['fb', 'facebook'],
            WhatsApp: ['whatsapp'],
            Viber: ['viber'],
            Skype: ['skype'],
            Twitter: ['twitter'],
            BlackBerry: ['bblackberry', 'bbbm'],
            Youtube: ['youtube'],
            PalTalk: ['paltalk'],
            Apple: ['apple'],
            Google: ['google'],
            Smukh: ['shmukh'],
            Tango: ['tango'],
            'Al-Fida': ['alfida', 'al fida', 'al-fida'],
            Sina: ['sina'],
            Orkut: ['youtube'],
            AlPlatformMedia: ['alplatformmedia', 'al platform media'],
            'Linked-In': ['linkedin', 'linked in', 'linked-in'],
            Tencent: ['tencent', 'qq'],
            Pinger: ['pinger'],
            Afraid: ['afraid'],
            ChangeIP: ['changeip', 'change ip', 'change-ip'],
            NoIP: ['noip', 'no ip', 'no-ip'],
            Dyn: ['dyn'],
            SiteLutions: ['sitelution', 'sitelutions'],
            Tagged: ['tagged'],
            Bayt: ['bayt'],
            'Cash U': ['cashu', 'cash u', 'cash-u'],
            Gurmad: ['gurmad'],
            Yahoo: ['yahoo'],
            HaninNET: ['haninnet', 'hanin net', 'hanin-net'],
            Instagram: ['instagram']
        }
    },
    regexPatterns: [
        {
            label: 'DNI',
            id: '',
            selected: true,
            children: [
                {
                    label: 'Email',
                    id: '',
                    selected: true,
                    patterns: [
                        {
                            name: 'Email',
                            regex: /(((?:[A-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"))(?:@|\(AT\))(?:(?:[A-z0-9](?:[A-z0-9-]*[A-z0-9])?\.)+[A-z0-9](?:[A-z0-9-]*[A-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[A-z0-9-]*[A-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))/ig,
                            groups: ['Email', 'Username'],
                            mainGroup: 'Email'
                        }
                    ],
                    options: [{
                        name: 'Email',
                        label: 'Exclude .gov, .mil',
                        regex: /(?!.*\.(?:mil|gov)\b)(((?:[A-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"))(?:@|\(AT\))(?:(?:[A-z0-9](?:[A-z0-9-]*[A-z0-9])?\.)+[A-z0-9](?:[A-z0-9-]*[A-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[A-z0-9-]*[A-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))/ig,
                        groups: ['Email', 'Username'],
                        mainGroup: 'Email',
                        defaults: true
                    }]
                },
                {
                    label: 'IP Address',
                    id: '',
                    selected: true,
                    patterns: [
                        {
                            name: 'ip_v4',
                            regex: /(\b[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\b)/ig
                        },
                        {
                            name: 'ip_v6',
                            regex: /(?:[0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,7}:|(?:[0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,5}(?::[0-9a-fA-F]{1,4}){1,2}|(?:[0-9a-fA-F]{1,4}:){1,4}(?::[0-9a-fA-F]{1,4}){1,3}|(?:[0-9a-fA-F]{1,4}:){1,3}(?::[0-9a-fA-F]{1,4}){1,4}|(?:[0-9a-fA-F]{1,4}:){1,2}(?::[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:(?:(?::[0-9a-fA-F]{1,4}){1,6})|:(?:(?:\:[0-9a-fA-F]{1,4}){1,7}|\:)|fe80:(?:\:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(?:ffff(?:\:0{1,4}){0,1}:){0,1}(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])|(?:[0-9a-fA-F]{1,4}:){1,4}:(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])/ig
                        }
                    ]
                },
                {
                    label: 'MAC Address',
                    id: '',
                    selected: true,
                    patterns: [
                        {
                            name: 'MAC',
                            regex: /\b(?=.*[a-f])([a-f\d]{2}(?:[:-])?[a-f\d]{2}(?:[:-])?[a-f\d]{2}(?:[:-])?[a-f\d]{2}(?:[:-])?[a-f\d]{2}(?:[:-])?[a-f\d]{2}(?:[:-])?)\b/ig
                        }
                    ]
                },
                {
                    label: 'URLs',
                    id: '',
                    selected: true,
                    patterns: [
                        {
                            name: 'url',
                            regex: /(?:(?:http|https|ftp):\/\/|www\.)(?:\S+(?::\S*)?@)?(?:(?:(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[0-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))|localhost)(?::\d{2,5})?(?:(?:\/|\\?|#)[^\s\[\]"'><]*)?/ig
                        }
                    ]
                },
                {
                    label: 'TPNs',
                    id: '',
                    selected: true,
                    dominant: ['All Selectors', 'random'],
                    patterns: [
                        {
                            name: 'tpn',
                            regex: /((?:TIDE Number(?:\:)?|TPN(?:\:)?)|\(TPN\))(?:\s*)\d+/ig
                        }
                    ]
                },
                {
                    label: 'Objective Names',
                    id: '',
                    selected: true,
                    patterns: [
                        {
                            name: 'objnames',
                            regex: /\bOBJ(?:ECTIVE)?(?:\s+)?[A-Z]+?\b/g
                        }
                    ]
                },
                {
                    label: 'Reporting',
                    id: '',
                    selected: true,
                    patterns: [
                        {
                            // serial: 1/AA/123456-12 or 1/AA/12-12
                            name: 'n',
                            regex: /\b(?:[a-z]-)?(\d\/[a-z]{2}(?:\/[a-z0-9]{3})?\/\d{2,6}\-\d{2})\b/ig,
                            groups: ['Report'],
                            mainGroup: 'Report'
                        },
                        {
                            // serial: tdx-123/123123-12 or td-123/123123-12
                            name: 't',
                            regex: /\bserial\:(?:\s+)?(td(?:x)?\-\d{3}\/\d{6}\-\d{2})\b/ig,
                            groups: ['Report'],
                            mainGroup: 'Report'
                        },
                        {
                            // SUBJECT: (U) IIR 1 222 3333 44 or  SUBJECT: (U) IIR 1 222 3333 44
                            name: 'i',
                            regex: /\bsubject\:(?:\s+)?\((?:U|U\/\/FOUO)\)(?:\s+)?(iir(?:\s+)?\d{1}(?:\s+)?\d{3}(?:\s+)?\d{4}(?:\s+)?\d{2})\b/ig,
                            groups: ['Report'],
                            mainGroup: 'Report'
                        }
                    ]
                },
                {
                    label: 'Hash',
                    id: '',
                    selected: true,
                    children: [
                        {
                            label: 'MD5',
                            id: '',
                            selected: true,
                            patterns: [
                                {
                                    name: 'md5',
                                    regex: /\b[a-f0-9]{32}\b/ig
                                }
                            ]
                        },
                        {
                            label: 'SHA-1',
                            id: '',
                            selected: true,
                            patterns: [
                                {
                                    name: 'sha-1',
                                    regex: /\b[a-f0-9]{40}\b/ig
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            label: 'Social Media',
            id: '',
            selected: true,
            children: [
                {
                    label: 'User Names',
                    id: '',
                    selected: true,
                    patterns: [
                        // VIBER
                        /*
                         ?_Viber_account: /(?:viber account )([\w]+)/ig,
                         ?_Viber_VoiceID: /([\w]+)(?:<vibervoiceID>)/ig,

                         //VSAT DATA
                         ?_VSAT_MAC: /(?:Yahclick VSAT)(?:.)+(?:MAC address )([0-9a-fA-F]+)/ig,
                         ?_VSAT_MAC_2: /(?:VSAT)(?:\s)+([0-9a-fA-F]+)/ig,
                         ?_VSAT_IP:/(?:IP address)(?:[\sa-zA-Z])+([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})/ig,
                         //IP address 95.210.162.20

                         //YOUTUBE
                         //?_Youtube_Username: nayeem acm

                         // TWITTER
                         ?_Twitter_url:/(?:Twitter URL: )([\w:\/.]+)/ig,
                         ?_Twitter_User: /([\S]+)(?:<twitter>)/ig,
                         //TELEGRAM
                         ?_Telegram_account:/(?:Telegram account \(*)([\w\d]{5,30})\)ig,

                         //WHATSAPP
                         ?_WhatsApp_account:/(?:whatsapp account )([\d]{1,30})/ig,
                         ?_WhatsApp_number:/(?:whatsapp number: )([\d]{1,30})/ig,
                         ?_WhatsApp_id:/(?:whatsapp id )([\d]{1,30})/ig,
                         // SKYPE
                         ?_Skype_account: /(?:Skype account )+([^\n\r\s]{1,30})/ig,
                         ?_Skype_ID: /(?:Skype ID: )+([^\n\r\s]{1,30})/ig,
                         ?_Skype_User: /([\S]+)(?:<SkypeUser>)/ig,
                         ?_Skype_Hash: /([\w]+)(?:<SkypeHash>)/ig,

                         ?: /(\b(\w{5,16}\b))(?=((?!\w{5,16}).)*\s(skype|facebook|viber|twitter|youtube|paltalk|shmukh|tango|al-fida|sina|orkut|alplatformmedia|linkedin|pinger|afraid|changeip|noip|dyn|sitelutions|tagged|bayt|cash u|gurmad)\b)/ig
                         */
                        {
                            // ex: {username}, his|her|their {service} account|id|number
                            name: 'FBI_sentence_before',
                            regex: /\b(\w+)(?:[,; ()]+)?\b(?:his|her|their)\b(?:\s+)(\w+)(?:\s+)?\b(?:account|id|number)\b/ig,
                            groups: ['User Names', 'Service'],
                            mainGroup: 'User Names'
                        },
                        {
                            // ex: {service} account|account name|id|user|username|user name is|was {username}
                            name: 'FBI_sentence_after',
                            regex: /(\w+)(?:\s+)?(?:account(?:\sname)?|id|user(?:\s?name)?)(?:\s+)?(?:is|was)(?:\s+)?(\w+)/ig,
                            groups: ['Service', 'User Names'],
                            mainGroup: 'User Names'
                        },
                        {
                            // ex: {username}<{service}>
                            name: 'FBI_Carrot',
                            regex: /(\w+)(?:\s+)?<([\w.]+)>/ig,
                            groups: ['User Names', 'Service'],
                            mainGroup: 'User Names'
                        },
                        {
                            // ex: {service} account|id|number|link {username}
                            name: 'FBI_ID_colon',
                            regex: /([A-Za-z]+ ?)(?:\s+)?(?:account|id|number|link):(?:\s+)?([\w@.,:/]+)/ig,
                            groups: ['Service', 'User Names'],
                            mainGroup: 'User Names'
                        },
                        {
                            // ex: {service} account|id {username}
                            name: 'FBI_ID_nocolon',
                            regex: /\b([\w]+)(?:\s+)?(?:(?:id)(?:s?|entifications?)?(?:\s+)?(?:numbers?)?|account(?:\s+)?(?:name)?|user(?:\s+)?(?:name)?)(?:\s+)?((?:[0-9]{1,15}(?:[\s,]+)?)+)\b/ig,
                            groups: ['Service', 'User Names'],
                            mainGroup: 'User Names'
                        },
                        {
                            // ex: BlackBerry PIN|PIN number|PIN numbers: 12345678 98765432
                            name: 'FBI_ID_BBM',
                            regex: /(BlackBerry)\s+(?:PIN\s+(?:numbers?)?):?(?:\s)?(?:\(+)((?:\w{8}(?:\s)?)*)/ig,
                            groups: ['Service', 'User Names'],
                            mainGroup: 'User Names'
                        },
                        {
                            // ex: Viber account|accounts|id|ids abcd1234 9874fedc
                            name: 'viber',
                            regex: /(Viber)(?:\s+)?(?:accounts?|ids?)(?:\s+)?((?:[a-f0-9]{8}(?:\s)?)*)/ig,
                            groups: ['Service', 'User Names'],
                            mainGroup: 'User Names'
                        },
                        {
                            // ex: tango user id|ids 12345678 012345678912
                            name: 'tango',
                            regex: /\b(tango)(?:\s+)?(?:user\sid)(?:\s+)?(?=[\d]{8,12})([\d]{8,12})\b/ig,
                            groups: ['Service', 'User Names'],
                            mainGroup: 'User Names'
                        },
                        {
                            // ex: username|user name|userID|handle|screenname|screen name {username}
                            name: 'fbiOne',
                            regex: /\b(?:user(?:\s+)?name|userID|handle|screen(?:\s+)?name)(?:\s+)?[ï¿½'"]{0,2}(\w{5,15})\b/ig,
                            groups: ['User Names'],
                            mainGroup: 'User Names'
                        },
                        {
                            name: 'twitter',
                            regex: /(\b@(\w{1,15})(?!\.\w)\b)/ig
                        },
                        {
                            name: 'FBI_simple_ID_colon',
                            regex: /id\:(?:\s+)?(\d+)()/ig
                        }
                    ]
                },
                {
                    label: 'Hashtags',
                    id: '',
                    selected: true,
                    patterns: [
                        {
                            name: 'hashtag',
                            regex: /#[^\s\t#^,."'()]{5,}/ig
                        }
                    ]
                }
            ]
        },
        {
            label: 'Locations',
            id: '',
            selected: true,
            patterns: [
                {
                    //14Q FJ 1234567890, 4Q FJ 1234567890, 42Q FJ 1234567890
                    name: 'mgrs',
                    regex: /\b(((?:[0]?[\d]|[\d]{2})[A-Z])(?:\s+)?([A-Z]{2})(?:\s+)?((?:\d\d\d\d\d(?:\s)?\d\d\d\d\d)|(?:\d\d\d\d(?:\s)?\d\d\d\d)|(?:\d\d\d(?:\s)?\d\d\d)|(?:\d\d(?:\s)?\d\d)|(?:\d(?:\s)?\d)))\b/ig,
                            groups: ['Locations'],
                            mainGroup: 'Locations'
                },
                {
                    name: 'dd',
                    regex: /\d{1,3}\.\d{2,}(?:\xB0)?[NS]?[ \,]{1,2}[-]?\d{1,3}\.\d{2,}(?:\xB0)?[EW]?/ig
                },
                {
                    name: 'dms',
                    regex: /\d{1,2}(?:\xB0)(?:\s+)?\d{1,2}\'(?:\s+)?\d{1,2}(?:\.\d{1,})?\"[NS](?:\s+)\d{1,3}(?:\xB0)(?:\s+)?\d{1,2}\'(?:\s+)?\d{1,2}(?:\.\d{1,})?\"[EW]/ig
                },
                {
                    name: 'degree-decMinutes',
                    regex: /\d{1,2}(?:\xB0)(?:\s+)?\d{1,2}\.\d{1,}\'[NS](?:\s+)\d{1,3}(?:\xB0)(?:\s+)?\d{1,2}\.\d{1,}\'[EW]/ig
                },
                {
                    name: 'dms-fbi',
                    regex: /\b\d{5,7}\.\d{2}[NS]?[/\s]\d{5,7}\.\d{2}[EW]\b/ig
                },
                {
                    name: 'dms-?',
                    regex: /\b\d{4,9}[NS][\s]{0,2}\d{5,10}[EW]\b/ig
                }
            ]

        },
        {
            label: 'Towers',
            id: '',
            selected: true,
            patterns: [{
                        name: 'tower',
                        regex: /\b\d{3}[.-]\d{3}[.-]\d{4,5}[.-]\d{5}\b/gi
                    }]
        },
        {
            label: 'Selectors',
            id: '',
            selected: true,
            children: [{
                label: 'Normalized Selectors',
                id: '',
                selected: true,
                patterns: [
                    {
                        name: 'selector',
                        regex: /\b\d{9,16}\b/gi
                    }
                ],
                options: [{
                    name: 'Normalized Selectors',
                    label: 'Exclude US selectors',
                    regex: /\b(?!1)\d{9,16}\b/g,
                    defaults: true
                }],
            }, {
                label: 'Proton',
                id: '',
                selected: false,
                patterns: [
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{1}\-\d{4,6}\-\d{4,6}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{2}\-\d{8,10}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{2}\-\d{1}\-\d{8}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{2}\-\d{2}\-\d{6,9}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{2}\-\d{3}\-\d{5,8}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{2}\-\d{4}\-\d{4,7}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{2}\-\d{5}\-\d{4,5}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{3}\-\d{6,9}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{3}\-\d{1}\-\d{5,10}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{3}\-\d{2}\-\d{5,10}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{3}\-\d{3}\-\d{5,6}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{3}\-\d{5}\-\d{5,6}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{3}\-\d{5}\-\d{4}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{3}\-\d{1}\-\d{5,10}\b/ig
                    },
                    {
                        name: 'selector',
                        regex: /\b(?:imsi|imei@)?\d{14}\-\d{1}\-\d{5,10}\b/ig
                    }
                ]
            }]
        },
        {
            label: 'Custom',
            id: '',
            selected: false,
            children: [
                {
                    label: 'CUSTOM_REGEX_INPUT_DIRECTIVE'
                }
            ]
        }
    ]

};
