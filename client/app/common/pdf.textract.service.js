/* eslint-disable no-undef,no-loop-func */
import PDFJS from 'pdfjs-dist';

class PdfTextractService {
    constructor($q) {
        'ngInject';
        this.$q = $q;
    }

    getTextAsync(input, fileName, fileExtension) {
        let deferred = this.$q.defer();
        let docText = {};
        PDFJS.getDocument(input).then((pdfDocument) => {
            for (let pageNum = 1; pageNum <= pdfDocument.numPages; pageNum++) {
                pdfDocument.getPage(pageNum).then((page) => {
                    page.getTextContent().then((textContent) => {
                        let pageText = [];
                        textContent.items.forEach((textItem) => {
                            pageText.push(textItem.str);
                        });
                        pageText = pageText.join(' ');
                        docText[pageNum] = pageText;
                        if (Object.keys(docText).length === pdfDocument.numPages) {
                            // we are done
                            let docArr = [];
                            for (pageNum = 1; pageNum <= pdfDocument.numPages; pageNum++) {
                                docArr.push(docText[pageNum]);
                            }
                            let text = docArr.join('\n\n');
                            deferred.resolve({fileName: fileName, extension: fileExtension, content: text});
                        }
                    });
                });
            }
        }).catch((er)=>{
            console.log(er);
            deferred.reject(er)
        });

        return deferred.promise;
    }
}

export default PdfTextractService;
