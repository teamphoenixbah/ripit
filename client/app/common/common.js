import angular from 'angular';

import AppStateService from './app-state.service.js';
import DocTextractService from './doc-textract.service.js';
import HelpersService from './helpers.service.js';
import HTMLTextractService from './html.trextract.service.js';
import JemaLoggerService from './jema-logger.service.js';
import orderByKeyFilter from './orderByKey.filter.js';
import ParseProvider from './parse/parse.js';
import PdfTextractService from './pdf.textract.service.js';
import PptTextractService from './ppt.textract.service.js';
import ResultsExportService from './results.export.service.js';
import StorageService from './storage.service.js';
import TableService from './table.service.js';
import XlsTextractService from './xls.textract.service.js';

let commonModule = angular
    .module('app.common', [
        ParseProvider.name
    ])
    .filter('orderByKey', orderByKeyFilter)
    .service('AppStateService', AppStateService)
    .service('DocTextractService', DocTextractService)
    .service('HelpersService', HelpersService)
    .service('HTMLTextractService', HTMLTextractService)
    .service('JemaLoggerService', JemaLoggerService)
    .service('PdfTextractService', PdfTextractService)
    .service('PptTextractService', PptTextractService)
    .service('ResultsExportService', ResultsExportService)
    .service('StorageService', StorageService)
    .service('TableService', TableService)
    .service('XlsTextractService', XlsTextractService);

export default commonModule;
