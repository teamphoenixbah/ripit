class AppStateService {
    constructor($rootScope) {
        'ngInject';

        this.$rootScope = $rootScope;

        this.state = {};
        this.selectedDocs = new Set();
        this.initialize();

        // this.watchers();
    }

    setState(appState, value) {
        if (value === undefined) {
            throw Error('Error: You must set a value for this method.');
        }
        if (!this.state[appState]) {
            this.state[appState] = value;
        }
        this.state[appState] = value;
    }

    getState(appState) {
        return this.state[appState];
    }

    removeState(appState) {
        delete this.state[appState];
    }

    // needsRipping() {
    //     return this.state.needs;
    // }

    // watchers() {
        // this.$rootScope.$watch(() => {
        //     return this.state.needsRipping;
        // }, (newValue, oldValue) => {
        //     this.state.needsToBeRipped = true;
        // }, true);
    // }

    // hacks for testing
    initialize() {
        window.toggleState = (appState, value = !this.getState(appState)) => {
            this.setState(appState, value);
            console.log(this.getState(appState));
        };
        window.viewAppState = () => {
            console.log(this.state);
        };
        window.viewSelectedDocuments = () => {
            console.log(this.selectedDocs);
        };
    }
}

export default AppStateService;
