// import kmlConverter from './kml-converter/kml.converter.service.js';
import XLSX from 'xlsx/xlsx.js';
import filesaver from 'filesaver.js';

class ResultsExportService {
    constructor($q, RipItService, StorageService) {
        'ngInject';

        this.$q = $q;
        this.RipItService = RipItService;
        this.StorageService = StorageService;
        this.XLSX = XLSX;
    }

    toANB(data){
        //
    }

    toKML(data){
        // console.log(this.StorageService.activeDocuments);
    }

    toXLSX() {
        function Workbook() {
            if(!(this instanceof Workbook)) {
                return new Workbook();
            }
            this.SheetNames = [];
            this.Sheets = {};
        }

        let bins = this.RipItService.binsList;

        let binData = {};

        for(let i = 0, len = bins.length; i < len; i++){
            binData[bins[i].name] = this.RipItService.generateBinResultData(bins[i].name);
        }

        var workbook = new Workbook();

        for(let bin in binData){
            if (binData.hasOwnProperty(bin)) {
                if (binData[bin]) {
                    var worksheet = this.sheetFromArrayOfArrays(this.arrOobjectToArrArray(binData[bin]), null);
                }

                /* add worksheet to workbook */
                workbook.SheetNames.push(bin);
                workbook.Sheets[bin] = worksheet;
            }
        }

        var workbookOutput = XLSX.write(workbook, {bookType:'xlsx', bookSST:true, type: 'binary'});
        filesaver.saveAs(new Blob([this.s2ab(workbookOutput)], { type: "application/octet-stream" }), 'RipIt-' + Date.now().toString().substr(-6) + '.xlsx');
    }

    arrOobjectToArrArray(data) {
        let outerArr = [];
        if (data && data[0]) {
            let headers = Object.keys(data[0]);
            outerArr.push(headers);

            for (let i = 0, len = data.length; i < len; i++){
                let row = [];
                for (let d in data[i]) {
                    row.push(data[i][d]);
                }
                outerArr.push(row);
            }
        }
        return outerArr;
    }

    datenum(v, date1904) {
        if(date1904) {
            v +=1462;
        }
        var epoch = Date.parse(v);
        return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
    }

    s2ab(s) {
        let buf = new ArrayBuffer(s.length);
        let view = new Uint8Array(buf);
        for (let i=0; i!==s.length; ++i) {
            view[i] = s.charCodeAt(i) & 0xFF;
        }
        return buf;
    }

    sheetFromArrayOfArrays(data, opts) {
        var worksheet = {};
        var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
        for(var R = 0; R !== data.length; ++R) {
            for(var C = 0; C !== data[R].length; ++C) {
                if(range.s.r > R){
                    range.s.r = R;
                }
                if(range.s.c > C) {
                    range.s.c = C;
                }
                if(range.e.r < R) {
                    range.e.r = R;
                }
                if(range.e.c < C) {
                    range.e.c = C;
                }
                var cell = {v: data[R][C]};
                if(cell.v === null) {
                    continue;
                }
                var cell_ref = this.XLSX.utils.encode_cell({c:C,r:R});

                if(typeof cell.v === 'number') {
                    cell.t = 'n';
                }
                else if(typeof cell.v === 'boolean') {
                    cell.t = 'b';
                }
                else if(cell.v instanceof Date) {
                    cell.t = 'n'; cell.z = this.XLSX.SSF._table[14];
                    cell.v = this.datenum(cell.v);
                }
                else {
                    cell.t = 's';
                }
                worksheet[cell_ref] = cell;
            }
        }
        if (range.s.c < 10000000) {
            worksheet['!ref'] = this.XLSX.utils.encode_range(range);
        }
        return worksheet;
    }
}

export default ResultsExportService;
