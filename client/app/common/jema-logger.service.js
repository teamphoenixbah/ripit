import _ from 'lodash';

class JemaLoggerService {
	constructor($q, $timeout){
		'ngInject';

		this._ = _;
		this.$q = $q;
		this.$timeout = $timeout;
	}

    // this is not angular-ish, and I don't like
    log(url) {
        let iframe = document.getElementById('log-iframe');

        if (iframe) {
            document.body.removeChild(iframe);
        }

        let newIframe = document.createElement('iframe');
        newIframe.id = 'log-iframe';
        // iframe.style.display = "none";
        newIframe.src = url;
        document.body.appendChild(newIframe);
    }

}

export default JemaLoggerService;