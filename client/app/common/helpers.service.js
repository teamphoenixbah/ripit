import _ from 'lodash';

class HelpersService {
	constructor($q, $timeout){
		'ngInject';

		this._ = _;
		this.$q = $q;
		this.$timeout = $timeout;
	}

	static csvToArray(csv) {
		var deferred = this.$q.defer();
		var rows = csv.split("\n");
		var data = [];

		var regex = new RegExp('(?:"([^"]*(?:""[^"]*)*)"|([^",\r\n]+)|,(?=,))', 'g');
		var matches = false;
		var tmp = [];

		angular.forEach(rows, function (row) {
			matches = false;
			tmp.length = 0;
			while (matches = regex.exec(row)) {
				tmp.push(matches[1] || matches[2] || '');
			}
			if (row.substr(row.length - 1, 1) === ','){
				tmp.push('');
			}

			data.push(tmp);
		});

		deferred.resolve(data);

		return deferred.promise;
	}

	size(data) {
		return this._.size(data);
	}

	toArray(obj) {
		return this._.toArray(obj);
	}

	safeApply(fn) {
        this.$timeout(fn);
    }

	objectsAreSame(obj1, obj2, propertiesToCheck) {
        let objectsAreSame = true;
        for (let i = 0, len = propertiesToCheck.length; i < len; i++) {
            if (obj1[propertiesToCheck[i]] !== obj2[propertiesToCheck[i]]) {
                objectsAreSame = false;
                break;
            }
        }
        return objectsAreSame;
    }

	emptyObject(obj){
        for(let key in obj){
            if(obj.hasOwnProperty(key)){
                delete obj[key];
            }
        }
    }
}

export default HelpersService;