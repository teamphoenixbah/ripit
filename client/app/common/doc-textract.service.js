import JSZip from 'jszip';

class DocTextractService {
    constructor($q) {
        'ngInject';

        this.$q = $q
    }
    getTextAsync(input, fileName, fileExtension) {
        let deferred = this.$q.defer();

        if (fileExtension === 'docx') {
            // docx parsing
            let newZip = new JSZip();
            newZip.load(input);
            let strippedText = newZip.file('word/document.xml').asText()
                .replace(/<w:p[\s\S]+?>/g, 'This_Is_The_Delimeter')
                .replace(/<[^\>]*\>/g, ' ')
                .replace(/\s+/g, ' ')
                .replace(/This_Is_The_Delimeter(?:This_Is_The_Delimeter+)?/g, '\n\n').trim();

            deferred.resolve({fileName: fileName, extension: fileExtension,  content: strippedText});
        } else if (fileExtension === 'doc') {
            // not yet supported
            //return '';
        }

        return deferred.promise;
    }

    getText(input, fileExtension) {
        if (fileExtension === 'docx') {
            // docx parsing
            let newZip = new JSZip();
            newZip.load(input);
            let strippedText = newZip.file('word/document.xml').asText()
                .replace(/<w:p[\s\S]+?>/g, 'This_Is_The_Delimeter')
                .replace(/<[^\>]*\>/g, ' ')
                .replace(/\s+/g, ' ')
                .replace(/This_Is_The_Delimeter(?:This_Is_The_Delimeter+)?/g, '\n\n').trim();
            return strippedText;
        } else if (fileExtension === 'doc') {
           // not yet supported
            return '';
        }
    }
}

export default DocTextractService;
