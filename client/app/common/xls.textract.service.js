import XLSX from 'xlsx/xlsx.js';

class XlsTextractService {
    constructor($q) {
        'ngInject';

        this.$q = $q;
    }

    getTextAsync(input, fileName, fileExtension) {
        let deferred = this.$q.defer();

        let unstructuredResult = [];
        if (fileExtension === 'xlsx' || fileExtension === 'xls') {
            /*    xlsx parsing               */
            let workbook = XLSX.read(input, {type: 'binary'});

            let sheetNameList = workbook.SheetNames;
            sheetNameList.forEach((y) => { /* iterate through sheets */
                let worksheet = workbook.Sheets[y];
                let csv = XLSX.utils.sheet_to_csv(worksheet);

                unstructuredResult.push(csv);
            });
        }

        unstructuredResult = unstructuredResult.join('\n\n');

        deferred.resolve({fileName: fileName, extension: fileExtension,  content: unstructuredResult});

        return deferred.promise;
    }
}

export default XlsTextractService;
