class TableService {
    constructor() {
        'ngInject';

        this.initialize();
        this.watchers();
        this.listeners();
    }

    initialize() {
        let columnDefs = [];
        this.defaultGridOptions = {
            columnDefs,
            rowData: [],
            enableFilter: true,
            enableSorting: true,
            enableColResize: true
        };
    }

    listeners(){
        //
    }

    watchers() {
        //
    }

    generateTableHeaders(data, sortObj) {
        let header = [];
        if (data && data[0]) {
            for (let d in data[0]) {
                if (data[0].hasOwnProperty(d)) {
                    let row = {
                        headerName: d,
                        field: d
                    };

                    if (sortObj && sortObj.colName === d) {
                        row.sortingOrder = ['asc', 'desc', null];
                    }

                    header.push(row);
                }
            }
        }
        return header;
    }
}

export default TableService;
