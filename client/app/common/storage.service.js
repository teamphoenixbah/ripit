import ripitSampleDocument from './../data/ripit-sample-document.txt';

class StorageService {
    constructor($rootScope, AppStateService, HelpersService) {
        'ngInject';

        this.$rootScope = $rootScope;
        this.AppStateService = AppStateService;
        this.HelpersService = HelpersService;

        this.documents = {};
        this.activeDocuments = {};

        this.count = {
            activeDocument: 0,
            totalDocument: 0
        };

        this.sampleDocument = {
            'Sample Document': {
                name: 'Sample Document',
                displayName: 'Sample Document',
                content: ripitSampleDocument,
                needsParsing: true,
                uploaded: false,
                valid: true,
                selected: false
            }
        };

        this.initialize();
        this.watchers();
        this.listeners();
    }

    getActiveDocuments() {
        this.HelpersService.emptyObject(this.activeDocuments);
        if (this.AppStateService.selectedDocs.size) {
            this.AppStateService.selectedDocs.forEach((docName) => {
                this.activeDocuments[docName] = this.documents[docName];
            });
        } else {
            // show all tabs as selected when none are selected
            for(let docName in this.documents) {
                if (this.documents.hasOwnProperty(docName)) {
                    if(this.documents[docName].valid){
                        this.activeDocuments[docName] = this.documents[docName];
                    }
                }
            }
        }
        return this.activeDocuments;
    }

    setData(documents) {
        this.data = documents;
    }

    getData() {
        return this.documents;
    }

    listeners(){
        this.$rootScope.$on('files-changed', () => {
            this.activeDocuments = this.getActiveDocuments();
            // this.AppStateService.setState('needsRipping', true);
        });
    }

    watchers() {

    }

    // hacks for testing
    initialize() {
        window.activeDocuments = () => {
            console.log(this.getActiveDocuments());
        };
        window.viewDocuments = () => {
            console.log(this.documents);
            return this.documents;
        };
    }
}

export default StorageService;
