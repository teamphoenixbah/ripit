export default function orderByKey($filter) {
    'ngInject';

    return function(items, field, reverse) {
        var keys = $filter('orderBy')(Object.keys(items), field, reverse),
            obj = {};
        keys.forEach(function(key) {
            obj[key] = items[key];
        });
        return obj;
    };
}
