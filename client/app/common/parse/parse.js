import angular from 'angular';
import ParseProvider from './parse.provider.js';

let parseModule = angular.module('ParseProvider', [])
    .provider('ParseProvider', ParseProvider)
    .config((ParseProviderProvider, APP_SETTINGS) => {
        'ngInject';

        ParseProviderProvider.setConfig(APP_SETTINGS);
    });
export default parseModule;
