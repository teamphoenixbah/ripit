/* eslint-disable object-shorthand, no-loop-func */
import angular from 'angular';

class ParseProvider {
    constructor() {
        'ngInject';
    }

    $get() {
        let config = this.config;
        let regexSet = [];
        return {
            setRegexsUsed: function() {
                if (regexSet.length > 0) {
                    regexSet = [];
                }
                this.iterate(config.regexPatterns);
            },
            iterate: function(nodeItem) {
                angular.forEach(nodeItem, (node) => {
                    if (node.selected) {
                        if (node.children) {
                            this.iterate(node.children);
                        } else {
                            regexSet.push(node);
                        }
                    }
                });
            },
            getRegexsUsed() {
                return regexSet;
            },
            ripIt(document, docName = document.displayName) {
                let resultObject = {};
                let regexes = this.getRegexsUsed();
                regexes.forEach((selection) => {
                    let label;
                    if (selection.bin){
                        label = selection.bin;

                    } else {
                        label = selection.label;
                    }

                    if (!resultObject[label]) {
                        resultObject[label] = [];
                    }
                    let output = [];

                    if (selection.options) {
                        angular.forEach(selection.options, (option) => {
                            let mainGroup = option.mainGroup ? option.mainGroup : selection.label;
                            if (option.defaults) {
                                output = output.concat(this.extract(document, option, selection));
                                if (config.normalizer[selection.label]) {
                                    angular.forEach(config.normalizer, (value, key) => {
                                        if (key.toLowerCase() === selection.label.toLowerCase()) {
                                            this.normalizeResults(output, value, key);
                                        }
                                    });
                                }
                            } else {
                                angular.forEach(selection.patterns, (pattern) => {
                                    let mainGroup = pattern.mainGroup ? pattern.mainGroup : selection.label;
                                    output = output.concat(this.extract(document, pattern, selection));
                                    var test;
                                });
                                angular.forEach(config.normalizer, (value, key) => {
                                    if (key.toLowerCase() === selection.label.toLowerCase()) {
                                        this.normalizeResults(output, value, key);
                                    }
                                });
                            }
                        });
                    } else {
                        angular.forEach(selection.patterns, (pattern) => {
                            let mainGroup = pattern.mainGroup ? pattern.mainGroup : selection.label;
                            output = output.concat(this.extract(document, pattern, selection));
                            var test;
                        });
                        angular.forEach(config.normalizer, (value, key) => {
                            if (key.toLowerCase() === selection.label.toLowerCase()) {
                                this.normalizeResults(output, value, key);
                            }
                        });
                    }

                    resultObject[label] = resultObject[label].concat(output);
                });
                resultObject = this.enforceDominance(resultObject);
                return resultObject;
            },
            // extract(pattern, input, labelTag, groups, docName, mainGroup) {
            extract(documentObj, optionsObj, binOptionsObj) {
                let output = [];
                let match;
                let matchObj = {};

                let mainGroup = optionsObj.mainGroup ? optionsObj.mainGroup : binOptionsObj.label;
                let label;
                if (binOptionsObj.bin){
                    label = binOptionsObj.bin;
                    mainGroup = binOptionsObj.bin;
                } else {
                    label = binOptionsObj.label;
                }

                if (optionsObj.regex.global) {
                    let indexes = new Set();
                    while ((match = optionsObj.regex.exec(documentObj.content))) {
                        if (!matchObj[match[0]]) {
                            matchObj[match[0]] = {};
                            matchObj[match[0]].docName = documentObj.displayName;
                            matchObj[match[0]].label = label;
                            matchObj[match[0]].indexes = new Set();
                            matchObj[match[0]].indexes.add(match.index);
                            matchObj[match[0]].hitLength = match[0].length;
                            matchObj[match[0]].mainGroup = mainGroup;

                            matchObj[match[0]].count = matchObj[match[0]].indexes.size;
                            if (optionsObj.groups) {
                                for (let i = 0, len = optionsObj.groups.length; i < len; i++) {
                                    if (match[i + 1]) {
                                        matchObj[match[0]][optionsObj.groups[i]] = match[i + 1].trim();
                                    } else {
                                        console.log('ERROR: Regex group mismatch with results.');
                                    }
                                }
                            } else {
                                matchObj[match[0]][label] = match[0].trim();
                            }
                        } else {
                            matchObj[match[0]].indexes.add(match.index);
                            matchObj[match[0]].count = matchObj[match[0]].indexes.size;
                        }
                    }
                } else {
                    match = documentObj.content.match(pattern);
                    if (!matchObj[match[0]]) {
                        matchObj[match[0]] = {};
                        matchObj[match[0]].indexes = new Set();
                        matchObj[match[0]].indexes.add(match.index);
                        matchObj[match[0]].docName = documentObj.displayName;
                        matchObj[match[0]].label = label;
                        matchObj[match[0]].indexes = [match.index];
                        matchObj[match[0]].hitLength = match[0].length;
                        matchObj[match[0]].mainGroup = mainGroup;

                        matchObj[match[0]].count = matchObj[match[0]].indexes.size;
                        if (binOptionsObj.label) {
                            for (let i = 0, len = optionsObj.groups.length; i < len; i++) {
                                if (match[i]) {
                                    matchObj[match[0]][optionsObj.groups[i]] = match[i + 1].trim();
                                } else {
                                    console.log('ERROR: Regex group mismatch with results.');
                                }
                            }
                        //} else if(){
                        } else {
                            matchObj[match[0]][label] = match[0].trim();
                        }
                    } else {
                        matchObj[match[0]].indexes.add(match.index);
                        matchObj[match[0]].count = matchObj[match[0]].indexes.size;
                    }
                }

                angular.forEach(matchObj, (obj) => {
                    output.push(obj);
                });

                return output;
            },
            enforceDominance(docResult) {
                // for each category, check for dominance
                angular.forEach(this.getRegexsUsed(), (selection) => {
                    if (selection.dominant) {
                        // if dominant globally
                        if (selection.dominant === true) {
                            // for each result in dominant category
                            angular.forEach(docResult[selection.label], (dResult) => {
                                // for each result in full results
                                let dominate = true;
                                angular.forEach(docResult, (lResult) => {
                                    // skip self
                                    if (lResult.length > 0 && !lResult[0][selection.label]) {
                                        // reverse loop through result category
                                        for (let i = lResult.length; i > 0; i--) {
                                            // loop through results indexes
                                            angular.forEach(lResult[i - 1].indexes, (index) => {
                                                // loop through dominant results indexes
                                                angular.forEach(dResult.indexes, (dIndex) => {
                                                    // if result index falls in dominant index
                                                    if (index >= dIndex && index <= dIndex + dResult.hitLength) {
                                                        // only allow one splice per result
                                                        if (dominate) {
                                                            // remove result index
                                                            lResult.splice(i - 1, 1);
                                                        }
                                                        dominate = false;
                                                    }
                                                });
                                            });
                                        }
                                    }
                                    dominate = true;
                                });
                            });
                            // if selected dominance
                        } else if (Array.isArray(selection.dominant)) {
                            // for each result in dominant category
                            angular.forEach(docResult[selection.label], (dResult) => {
                                let dominate = true;
                                angular.forEach(selection.dominant, (dominantSelected) => {
                                    let sub = docResult[dominantSelected];
                                    if (sub) {
                                        for (let i = sub.length; i > 0; i--) {
                                            // loop through results indexes
                                            angular.forEach(sub[i - 1].indexes, (index) => {
                                                // loop through dominant results indexes
                                                angular.forEach(dResult.indexes, (dIndex) => {
                                                    // if result index falls in dominant index
                                                    if (index >= dIndex && index <= dIndex + dResult.hitLength) {
                                                        // only allow one splice per result
                                                        if (dominate) {
                                                            // remove result index
                                                            sub.splice(i - 1, 1);
                                                        }
                                                        dominate = false;
                                                    }
                                                });
                                            });
                                        }
                                    }
                                    dominate = true;
                                });
                            });
                        }
                    }
                });
                return docResult;
            },
            normalizeResults(unNormalized, normalizePatterns, valName) {
                // for each category hit
                angular.forEach(unNormalized, (uNameHit) => {
                    // for each normalizer pattern
                    angular.forEach(normalizePatterns, (normRegex, finalFormat) => {
                        let builtRegexSet = new Set();
                        // build regex
                        angular.forEach(normRegex, (normRegexMember) => {
                            builtRegexSet.add(`\\b${normRegexMember}\\b`);
                        });

                        // remove ending pipe |
                        let builtRegex = Array.from(builtRegexSet).join('|');
                        // create regex
                        let normPattern = new RegExp(builtRegex, 'ig');
                        // grab value
                        let value = uNameHit[valName];
                        // will replace if matches
                        uNameHit[valName] = value.replace(normPattern, finalFormat);
                    });
                });
                return unNormalized;
            }
        };
    }

    setConfig(config) {
        this.config = config;
    }
}


export default ParseProvider;
