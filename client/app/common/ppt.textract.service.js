import JSZip from 'jszip';

class PptTextractService {
    constructor($q) {
        'ngInject';

        this.$q = $q;
    }

    getText(input, fileExtension) {
        if (fileExtension === 'pptx') {
            // pptx ripping
            let newZip = new JSZip();
            newZip.load(input);

            let strippedText = [];
            for (let key in newZip.files) {
                if (key.indexOf('ppt/slides/slide') !== -1) {
                    strippedText.push(newZip.file(key).asText().replace(/<[^\>]*\>/g, ' '));
                }
            }
            strippedText = strippedText.join('\n\n');
            return strippedText.replace(/\s+/g, ' ');
        } else if (fileExtension === 'ppt') {
            // not yet supported
            return '';
        }
    }

    getTextAsync(input, fileName, fileExtension) {
        let deferred = this.$q.defer();

        if (fileExtension === 'pptx') {
            // pptx ripping
            let newZip = new JSZip();
            newZip.load(input);

            let strippedText = [];
            for (let key in newZip.files) {
                if (key.indexOf('ppt/slides/slide') !== -1) {
                    strippedText.push(newZip.file(key).asText().replace(/<[^\>]*\>/g, ' '));
                }
            }
            strippedText = strippedText.join('\n\n');
            strippedText = strippedText.replace(/\s+/g, ' ');

            deferred.resolve({fileName: fileName, extension: fileExtension, content: strippedText});
        } else if (fileExtension === 'ppt') {
            // not yet supported
            // return '';
        }

        return deferred.promise;
    }
}

export default PptTextractService;
