import html2Text from './html2Text.js';

class HTMLTextractService {
    constructor($q) {
        'ngInject';
        this.$q = $q;
    }

    getTextAsync(input, fileName = '', fileExtension = '') {
        let deferred = this.$q.defer();
        let docText = {};

        let text = this.getText(input);

        deferred.resolve({fileName: fileName, extension: fileExtension, content: text});

        return deferred.promise;
    }

    getText(input, fileName = '', fileExtension = '') {
        return html2Text(input);
    }
}

export default HTMLTextractService;




