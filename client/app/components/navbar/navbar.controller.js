class NavbarController {
    constructor(APP_SETTINGS) {
        'ngInject';

        this.email = APP_SETTINGS.feedbackEmail;
    }
}

export default NavbarController;
