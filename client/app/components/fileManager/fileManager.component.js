import template from './fileManager.html';
import controller from './fileManager.controller.js';
import './fileManager.scss';

let fileManagerComponent = function () {
    return {
        template,
        controller,
        restrict: 'E',
        controllerAs: 'vm',
        scope: {},
        bindToController: true
    };
};

export default fileManagerComponent;
