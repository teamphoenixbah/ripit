import angular from 'angular';

class FileManagerController {
    constructor($mdDialog, $rootScope, $scope, AppStateService, FileManagerService, HelpersService, JemaLoggerService, RipItService, ParseProvider, StorageService) {
        'ngInject';

        this.$mdDialog = $mdDialog;
        this.$rootScope = $rootScope;
        this.$scope = $scope;
        this.AppStateService = AppStateService;
        this.FileManagerService = FileManagerService;
        this.HelpersService = HelpersService;
        this.JemaLoggerService = JemaLoggerService;
        this.ParseProvider = ParseProvider;
        this.RipItService = RipItService;
        this.StorageService = StorageService;

        this.needsRipping = this.RipItService.needsRipping;

        this.count = {
            activeDocument: 0,
            totalDocument: 0
        };

        this.contextMenuOptions = [
            {
                displayName: 'Edit',
                action: (name, $event) => this.clickedEdit(name, $event)
            }, {
                displayName: 'Delete',
                action: (name, $event) => this.clickedDelete(name, $event)
            }
        ];

        this.listeners();

        this.watchers();
    }

    ripIt(evt) {
        this.ParseProvider.setRegexsUsed(); // TODO: we should find a better way to do this
        if (Object.keys(this.StorageService.documents).length === 0) {
            this.showLoadSampleConfirm(evt);
        } else if (this.AppStateService.getState('editMode')){
            this.showCannotRip(evt);
        } else {
            this.RipItService.parse();
        }
    }

    clickedDelete(toDelete, evt) {
        if (this.AppStateService.getState('editMode')) {
            // to not allow delete if in edit mode
            this.deleteAlert(evt);
        } else {
            return this.FileManagerService.deleteDocument(toDelete);
        }
    }

    clickedEdit(toEdit, evt) {
        if (this.AppStateService.getState('editMode')) {
            // do not allow edit if already in edit mode
            this.editAlert(evt);
        } else {
            return this.FileManagerService.editDocument(toEdit);
        }
    }

    disableContextMenuItem(item) {
        let isDisabled = false;
        if (this.AppStateService.getState('editMode')) {
            isDisabled = true;
        } else if (item.uploaded) {
            isDisabled = true;
        }

        return isDisabled;
    }

    deleteAlert(ev) {
        this.$mdDialog.show(
            this.$mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Cannot Delete')
                .textContent('You can\'t delete a document in Edit Mode.')
                .ariaLabel('Cannot Delete')
                .ok('Got it!')
                .targetEvent(ev)
        );
    }

    editAlert(ev) {
        this.$mdDialog.show(
            this.$mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Cannot Edit')
                .textContent('Please \'Cancel\' or \'Save\' current document.')
                .ariaLabel('Already editing')
                .ok('Got it!')
                .targetEvent(ev)
        );
    }

    showFileManagerInfo(evt) {
        this.$mdDialog.show({
            controller: function($scope, FileManagerService) {
                $scope.supportedFileTypes = FileManagerService.supportedFileTypes;
                console.log($scope.supportedFileTypes)
            },
            template: `
                <md-dialog aria-label="Mango (Fruit)" style="width:500px;" ng-cloak>
                    <md-toolbar>
                        <div class="md-toolbar-tools">
                            <h2>File Upload</h2>
                        </div>
                    </md-toolbar>
                    <md-dialog-content>
                        <div class="md-dialog-content">
                            <p class="info-text">
                            The RipIt supports the following file upload formats:
                            </p>
                            <ul style="list-style:disc">
                                <li ng-repeat="type in supportedFileTypes">{{type}}</li>
                            </ul>
                            <p class="info-text">
                            If you need to rip text from a file that is not currently supported, you can use the Paste Text option in RipIt and copy and paste your text from the unsupported document. After clicking Save, RipIt will be able to treat the document as if it had been uploaded.
                            </p>
                        </div>
                    </md-dialog-content>
                </md-dialog>
            `,
            parent: angular.element(document.body),
            targetEvent: evt,
            clickOutsideToClose:true,
            fullscreen: false
        })
    }

    toggleSelected(item) {
        return this.FileManagerService.toggleSelected(item);
    }

    uploadFiles(files, errFiles) {
        return this.FileManagerService.uploadFilesAsync(files, errFiles);
    }

    pasteText() {
        return this.FileManagerService.pasteText();
    }

    // dynamic styling
    determineStyle(item) {
        let styleObj = {};

        if (item.selected) {
            styleObj['background-color'] = 'rgba(0, 0, 0, 0.2)';
        }
        if (!item.content || item.content === '') {
            styleObj.opacity = 0.4;
        }

        return styleObj;
    }

    setIcon(item) {
        let icon = '';
        if (!item.valid) {
            icon = 'document-error';
        } else if (item.uploaded) {
            icon = `document-${item.extension}`;
        } else {
            icon = 'document';
        }
        return icon;
    }

    showLoadSampleConfirm(evt) {
        let confirm = this.$mdDialog.confirm()
            .title('No documents to Rip')
            .textContent('Would you like to load the RipIt sample document?')
            .ariaLabel('Question modal')
            .targetEvent(evt)
            .ok('Yes, please')
            .cancel('Oops, nevermind');
        this.$mdDialog.show(confirm).then(() => {
            this.StorageService.documents = angular.copy(this.StorageService.sampleDocument);
            this.$rootScope.$broadcast('files-changed');
            this.RipItService.parse();
        });
    }

    showCannotRip(evt) {
        this.$mdDialog.show(
        this.$mdDialog.alert()
            // .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title('Edit in Progress')
            .textContent('You must Save your document or Cancel.')
            .ariaLabel('Alert Dialog')
            .ok('Ok')
            .targetEvent(evt)
        );
    }

    listeners() {
        this.$scope.$on('files-changed', () => {
            // TODO: this needs to move to StorageService
            this.count.activeDocument = Object.keys(this.StorageService.activeDocuments).length
            this.count.totalDocument = Object.keys(this.StorageService.documents).length
            this.HelpersService.safeApply();
        });
    }

    watchers() {
        //
    }
}

export default FileManagerController;
