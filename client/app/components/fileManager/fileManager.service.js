/* eslint-disable no-param-reassign, no-unneeded-ternary */
import angular from 'angular';

const SUPPORTED_FILE_TYPES = ['csv', 'docx', 'html', 'kml', 'pdf', 'pptx', 'txt', 'xlsx', 'xml']; //'xls',
const SUPPORTED_TEXT_TYPES = ['txt', 'xml', 'kml', 'html', 'csv'];

class FileManagerService {
    constructor($q, $rootScope, AppStateService, DocTextractService, PdfTextractService, PptTextractService, StorageService, XlsTextractService) {
        'ngInject';

        this.$q = $q;
        this.$rootScope = $rootScope;
        this.AppStateService = AppStateService;
        this.DocTextractService = DocTextractService;
        this.PdfTextractService = PdfTextractService;
        this.PptTextractService = PptTextractService;
        this.StorageService = StorageService;
        this.XlsTextractService = XlsTextractService;
        this.accept = '*';
        this.supportedFileTypes = SUPPORTED_FILE_TYPES;
        this.supportedTextTypes = SUPPORTED_TEXT_TYPES;

        // this.needsRipping = this.AppStateService.getState('needsRipping');
    }

    selectDocument(document) {
        this.AppStateService.selectedDocs.add(document.displayName);
        document.selected = true;

        this.$rootScope.$broadcast('files-changed');
    }

    deselectDocument(document) {
        this.AppStateService.selectedDocs.delete(document.displayName);
        document.selected = false;

        this.$rootScope.$broadcast('files-changed');
    }

    toggleSelected(document) {
        // only allow selection changes if not in edit mode
        if (!this.AppStateService.getState('editMode')) {
            if (document.valid) {
                if (!document.selected) {
                    this.selectDocument(document);
                } else {
                    this.deselectDocument(document);
                }

                this.$rootScope.$broadcast('files-changed');
            }
        }
    }

    pasteText() {
        // first, de-selected all
        for (let docName in this.StorageService.activeDocuments) {
            if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
                this.StorageService.activeDocuments[docName].selected = false;
            }
        }

        let randomDocName = 'PastedDoc1';
        while (this.StorageService.documents[randomDocName]) {
            let pattern = /\d+$/g;
            if (randomDocName.match(pattern)) {
                let docNumber = parseInt(randomDocName.match(pattern)[0], 10);
                docNumber++;
                randomDocName = randomDocName.replace(pattern, docNumber);
            }
        }

        this.StorageService.documents[randomDocName] = {
            displayName: randomDocName,
            name: randomDocName.replace(/[.]/g, ''),
            content: '',
            needsParsing: true,
            uploaded: false,
            valid: true,
            selected: false
        };

        this.AppStateService.setState('editMode', true);
        this.AppStateService.setState('documentToEdit', randomDocName);
        this.$rootScope.$broadcast('files-changed');
    }

    deleteDocument(toDelete) {
        this.deselectDocument(this.StorageService.documents[toDelete])
        delete this.StorageService.documents[toDelete];
        this.$rootScope.$broadcast('files-changed');
    }

    editDocument(toEdit) {
        this.selectDocument(this.StorageService.documents[toEdit]);

        this.AppStateService.setState('documentToEdit', toEdit);
        let content = angular.copy(this.StorageService.documents[toEdit].content);
        this.AppStateService.setState('editMode', true);
        this.AppStateService.setState('editBackup', content);

        this.$rootScope.$broadcast('edit-document', {
            content
        });
    }

    uploadFilesAsync(files) {
        let defered = this.$q.defer();
        let promises = [];

        let done = false;
        angular.forEach(files, (file, index) => {
            if (index === files.length - 1) {
                done = true;
            }

            promises.push(this.uploadSingleFileAsync(file));
        });

        if (done) {
            this.$q.all(promises).then(() => {
                this.$rootScope.$broadcast('files-changed');
            }).catch((e) => {
                // console.error(e);
                defered.reject(e);
            });
        }
        return defered.promise;
    }

    uploadSingleFileAsync(file) {
        let defered = this.$q.defer();
        let promises = [];

        let reader = new FileReader();
        let extension = file.name.substr(file.name.lastIndexOf('.') + 1);
        if (this.supportedFileTypes.indexOf(extension) >= 0) {
            // IE doesnt support read as binary string
            // reader.readAsBinaryString(file);
            // reader.readAsArrayBuffer(file);
            if (this.supportedTextTypes.includes(extension)) {
                reader.readAsText(file);
            } else {
                reader.readAsArrayBuffer(file);
            }

            reader.onload = (onLoadEvent) => {
                if (this.supportedTextTypes.indexOf(extension) >= 0) {
                    promises.push(this.$q.when({
                        fileName: file.name,
                        extension,
                        content: onLoadEvent.target.result
                    }));
                } else if (extension === 'doc' || extension === 'docx') {
                    promises.push(this.DocTextractService.getTextAsync(onLoadEvent.target.result, file.name, extension));
                } else if (extension === 'ppt' || extension === 'pptx') {
                    promises.push(this.PptTextractService.getTextAsync(onLoadEvent.target.result, file.name, extension));
                } else if (extension === 'xls' || extension === 'xlsx') {
                    let bstr = this.processExcelFiles(onLoadEvent.target.result);
                    promises.push(this.XlsTextractService.getTextAsync(bstr, file.name, extension));
                } else if (extension === 'pdf') {
                    promises.push(this.PdfTextractService.getTextAsync(onLoadEvent.target.result, file.name, extension));
                }
                this.processFiles(promises).then(() => {
                    defered.resolve(this.StorageService.documents);
                }).catch((e) => {
                    // console.error(e)
                    defered.reject(e);
                });
            };
        } else {
            // add a blank, invalid file (placeholder)
            let invalidFile = {fileName: file.name, extension, content: '', valid: false};
            promises.push(this.$q.when(invalidFile));

            this.processFiles(promises).then(() => {
                defered.resolve(this.StorageService.documents);
            }).catch((e) => {
                // console.error(e)
                defered.reject(e);
            });
        }

        return defered.promise;
    }

    processFiles(promises) {
        let deferred = this.$q.defer();
        this.$q.all(promises).then((results) => {
            results.forEach((r) => {
                this.StorageService.documents[r.fileName] = {
                    name: r.fileName.replace(/[\.]/g, ''),
                    displayName: r.fileName,
                    content: r.content,
                    needsParsing: true,
                    uploaded: true,
                    valid: r.valid === false ? false : true,
                    extension: r.extension,
                    selected: false
                };
            });
            deferred.resolve(true);
        }).catch((e) => {
            // console.error(e);
            deferred.reject(e);
        });

        return deferred.promise;
    }

    processExcelFiles(result) {
        /* convert ArrayBuffer to binary string */
        let data = new Uint8Array(result);
        let arr = [];
        for (let i = 0; i !== data.length; ++i) {
            arr[i] = String.fromCharCode(data[i]);
        }
        return arr.join('');
    }
}

export default FileManagerService;
