import angular from 'angular';
import fileManagerComponent from './fileManager.component';
import 'ng-file-upload';
import 'angular-material-icons';
import 'simple-context-menu';
import 'simple-context-menu/context-menu.css';

import fileManagerService from './fileManager.service.js';

let fileManagerModule = angular.module('fileManager', [
    'ngFileUpload',
    'ngMdIcons',
    'simple-context-menu'
])
.directive('fileManager', fileManagerComponent)
    .service('FileManagerService', fileManagerService);

export default fileManagerModule;
