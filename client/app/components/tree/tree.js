import angular from 'angular';
import 'angular-ui-router';
import treeComponent from './tree.component';
import 'angular-ivh-treeview/dist/ivh-treeview.js';

let treeModule = angular.module('tree', [
    'ui.router',
    'ivh.treeview'
])
    .directive('tree', treeComponent)
    .config((ivhTreeviewOptionsProvider) => {
        'ngInject';
        ivhTreeviewOptionsProvider.set({
            idAttribute: 'id',
            labelAttribute: 'label',
            childrenAttribute: 'children',
            selectedAttribute: 'selected',
            useCheckboxes: true,
            expandToDepth: 0,
            indeterminateAttribute: '__ivhTreeviewIndeterminate',
            defaultSelectedState: true,
            validate: true,
            twistieExpandedTpl: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28" width="28" height="28" style="margin-bottom:-10px;"><path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"></path></svg>',
            twistieCollapsedTpl: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28" width="28" height="28" style="margin-bottom:-10px;"><path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"></path></path></svg>',
            twistieLeafTpl: '',
            // nodeTpl: ['<div class="ivh-treeview-node-content" title="{{trvw.label(node)}}">', '<span ivh-treeview-toggle>', '<span class="ivh-treeview-twistie-wrapper" ivh-treeview-twistie></span>', '</span>', '<span class="ivh-treeview-checkbox-wrapper" ng-if="trvw.useCheckboxes()"', 'ivh-treeview-checkbox>', '</span>', '<span class="ivh-treeview-node-label" ivh-treeview-toggle>', '{{trvw.label(node)}}', '</span>','<div>{{ node }}</div>', '<div ivh-treeview-children></div>', '</div>'].join('\n')nodeTpl: ['<div class="ivh-treeview-node-content" title="{{trvw.label(node)}}">', '<span ivh-treeview-toggle>', '<span class="ivh-treeview-twistie-wrapper" ivh-treeview-twistie></span>', '</span>', '<span class="ivh-treeview-checkbox-wrapper" ng-if="trvw.useCheckboxes()"', 'ivh-treeview-checkbox>', '</span>', '<span class="ivh-treeview-node-label" ivh-treeview-toggle>', '{{trvw.label(node)}}', '</span>','<div>{{ node }}</div>', '<div ivh-treeview-children></div>', '</div>'].join('\n')
            nodeTpl: `
                <div class="ivh-treeview-node-content" title="{{trvw.label(node)}}">
                    <span ng-if="trvw.label(node) !== 'CUSTOM_REGEX_INPUT_DIRECTIVE'" ivh-treeview-toggle style="padding-right:5px">
                        <span ng-if="trvw.label(node) !== 'CUSTOM_REGEX_INPUT_DIRECTIVE'" class="ivh-treeview-twistie-wrapper" ivh-treeview-twistie></span>
                    </span>
                    <span ng-if="trvw.label(node) !== 'CUSTOM_REGEX_INPUT_DIRECTIVE'" class="ivh-treeview-checkbox-wrapper" ng-if="trvw.useCheckboxes()" ivh-treeview-checkbox ></span>
                    <span  class="ivh-treeview-node-label" ivh-treeview-toggle>
                        <span ng-if="trvw.label(node) !== 'CUSTOM_REGEX_INPUT_DIRECTIVE'">
                            {{trvw.label(node)}}
                        </span>
                        <div ng-if="trvw.label(node) === 'CUSTOM_REGEX_INPUT_DIRECTIVE'">
                            <custom-regex-input></custom-regex-input>
                        </div>
                    </span>
                    <ul style="padding-left:31px">
                        <li ng-repeat="node in node.options">
                            {{ node.label + ' ' }}<md-checkbox class="" aria-label="close" ng-model="node.defaults"></md-checkbox>
                        </li>
                    </ul>
                    <div ivh-treeview-children></div>
                </div>
            `
        });
    });

export default treeModule;
