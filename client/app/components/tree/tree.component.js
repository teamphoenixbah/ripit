import template from './tree.html';
import controller from './tree.controller.js';
import './tree.scss';
import 'angular-ivh-treeview/dist/ivh-treeview.js';
import 'angular-ivh-treeview/dist/ivh-treeview.css';

let treeComponent = function () {
    return {
        template,
        controller,
        restrict: 'E',
        controllerAs: 'vm',
        scope: {},
        bindToController: true
    };
};

export default treeComponent;
