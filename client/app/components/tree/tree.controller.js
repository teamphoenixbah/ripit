class TreeController {
    constructor($mdDialog, APP_SETTINGS, AppStateService, ivhTreeviewMgr, StorageService) {
        'ngInject';

        this.$mdDialog = $mdDialog;
        this.APP_SETTINGS = APP_SETTINGS;
        this.AppStateService = AppStateService;
        this.ivhTreeviewMgr = ivhTreeviewMgr;
        this.StorageService = StorageService;

        this.treeData = this.APP_SETTINGS.regexPatterns;
        this.isOpen = false;
    }

    selectAll() {
        this.ivhTreeviewMgr.selectAll(this.treeData);
    }

    deselectAll() {
        this.ivhTreeviewMgr.deselectAll(this.treeData);
    }

    changeCallback() {
        this.AppStateService.setState('needsRipping', true);
    }
}

export default TreeController;
