import template from './customRegexInput.html';
import controller from './customRegexInput.controller.js';
import './customRegexInput.scss';

let customRegexInputComponent = function () {
    return {
        template,
        controller,
        restrict: 'E',
        controllerAs: 'vm',
        scope: {},
        bindToController: true
    };
};

export default customRegexInputComponent;
