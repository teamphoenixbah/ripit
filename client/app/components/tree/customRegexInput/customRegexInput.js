import angular from 'angular';
// import 'SVG-Morpheus';
import 'angular-material-icons';

import customRegexInputComponent from './customRegexInput.component.js';


let customRegexInputModule = angular.module('customRegexInput', [
    'ngMdIcons'
])
.directive('customRegexInput', customRegexInputComponent);

export default customRegexInputModule;
