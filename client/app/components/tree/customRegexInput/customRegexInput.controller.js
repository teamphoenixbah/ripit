/* eslint-disable no-param-reassign */
import angular from 'angular';

class CustomRegexInputController {
    constructor(APP_SETTINGS) {
        'ngInject';
        this.CUSTOM_BIN = 'Custom';

        this.regexdata = APP_SETTINGS.regexPatterns;
        this.bins = [];
        this.buildBins(this.regexdata);

        this.defaultResult = {
            flags: '',
            regex: '',
            bin: this.CUSTOM_BIN
        };
        this.defaultFlags = {
            'ignore case': true,
            global: true,
            multiline: false
        };
        this.flags = angular.copy(this.defaultFlags);
        this.result = angular.copy(this.defaultResult);
    }

    selectFlag(flag) {
        if (this.flags[flag]) {
            this.flags[flag] = false;
        } else {
            this.flags[flag] = true;
        }
    }

    buildBins(nodeItem) {
        angular.forEach(nodeItem, (node) => {
            if (node.children) {
                this.buildBins(node.children);
            } else {
                this.bins.push(node);
            }
        });

        this.bins.splice(this.bins.indexOf('custom-child'), 1);

        return this.bins;
    }

    save(result) {
        if (result.pattern) {
            // this is the format of the node structure that the
            result.patterns = [{
                name: result.pattern,
                regex: this.parseRegex(result)
            }];
            result.selected = true;
            result.label = `${result.pattern.length > 10 ? `${result.pattern.substr(0, 10)}` : result.pattern.substr(0, 10)} (${result.bin})`;

            // clean up the result object before it is inserted into the regex array
            delete result.flags;
            delete result.regex;
            delete result.pattern;

            // new pattern to regex object (this could probably be made more configurable)
            angular.forEach(this.regexdata, (data) => {
                if (data.label === 'Custom') {
                    // set Custom's checkbox to true when adding a custom regex
                    data.selected = true;
                    data.children.push(result);
                }
            });

            // reset form variables
            this.flags = angular.copy(this.defaultFlags);
            this.result = angular.copy(this.defaultResult);
        }
    }

    parseRegex(result) {
        let regExp = new RegExp(result.pattern, this.parseFlags(this.flags));
        return regExp;
    }

    parseFlags(flags) {
        let flagStr = '';
        angular.forEach(flags, (value, flag) => {
            if (flag === 'ignore case' && value === true) {
                flagStr += 'i';
            } else if (flag === 'global' && value === true) {
                flagStr += 'g';
            } else if (flag === 'multiline' && value === true) {
                flagStr += 'm';
            }
        });
        return flagStr;
    }
}

export default CustomRegexInputController;
