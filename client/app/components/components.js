import angular from 'angular';

import documentModule from './document/document.js';
import fileManagerModule from './fileManager/fileManager.js';
import matchMakerModule from './resultOutput/matchMaker/matchMaker.js';
import navbarModule from './navbar/navbar.js';
import queryBuilderModule from './queryBuilder/queryBuilder.js';
import resultOutputModule from './resultOutput/resultOutput.js';
import resultsModule from './resultOutput/results/results.js';
import ripItActionsModule from './ripItActions/ripItActions.js';
import treeModule from './tree/tree.js';
import customRegexInputModule from './tree/customRegexInput/customRegexInput.js';

let componentModule = angular.module('app.components', [
    documentModule.name,
    fileManagerModule.name,
    matchMakerModule.name,
    navbarModule.name,
    queryBuilderModule.name,
    resultOutputModule.name,
    resultsModule.name,
    ripItActionsModule.name,
    treeModule.name,
    customRegexInputModule.name
]);

export default componentModule;
