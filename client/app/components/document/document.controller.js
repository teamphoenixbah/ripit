class DocumentController {
    constructor($rootScope, AppStateService, FileManagerService, HTMLTextractService, StorageService, $scope) {
        'ngInject';

        this.$rootScope = $rootScope;
        this.AppStateService = AppStateService;
        this.FileManagerService = FileManagerService;
        this.HTMLTextractService = HTMLTextractService;
        this.StorageService = StorageService;
        this.$scope = $scope;

        this.listeners();
        this.watchers();
    }

    save(content) {
        // this.HTMLTextractService.getTextAsync(content).then((text) => {
        //     this.StorageService.documents[this.AppStateService.getState('documentToEdit')].content = text.content;
        //     this.AppStateService.setState('editBackup', false);
        //     this.AppStateService.setState('editContent', false);
        //     this.AppStateService.setState('editMode', false);
        //     this.AppStateService.setState('documentToEdit', false);
        //     // this.AppStateService.setState('needsRipping', false);
        // });
            this.StorageService.documents[this.AppStateService.getState('documentToEdit')].content = content;
            this.AppStateService.setState('editBackup', false);
            this.AppStateService.setState('editContent', false);
            this.AppStateService.setState('editMode', false);
            this.AppStateService.setState('documentToEdit', false);
    }

    cancel(document) {
        if (this.AppStateService.getState('editBackup')) {
            this.StorageService.documents[this.AppStateService.getState('documentToEdit')].content = this.AppStateService.getState('editBackup');
            this.AppStateService.setState('editBackup', false);
            this.AppStateService.setState('editcontent', false);
            this.AppStateService.setState('editMode', false);
            this.AppStateService.setState('documentToEdit', false);
        } else {
            delete this.StorageService.documents[this.AppStateService.getState('documentToEdit')];
            this.AppStateService.selectedDocs.delete(this.AppStateService.getState('documentToEdit'));
            this.AppStateService.setState('editMode', false);
            this.AppStateService.setState('documentToEdit', false);
        }
        this.$rootScope.$broadcast('files-changed');
    }

    handlePaste(evt, document) {
        document.content = this.HTMLTextractService.getText(evt.clipboardData.getData('text/plain'));
        console.log(document.content)

        // var item = evt.clipboardData.items[0];
        // item.getAsString((data) => {
        //     document.content = this.HTMLTextractService.getText(data);
        // });

        return document;
    }

    disableTab(document) {
        return this.AppStateService.getState('editMode') && this.AppStateService.documentToEdit !== document.name;
    }

    closeTab(document) {
        let index = this.AppStateService.selectedDocs.indexOf(document.name);
        this.AppStateService.selectedDocs.splice(index, 1);
        this.FileManagerService.deselectDocument(document);
    }

    listeners() {
        //
    }

    watchers() {
        //
    }
}

export default DocumentController;
