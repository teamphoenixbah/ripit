import template from './document.html';
import controller from './document.controller.js';
import './document.scss';

let documentComponent = function () {
    return {
        template,
        controller,
        restrict: 'E',
        controllerAs: 'vm',
        scope: {},
        bindToController: true
    };
};

export default documentComponent;
