import angular from 'angular';
import 'angular-sanitize';
import 'angular-contenteditable';
import commonModule from '../../common/common.js';
import documentComponent from './document.component.js';

let documentModule = angular.module('document', [
    commonModule.name,
    'contenteditable'
])
.directive('document', documentComponent);

export default documentModule;
