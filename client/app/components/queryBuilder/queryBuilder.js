import angular from 'angular';
import ngclipboard from 'ngclipboard';

import queryBuilderComponent from './queryBuilder.component.js';
import QueryBuilderService from './queryBuilder.service.js';

let queryBuilderModule = angular.module('queryBuilder', [
    ngclipboard
])
    .component('queryBuilder', queryBuilderComponent)
    .service('QueryBuilderService', QueryBuilderService);

export default queryBuilderModule;
