import angular from 'angular';

class QueryBuilderService {
    constructor($rootScope, RipItService, StorageService) {
        'ngInject';

        this.$rootScope = $rootScope;
        this.RipItService = RipItService;
        this.StorageService = StorageService;

        this.queryFormatOptionsMaster = [
            {label: 'All', value: 'all', excludedBins: ['ALL SELECTORS']},
            {label: 'All selectors', value: 'selectorAll', includedBins: ['ALL SELECTORS']},
            {label: 'Prepend with *', value: 'last9PrependWithAsterisk', includedBins: ['ALL SELECTORS']},
            {label: 'Harmony', value: 'harmony', includedBins: ['ALL SELECTORS']},
            {label: 'Custom', value: 'customFormat'}
        ];

        this.queryDelimiterOptions = [
            {label: 'Comma', value: ','},
            {label: 'AND', value: ' AND '},
            {label: 'OR', value: ' OR '},
            {label: 'New Line', value: '\n'},
            {label: 'Custom', value: 'customDelimiter'}
        ];

        this.queryBinsOptions = this.RipItService.binsList;
        this.queryFormatOptions = angular.copy(this.queryFormatOptionsMaster);

        this.queryBuilderForm = {
            bins: {
                options: this.queryBinsOptions,
                selected: this.queryBinsOptions[0] ? this.queryBinsOptions[0].value : ''
            },
            format: {
                options: this.queryFormatOptions,
                selected: this.queryFormatOptions[0].value,
                custom: {
                    pattern: '',
                    replace: '',
                    flags: 'ig'
                }
            },
            delimiter: {
                options: this.queryDelimiterOptions,
                selected: this.queryDelimiterOptions[0].value,
                custom: {
                    pattern: ''
                }
            },
            queryViewer: ''
        };

        this.listeners();
    }

    generateAllowedFormats(binName) {
        let allOptions = angular.copy(this.queryFormatOptionsMaster);
        let options = [];

        allOptions.forEach((option) => {
            if (option.includedBins && option.includedBins.includes(binName.toUpperCase())) {
                options.push(option);
            } else if (option.excludedBins && !option.excludedBins.includes(binName.toUpperCase())) {
                options.push(option);
            } else if(!option.includedBins && !option.excludedBins){
                options.push(option);
            }
        });

        this.queryBuilderForm.format.options = options;
        this.queryBuilderForm.format.selected = this.queryBuilderForm.format.options[0].value;

        // and now run because that what people want
        this.formatQuery(this.queryBuilderForm.bins, this.queryBuilderForm.format, this.queryBuilderForm.delimiter);

    }

    formatQuery(binObj, formatObj, delimiterObj) {
        if (binObj.selected) {
            let binData = this.generateBinData(binObj.selected);
            let formattedBinData = this.formatBinData(formatObj, binData);
            formattedBinData = this.joinBinData(formattedBinData, delimiterObj);
            this.queryBuilderForm.queryViewer = formattedBinData;
        }
    }

    generateBinsList() {
        return this.RipItService.generateBinsList();
    }

    generateBinData(binName) {
        let output = [];
        let uniqueEntities = new Set();

        // loop through Storage to get all documents
        for (let docName in this.StorageService.activeDocuments) {
            if (this.StorageService.activeDocuments.hasOwnProperty(docName)) {
                // get to the selected bin
                let bin = angular.copy(this.StorageService.activeDocuments[docName].results[binName]);

                for (let i = 0, len = bin.length; i < len; i++) {
                    uniqueEntities.add(bin[i][bin[i].mainGroup]);
                }
            }
        }

        // convert back to array
        output = Array.from(uniqueEntities);

        return output;
    }

    joinBinData (binData, delimiterObj) {
        let output;
        if (delimiterObj.selected === 'customDelimiter') {
            output = binData[0];
            for (let i = 1, len = binData.length; i < len; i++) {
                let pattern = angular.copy(delimiterObj.custom.pattern);
                pattern = pattern.replace(/\\n/g, '\n').replace(/\\t/g, '\t').replace(/\\r/g, '\r');
                output += pattern + binData[i];
            }
        } else {
            output = binData.join(delimiterObj.selected);
        }

        return output;
    }

    formatBinData(formatObj, binData) {
        let output;

        if (this[formatObj.selected] && typeof this[formatObj.selected] === 'function') {
            output = this[formatObj.selected](binData, formatObj);
        }

        return output;
    }

    all(binData) {
        return binData.map((value) => `"${value}"`);
    }

    selectorAll(binData) {
        return binData.map((value) => `${value}`);
    }

    last9PrependWithAsterisk(binData) {
        return binData.map((value) => `*${value.substr(-9)}`);
    }

    harmony(binData) {
        return binData.map((value) => `${value.substr(-9)} OR 0${value.substr(-9)} OR ${value}`);
    }

    customFormat(binData, formatObj) {
        let output = [];
        if (formatObj.custom.pattern !== '') {
            let re = new RegExp(formatObj.custom.pattern, formatObj.custom.flags);

            binData.forEach((value) => {
                let replacement = value.replace(re, formatObj.custom.replace);
                output.push(replacement);
            });
        }
        return output;
    }

    listeners() {
        this.$rootScope.$on('data-ripped', () => {
            this.generateBinsList();
        });
    }
}

export default QueryBuilderService;
