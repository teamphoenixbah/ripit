import template from './queryBuilder.html';
import controller from './queryBuilder.controller';
import './queryBuilder.scss';

let queryBuilderComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default queryBuilderComponent;
