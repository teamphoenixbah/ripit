class QueryBuilderController {
    constructor($mdToast, $rootScope, QueryBuilderService, RipItService, StorageService) {
        'ngInject';

        this.$mdToast = $mdToast;
        this.$rootScope = $rootScope;
        this.QueryBuilderService = QueryBuilderService;
        this.RipItService = RipItService;
        this.StorageService = StorageService;

        this.queryFormatOptionsMaster = this.RipItService.queryFormatOptionsMaster;
        this.queryBuilderForm = this.QueryBuilderService.queryBuilderForm;

        this.listeners()
    }

    getQueryOutput(queryView) {
        return this.QueryBuilderService.getQueryOutput(queryView);
    }

    formatQuery(binObj, formatObj, delimiterObj) {
        return this.QueryBuilderService.formatQuery(binObj, formatObj, delimiterObj);
    }

    generateAllowedFormats(binName) {
        return this.QueryBuilderService.generateAllowedFormats(binName);
    }

    onSuccess(evt) {
        evt.clearSelection();

        this.$mdToast.show(
            this.$mdToast.simple()
                .textContent('Query Copied!')
                .position('top right')
                .hideDelay(3000)
        );
    }

    listeners(){
        this.$rootScope.$on('files-changed', () => {
            if (this.queryBuilderForm) {
                this.formatQuery(this.queryBuilderForm.bins, this.queryBuilderForm.format, this.queryBuilderForm.delimiter)
            }
        });
    }

}

export default QueryBuilderController;
