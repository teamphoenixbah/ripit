import angular from 'angular';
import ripitActionsComponent from './ripitActions.component.js';

let ripitActionsModule = angular.module('ripitActions', [])
    .component('ripitActions', ripitActionsComponent);

export default ripitActionsModule;
