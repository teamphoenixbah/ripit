import template from './ripitActions.html';
import controller from './ripitActions.controller';
import './ripitActions.scss';

let ripitActionsComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default ripitActionsComponent;
