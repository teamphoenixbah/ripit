/* eslint-disable consistent-return */
import angular from 'angular';

class RipitActionsController {
    constructor($mdDialog, $q, $rootScope, RipItService, StorageService, ParseProvider, ResultsExportService, $scope) {
        'ngInject';

        this.$mdDialog = $mdDialog;
        this.$q = $q;
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.RipItService = RipItService;
        this.StorageService = StorageService;
        this.ParseProvider = ParseProvider;
        this.ResultsExportService = ResultsExportService;
    }

    exportXLSX (evt) {
        if (!this.RipItService.binsList.length) {
            this.$mdDialog.show(
                this.$mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('No Data to Export')
                    .textContent('Please add files and click RipIt to enable the Export feature')
                    .ariaLabel('Alert Modal')
                    .ok('OK')
                    .targetEvent(evt)
            );
            return false;
        }

        this.ResultsExportService.toXLSX();
    }

    exportKML(evt) {
        this.$mdDialog.show(
            this.$mdDialog.alert()
                .clickOutsideToClose(true)
                .title('KML Export')
                .textContent('This feature is not implemented at this time.')
                .ariaLabel('Alert Modal')
                .ok('OK')
                .targetEvent(evt)
        );
        return false;
    }

    exportANB(evt) {
        this.$mdDialog.show(
            this.$mdDialog.alert()
                .clickOutsideToClose(true)
                .title('ANB Export')
                .textContent('This feature is not implemented at this time.')
                .ariaLabel('Alert Modal')
                .ok('OK')
                .targetEvent(evt)
        );
        return false;
    }
}

export default RipitActionsController;
