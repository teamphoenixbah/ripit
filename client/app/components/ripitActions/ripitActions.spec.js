/* eslint-disable new-cap, no-unused-vars, no-undef */
/* global inject */

import RipitActionsModule from './ripitActions.js';
import RipitActionsController from './ripitActions.controller.js';
import RipitActionsComponent from './ripitActions.component.js';
import RipitActionsTemplate from './ripitActions.html';

describe('RipitActions', () => {
    let $rootScope;
    let makeController;

    beforeEach(window.module(RipitActionsModule.name));
    beforeEach(inject((_$rootScope_) => {
        $rootScope = _$rootScope_;
        makeController = () => new RipitActionsController();
    }));

    describe('Module', () => {
        // top-level specs: i.e., routes, injection, naming
    });

    describe('Controller', () => {
        // controller specs
        it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
            let controller = makeController();
            expect(controller).to.have.property('name');
        });
    });

    describe('Template', () => {
        // template specs
        // tip: use regex to ensure correct bindings are used e.g., {{  }}
        it('has name in template [REMOVE]', () => {
            expect(RipitActionsTemplate).to.match(/{{\s?vm\.name\s?}}/g);
        });
    });

    describe('Component', () => {
        // component/directive specs
        let component = RipitActionsComponent;

        it('includes the intended template', () => {
            expect(component.template).to.equal(RipitActionsTemplate);
        });

        it('uses `controllerAs` syntax', () => {
            expect(component).to.have.property('controllerAs');
        });

        it('invokes the right controller', () => {
            expect(component.controller).to.equal(RipitActionsController);
        });
    });
});
