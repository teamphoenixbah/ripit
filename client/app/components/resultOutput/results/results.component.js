import template from './results.html';
import controller from './results.controller.js';
import './results.scss';

let resultsComponent = function () {
    return {
        template,
        controller,
        restrict: 'E',
        controllerAs: 'vm',
        scope: {},
        bindToController: true
    };
};

export default resultsComponent;
