class ResultsController {
    constructor($mdDialog, $scope, $timeout, AppStateService, HelpersService, RipItService, StorageService, TableService) {
        'ngInject';

        this.$scope = $scope;
        this.$timeout = $timeout;
        this.AppStateService = AppStateService;
        this.HelpersService = HelpersService;
        this.RipItService = RipItService;
        this.StorageService = StorageService;
        this.TableService = TableService;
        this.$mdDialog = $mdDialog;

        this.binData = {};


        this.gridOptions = angular.copy(this.TableService.defaultGridOptions);

        this.initialize();
        this.listeners();
        //this.watchers();
    }

    initialize() {
        //
    }

    viewBinResult(binName) {
        this.showResultTable = true;
        this.AppStateService.setState('showResultTable', true);
        this.AppStateService.setState('currentBinView', binName);

        this.generateTableData(binName);
    }

    generateTableData(binName) {
        this.$timeout(() => {
            let binResultData = this.generateBinResultData(binName);
            this.gridOptions.api.setColumnDefs(this.TableService.generateTableHeaders(binResultData, {colName: 'Count'}));
            this.gridOptions.api.setRowData(binResultData);
            this.gridOptions.api.sizeColumnsToFit();
        });
    }

    closeBinResults() {
        this.AppStateService.setState('showResultTable', false);
        this.AppStateService.setState('currentBinView', '');
        this.binData = this.generateBinOverviewResultData();
    }

    generateBinOverviewResultData() {
        return this.RipItService.generateBinOverviewResultData();
    }

    generateBinResultData(binName) {
        return this.RipItService.generateBinResultData(binName);
    }

    generateTableHeaders(data, sortObj) {
        return this.RipItService.generateTableHeaders(data, sortObj);
    }

    watchers() {
        this.$scope.$watch(() => this.StorageService.activeDocuments, () => {
            this.binData = this.generateBinOverviewResultData();
        }, true);
    }

    listeners() {
        // TODO: this is a hack for now, and needs to be changed
        this.$scope.$root.$onMany(['files-changed', 'data-ripped', 'build-results'], () => {
            if (this.AppStateService.getState('ResultsView') === 'Results') {
                if (this.AppStateService.getState('showResultTable')) {
                    this.generateTableData(this.AppStateService.getState('currentBinView'));
                } else {
                    this.binData = this.generateBinOverviewResultData();
                }
            }
        });
    }
}

export default ResultsController;
