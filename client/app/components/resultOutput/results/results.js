import angular from 'angular';
import resultsComponent from './results.component.js';

import agGrid from 'ag-grid';
agGrid.initialiseAgGridWithAngular1(angular);
import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/theme-fresh.css';
import 'ag-grid/dist/styles/theme-blue.css';

let resultsModule = angular.module('results', [
    'agGrid'
])
.directive('results', resultsComponent);

export default resultsModule;
