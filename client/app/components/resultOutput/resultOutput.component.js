import template from './resultOutput.html';
import controller from './resultOutput.controller.js';
import './resultOutput.scss';

let resultOutputComponent = function () {
    return {
        template,
        controller,
        restrict: 'E',
        controllerAs: 'vm',
        scope: {},
        bindToController: true
    };
};

export default resultOutputComponent;
