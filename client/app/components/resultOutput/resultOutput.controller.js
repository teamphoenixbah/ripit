import angular from 'angular';
class ResultOutputController {
    constructor($mdDialog, $rootScope, $scope, AppStateService) {
        'ngInject';

        this.$mdDialog = $mdDialog;
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.AppStateService = AppStateService;
        this.selectedTab = this.AppStateService.state.selectedTab;

        this.watchers();
    }

    watchers() {
        this.$scope.$watch(() => this.AppStateService.state.selectedTab, () => {
            if (this.AppStateService.state.selectedTab === 0) {
                this.AppStateService.setState('ResultsView', 'Results');
            } else if (this.AppStateService.state.selectedTab === 1) {
                this.AppStateService.setState('ResultsView', 'MatchMaker');
            }
            this.$rootScope.$broadcast('build-results');
        }, true);
    }

    showInfo(evt) {
        this.$mdDialog.show({
            template: `
                <md-dialog aria-label="Results Dialog" ng-cloak>
                    <md-dialog-content>
                        <div class="md-dialog-content">
                            <h2>Results Section</h2>
                            <p style="width:250px">
                                These results provide a high level overview of all extracted entities across all documents and extraction bin.
                            </p>
                            <p>
                                <div>Legend:</div>
                                <div class="bin-container" style="font-size: 20px;height:initial;">
                                    <table style="border: 1px solid black;">
                                        <tr>
                                            <td style="width:75px">Unique</td>
                                            <td style="width:75px">Total</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Documents</td>
                                        </tr>
                                    </table>
                                </div>
                            </p>
                        </div>
                    </md-dialog-content>
                </md-dialog>
            `,
            parent: angular.element(document.body),
            targetEvent: evt,
            clickOutsideToClose: true,
            fullscreen: true
        });
    }
}

export default ResultOutputController;
