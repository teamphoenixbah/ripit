import angular from 'angular';
import matchMakerComponent from './matchMaker.component.js';

let matchMakerModule = angular.module('matchMaker', [])
    .component('matchMaker', matchMakerComponent);

export default matchMakerModule;
