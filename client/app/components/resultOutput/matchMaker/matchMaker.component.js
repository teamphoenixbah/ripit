import template from './matchMaker.html';
import controller from './matchMaker.controller.js';
import './matchMaker.scss';

let matchMakerComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default matchMakerComponent;
