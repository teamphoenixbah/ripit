import angular from 'angular';
import _ from 'lodash';

class MatchMakerController {
    constructor($scope, $timeout, AppStateService, HelpersService, RipItService, StorageService, TableService) {
        'ngInject';

        this.$scope = $scope;
        this.$timeout = $timeout;
        this.AppStateService = AppStateService;
        this.HelpersService = HelpersService;
        this.RipItService = RipItService;
        this.StorageService = StorageService;
        this.TableService = TableService;

        this.initialize();
        this.listeners();
    }

    initialize() {
        this.matchMakerOverviewBins = [];
        this.description = 'Quickly identify common entities between two or more documents.';
        this.gridOptions = angular.copy(this.TableService.defaultGridOptions);
    }

    generateMatchMakerData() {
        return this.RipItService.generateMatchMakerData();
    }

    viewMatchMakerResult(binName) {
        this.showMatchMakerTable = true;
        this.AppStateService.setState('showMatchMakerTable', true);
        this.AppStateService.setState('currentBinView', binName);

        let binData = this.generateMatchMakerData()[binName];
        this.generateTableData(binData);
    }

    generateTableData(binData) {
        this.$timeout(() => {
            this.gridOptions.api.setColumnDefs(this.TableService.generateTableHeaders(binData));
            this.gridOptions.api.setRowData(binData);
            this.gridOptions.api.sizeColumnsToFit();
        });
    }

    generateMatchMakerResultData(binName) {

        if (binName.toUpperCase() === 'ALL') {
            let allMatchMakerTable = [];
            let binsList = angular.copy(this.RipItService.binsList);
            for (let bin in binsList) {
                if (binsList.hasOwnProperty(bin)) {
                    let binResultData = this.generateMatchMakerResultData(binsList[bin].name);
                    console.log(binsList[bin].name, binResultData);
                    allMatchMakerTable.concat(binResultData);
                }
            }

            console.log(allMatchMakerTable);
        }


        let binData = this.RipItService.generateBinResultData(binName);
        let metaProperties = ['Count', 'Document'];

        let matchMaker = {};
        let matchMakerTable = {};

        let extractedEntityProperties = new Set();

        for (let i = 0, len = binData.length; i < len; i++) {
            // build up the unique key
            let uniqueKey = [];
            for (let d in binData[i]) {
                if (binData[i].hasOwnProperty(d)) {
                    if (!metaProperties.includes(d)) {
                        uniqueKey.push(binData[i][d]);
                        extractedEntityProperties.add(d);
                    }
                }
            }
            uniqueKey = uniqueKey.join('|');

            if (!matchMaker[uniqueKey]) {
                matchMaker[uniqueKey] = {};
            }
            if (!matchMaker[uniqueKey][binData[i].Document]) {
                matchMaker[uniqueKey][binData[i].Document] = [];
            }
            matchMaker[uniqueKey][binData[i].Document].push(binData[i])
        }


        // remove entities that have less than 2 matches between documents
        for (let uniqueKey in matchMaker) {
            if (matchMaker.hasOwnProperty(uniqueKey)) {
                if (Object.keys(matchMaker[uniqueKey]).length < 2) {
                    delete matchMaker[uniqueKey];
                }
            }
        }
        let headers = new Set();
        for (let entity of extractedEntityProperties) {
            headers.add(entity);
        }

        headers.add('Total');
        for (let uniqueKey in matchMaker) {
            if (matchMaker.hasOwnProperty(uniqueKey)) {
                for (let u in matchMaker[uniqueKey]) {
                    if (matchMaker[uniqueKey].hasOwnProperty(u)) {
                        headers.add(u.replace(/\./g, ''));
                    }
                }
            }
        }
        let defaultRow = {};

        for (let h of headers) {
            if (h === 'Total') {
                defaultRow[h] = 0;
            } else {
                defaultRow[h] = '';
            }
        }
        for (let uniqueKey in matchMaker) {
            if (matchMaker.hasOwnProperty(uniqueKey)) {
                for (let u in matchMaker[uniqueKey]) {
                    if (matchMaker[uniqueKey].hasOwnProperty(u)) {
                        if (!matchMakerTable[uniqueKey]) {
                            matchMakerTable[uniqueKey] = angular.copy(defaultRow);
                            for (let entity of extractedEntityProperties) {
                                matchMakerTable[uniqueKey][entity] = matchMaker[uniqueKey][u][0][entity];
                            }
                            matchMakerTable[uniqueKey][matchMaker[uniqueKey][u][0].Document.replace(/\./g, '')] = 'True';
                            matchMakerTable[uniqueKey].Total++;
                        } else {
                            matchMakerTable[uniqueKey][matchMaker[uniqueKey][u][0].Document.replace(/\./g, '')] = 'True';
                            matchMakerTable[uniqueKey].Total++;
                        }
                    }
                }
            }
        }
        matchMakerTable = _.values(matchMakerTable)

        console.log(matchMakerTable);

        return matchMakerTable;
    }

    closeMatchMakerTable() {
        this.AppStateService.setState('showMatchMakerTable', false);
        this.AppStateService.setState('currentBinView', '');
    }

    listeners() {
        this.$scope.$on('files-changed', () => {
            if (this.AppStateService.getState('ResultsView') === 'MatchMaker') {
                console.log(1)
                if (this.AppStateService.getState('showMatchMakerTable')) {
                    console.log(2)
                    let binData = this.generateMatchMakerData()[this.AppStateService.getState('currentBinView')]
                    this.generateTableData(binData);
                } else {
                    console.log(3)
                    this.matchMakerOverviewBins = this.generateMatchMakerData();
                }
            }
        });

        this.$scope.$on('data-ripped', () => {
            if (this.AppStateService.getState('ResultsView') === 'MatchMaker') {
                this.matchMakerOverviewBins = this.generateMatchMakerData();
            }
        });

        this.$scope.$on('build-results', () => {
            if (this.AppStateService.getState('ResultsView') === 'MatchMaker') {
                this.matchMakerOverviewBins = this.generateMatchMakerData();
            }
        });
    }
}

export default MatchMakerController;
