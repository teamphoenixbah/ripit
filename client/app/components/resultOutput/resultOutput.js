import angular from 'angular';
import resultOutputComponent from './resultOutput.component.js';

let resultOutputModule = angular.module('resultOutput', [

])
.directive('resultOutput', resultOutputComponent);

export default resultOutputModule;
