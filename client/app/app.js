import 'core-js/shim';
import angular from 'angular';
import uiRouter from 'angular-ui-router';

import ngMaterial from 'angular-material';
import 'angular-material/angular-material.css';

import 'bootstrap_grid_small/bootstrap_grid_small.css';
import 'animate.css/animate.min.css';

import Core from './core/core.js';
import Common from './common/common.js';
import Components from './components/components.js';
import AppComponent from './app.component.js';

angular.module('app', [
    uiRouter,
    ngMaterial,
    Core.name,
    Common.name,
    Components.name
])
    // TODO: this works for now, but probably should get moved to its own file
    .run(($rootScope) => {
        'ngInject';
        $rootScope.$onMany = function(events, fn) {
            for (var i = 0, len = events.length; i < len; i++) {
                this.$on(events[i], fn);
            }
        }
    })

    .config(($locationProvider) => {
        'ngInject';
        // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
        // #how-to-configure-your-server-to-work-with-html5mode
        $locationProvider.html5Mode(true).hashPrefix('#');
    })

    .component('app', AppComponent);


// do not allow user to accidentally close (not set on localhost)
if (window.location.href.indexOf('localhost') === -1) {
    window.onbeforeunload = () => '';
}
